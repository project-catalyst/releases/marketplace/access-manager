$(document).ready(function () {

    $(".vertical_tab").removeClass("active");
    $("#service-message").removeClass("active");

    if (service == "congestion_management") {
        $("#congestion_management_tab").addClass("active");
    } else if (service == "reactive_power_compensation") {
        $("#reactive_power_compensation_tab").addClass("active");
    } else if (service == "spinning_reserve") {
        $("#spinning_reserve_tab").addClass("active");

    } else if (service == "scheduling") {
        $("#scheduling_tab").addClass("active");
    }
});