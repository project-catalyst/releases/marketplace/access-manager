

$(document).on('all.bs.table', "#ancillary_actions_table", function (name, args) {
    $("td").each(function () { $(this).css('vertical-align', 'middle').css('font-size', '11pt'); });
    $("th").css('vertical-align', 'middle');
});


function setActionTypeFormatter(value, row, index) {
    if (value == "bid") {
        return '<div style="color:red;">Bid</div>';
    }
    return '<div style="color:green;">Offer</div>';
}

function setActionStatusFormatter(value, row, index) {
    return ActionStatus.formatter(value);
}
