var gamTableSelector = "#data_table_session_closed";
var deliveryTableSelector = "#delivery_table";
var actorId;
var USERNAME_TO_ACTOR = "/actor-by-username/";
var actionStatus = {};

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            xhr.setRequestHeader("X-CSRFToken", new Cookies().getValue('csrftoken'));
        }
    }
});

$(document).ready(function () {

    $(".vertical_tab").removeClass("active");
    $("#service-message").removeClass("active");
    actorId = getActorId(USERNAME_TO_ACTOR);
    if (service == "congestion_management") {
        $("#congestion_management_tab").addClass("active");
    }
    else if (service == "reactive_power_compensation") {
        $("#reactive_power_compensation_tab").addClass("active");
    }
    else if(service == "spinning_reserve"){
        $("#spinning_reserve_tab").addClass("active");

    } else if(service == "scheduling"){
          $("#scheduling_tab").addClass("active");
    }


    if(timeframe == "day_ahead"){
        $("#day_ahead_tab").addClass("active");
    } else if(timeframe == "intra_day"){
         $("#intra_day_tab").addClass("active");
    }

    $("#service").addClass("active");

    if (is_dso) {
        $("#leftmenu").addClass("margin-top-60px");
    }
    else {
        $("#leftmenu").removeClass("margin-top-60px");
    }

    $('#actionStartTime').datetimepicker();
    $('#actionStartTime').data("DateTimePicker").format('YYYY-MM-DD HH:mm ZZ');
    $('#actionEndTime').datetimepicker();
    $('#actionEndTime').data("DateTimePicker").format('YYYY-MM-DD HH:mm ZZ');

    var data = [];
    $(gamTableSelector).bootstrapTable({ data: data });
    $(deliveryTableSelector).bootstrapTable({ data: data });
    $('.fixed-table-body').css('height', 'auto');


    //moment.tz.setDefault("Europe/London");
    moment.tz.setDefault("UTC");
    setInterval(function () { updateData(); }, 60000);

    if (sessionId > 0) {
        $('#actionStartTime').datetimepicker();
        $('#actionStartTime').data("DateTimePicker").format(UtcDateTime.format());
        $('#actionEndTime').datetimepicker();
        $('#actionEndTime').data("DateTimePicker").format(UtcDateTime.format());
    }
}).on('all.bs.table', gamTableSelector, function (name, args) {
    $("td").each(function () {
        $(this).css('vertical-align', 'middle');
    });
}).on('check.bs.table ', gamTableSelector, function (name, service) {
    $(".select-offers-btn").removeAttr('disabled');
}).on('uncheck.bs.table', gamTableSelector, function (name, service) {
    if (!$(gamTableSelector).bootstrapTable('getSelections').length) {
        $(".select-offers-btn").attr('disabled', 'disabled');
    }
}).on('check-all.bs.table', gamTableSelector, function (name, service) {
    $(".select-offers-btn").removeAttr('disabled');
}).on('uncheck-all.bs.table', gamTableSelector, function (name, service) {
    $(".select-offers-btn").attr('disabled', 'disabled');
}).on('all.bs.table', deliveryTableSelector, function (name, args) {
    $("td").each(function () {
        $(this).css('vertical-align', 'middle');
    });
});

function getActorId(url){
    console.log("getActorId")
    res = -1;
    console.log(registeredMarket.url + url + username);
     $.ajax({
        type: "GET",
        url: registeredMarket.url + url + username + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
                res = response.id;
        },
         error: function () {
            console.log("Couldn't get actor id by username");
         }

    });
    return res;
}


function getClearingPrice() {
    $.ajax({
        type: "GET",
        url: clearPriceURL,
        datatype: "json",
        contenttype: 'application/json',
        success: function (response) {
            $("#last-clearing-price").html(response.text);
        },
        error: function (response) {
            console.error("Clearing price is not provided");
        },
        complete: function () { }
    });
}

function updateData() {
    $(gamTableSelector).bootstrapTable('refresh', { silent: true });
    $(deliveryTableSelector).bootstrapTable('refresh', { silent: true });
}


function modal_actuate(url, method, id){
    var valid = {
        a: ActionValidator.datetime($("#actionStartTime")),
        b: ActionValidator.datetime($("#actionEndTime")),
        c: ActionValidator.service($("#select-ancillary-service")),
        d: ActionValidator.value($("#value")),
        e: ActionValidator.uom($("#uoml")),
        f: ActionValidator.price($("#price")),
        g: ActionValidator.deliveryPoint($("#deliveryPoint")),
        h: ActionValidator.type($("#typel"))
    };

    if (!(valid.a && valid.b && valid.c && valid.d && valid.e && valid.f && valid.g) ) {
        return;
    }
    var $inputs = $('#AddActionForm input');
    var list = [];
    var values = {};

    $inputs.each(function () {
        if (this.id === "type") {
            values["actionTypeid"] =  getActionType(registeredMarket.url + ACTION_TYPE_URL, $(this).val());
        } else {
            values[this.id] = $(this).val();
        }
        $(this).val('');
        $('#uom').val($('#uoml').val());
        $('#type').val($('#typel').val());
    });
    values['date'] = moment.utc().format();
    values['actionStartTime'] = moment.utc(values['actionStartTime'], 'YYYY-MM-DD HH:mm Z').format();
    values['actionEndTime'] = moment.utc(values['actionEndTime'], 'YYYY-MM-DD HH:mm Z').format();
    values['marketSessionid'] = SESSION.id;
    values['marketActorid'] = actorId;
    values['formid'] = getSessionForm(registeredMarket.url + MPLACE_URL+ MPLACE +"/marketsessions/" + SESSION.id + "/", MPLACE, SESSION.id);
    values['statusid'] = getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "unchecked");

    // added these fields to support standard format of actions (considering it load market actions required these fields on the corresponding Serializers)
    values['cpu'] = 0.0;
    values['ram'] = 0.0;
    values['disk'] = 0.0;
    values['loadid'] = null;

    if (id != null) {
        values['id'] = id;
    }
    $('#addAction').modal('hide');
    var payload = null;
    if (method === "POST") {
        list.push(values);
        payload = list;
    }
    else {
        payload=values;
    }

    $.ajax({
      url:url,
      type: method,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
      dataType:"json",
      contentType: "application/json; charset=utf-8",
      data:JSON.stringify(payload),
      success: function(data){
          ancCustomRefresher();
      },
      error: function(){
        ancCustomRefresher();
      }
    });
}

function newAction() {
    if (ref_price != null){
        $("#reference_price").text(parseFloat(ref_price.price).toFixed(2).replace('.', ','));
    }else{$("#reference_price").text("Not set");}

    $("#actionStartTime").val('');
    $("#actionEndTime").val('');
    $("#price").val('');
    $("#deliveryPoint").val('');
    $("#value").val('');
    $("#typel").val('').change();
    $("#uoml").val('').change();
    setServiceUnit();
    $("#modalAccept").html("Add");
    $("#modalAccept").unbind();
    $("#modalAccept").click(function () {
        var u1 = registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + SESSION.id + "/actions/";
        modal_actuate(u1, "POST");
    });
    $("#addActionLabel").text("Place a new market action");
    $("#addAction").modal('show');
}

function editAction(id){
    var url = registeredMarket.url + ACTION_URL + id+"/";
     if (ref_price != null){
        $("#reference_price").text(parseFloat(ref_price.price).toFixed(2).replace('.', ','));
    }else{$("#reference_price").text("Not set");}
    $.ajax({
        url:url,
        type:"GET",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        success:function( data ) {
            $("#actionStartTime").val(moment.utc(data.actionStartTime).format('YYYY-MM-DD HH:mm ZZ'));
            $("#actionEndTime").val(moment.utc(data.actionEndTime).format('YYYY-MM-DD HH:mm ZZ'));
            $("#price").val(data.price);
            $("#deliveryPoint").val(data.deliveryPoint);
            $("#value").val(data.value);
            if (data.actionTypeid.type == "offer") {
                $("#typel").val('offer').change();
            } else {
                $("#typel").val('bid').change();
            }
            if (data.uom.toUpperCase() == "KW") {
                $("#uoml").val('KW').change();
            } else {
             $("#uoml").val('KVA').change();
            }
            $("#modalAccept").html("Update");
            $("#modalAccept").unbind();
            $("#modalAccept").click(function(){
                var u1 = registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + SESSION.id + "/actions/" + data.id + "/edit/";
                modal_actuate(u1, "PUT", data.id);
        });
        $("#addActionLabel").text("Update market action");
        $("#addAction").modal('show');
        }
    });
}

function removeAction(id) {
    var list_data = {};
    $.ajax({
        url:registeredMarket.url + ACTION_URL + id + "/",
        type:"GET",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        success:function( data ) {
            list_data['statusid'] = getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "withdrawn");
            list_data['actionTypeid'] = data['actionTypeid'].id;
            list_data['id'] = data.id;
            list_data['formid'] = data['formid'].id;
            list_data['marketActorid'] = data['marketActorid'].id;
            list_data['marketSessionid'] = data['marketSessionid'].id;
            list_data['actionEndTime'] = data['actionEndTime'];
            list_data['actionStartTime'] = data['actionStartTime'];
            list_data['date'] = data['date'];
            list_data['deliveryPoint'] = data['deliveryPoint'];
            list_data['uom'] = data['uom'];
            list_data['price'] = data['price'];
            list_data['value'] = data['value'];

            // added these fields to support standard format of actions (considering it load market actions required these fields on the corresponding Serializers)
            list_data['cpu'] = 0.0;
            list_data['ram'] = 0.0;
            list_data['disk'] = 0.0;
            list_data['loadid'] = null;

            options = {headers: {"Authorization": registeredMarket.token}};
            $.ajax({
                type: "PUT",
                beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
                },
                url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + SESSION.id + "/actions/" + data.id + "/edit/",
                contentType: "application/json",
                data: JSON.stringify(list_data),
                success: function(){
                ancCustomRefresher();
                },
                error: function(){
                    ancCustomRefresher();
                }
            });
        }
    });
}

function selectOffer() {
    // DSO selects offers
    var offers = $(gamTableSelector).bootstrapTable('getSelections');
    var mplace = MPLACE;
    var ses =$("#select_balancing_session2").val();
    for (var i in offers) {
        var id = offers[i].id;
        var payload={};
        $.ajax({
            type: "GET",
            url: registeredMarket.url + ACTION_URL + id + "/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: true,
            success: function (data) {
               payload['statusid']=getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "selected");
               payload['actionTypeid']=data.actionTypeid.id;
               payload['id']=data.id;
               payload['formid']=data.formid.id;
               payload['marketActorid']=data.marketActorid.id;
               payload['marketSessionid']=data.marketSessionid.id;
               payload['actionEndTime']=data.actionEndTime;
               payload['actionStartTime']=data.actionStartTime;
               payload['date']=data.date;
               payload['deliveryPoint']=data.deliveryPoint;
               payload['price']=data.price;
               payload['uom']=data.uom;
               payload['value']=data.value;

               // added these fields to support standard format of actions (considering it load market actions required these fields on the corresponding Serializers)
               payload['ram'] = 0.0;
               payload['cpu'] = 0.0;
               payload['disk'] = 0.0;
               payload['loadid'] = null;

               $.ajax({
                   type: "PUT",
                   url: registeredMarket.url + MPLACE_URL+mplace+"/marketsessions/"+ses+"/actions/"+data.id+"/selected/",
                   contentType: "application/json",
                   data: JSON.stringify(payload),
                   beforeSend: function (xhr) {
                       xhr.setRequestHeader("Authorization", registeredMarket.token)
                   },
                   success: function () {
                       setClosedMarketSessionInfo();
                       if(i == offers.length-1 ) {
                           swal({
                               title: "Offer(s) selection",
                               text: 'The offers\' selection has been submitted.',
                               type: "success",
                               confirmButtonText: "OK",
                               confirmButtonColor: "#009245",
                           },
                            function(isConfirm) {
                              if (isConfirm) {
                                $('#slctbtn').attr('disabled', 'disabled');
                              }
                            });
                       }
                       },
                   error: function () {
                       console.error("Error in offer selection with id:" + id);
                       setClosedMarketSessionInfo();
                   }
               });
            }
        });
    }
    return;
}

function confirmDelivery(id) {
    // DSO confirm delivery
    swal({
        title: "Delivery confirmation",
        text: 'Do you confirm that the current service has been delivered?',
        type: "info",
        confirmButtonText: "Yes",
        confirmButtonColor: "#009245",
        showCancelButton: true,
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            swal({
                title: "Delivery confirmation",
                text: 'The service delivery confirmation has been submitted',
                type: "success",
                confirmButtonText: "OK",
                confirmButtonColor: "#009245"
            });
            var payload={};
            var mplace = MPLACE;
            var ses =$("#select_balancing_session3").val();
            $.ajax({
                type: "GET",
                url: registeredMarket.url + ACTION_URL + id + "/",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", registeredMarket.token)
                },
                datatype: "json",
                contenttype: 'application/json',
                async: true,
                success: function (data) {
                   payload['statusid']=getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "delivered");
                   payload['actionTypeid']=data.actionTypeid.id;
                   payload['id']=data.id;
                   payload['formid']=data.formid.id;
                   payload['marketActorid']=data.marketActorid.id;
                   payload['marketSessionid']=data.marketSessionid.id;
                   payload['actionEndTime']=data.actionEndTime;
                   payload['actionStartTime']=data.actionStartTime;
                   payload['date']=data.date;
                   payload['deliveryPoint']=data.deliveryPoint;
                   payload['price']=data.price;
                   payload['uom']=data.uom;
                   payload['value']=data.value;
                   payload['ram'] = 0.0;
                   payload['cpu'] = 0.0;
                   payload['disk'] = 0.0;
                   payload['loadid'] = null;
                   $.ajax({
                       type: "PUT",
                       url: registeredMarket.url + MPLACE_URL+mplace+"/marketsessions/"+ses+"/actions/"+data.id+"/edit/",
                       contentType: "application/json",
                       data: JSON.stringify(payload),
                       beforeSend: function (xhr) {
                           xhr.setRequestHeader("Authorization", registeredMarket.token)
                       },
                       success: function () {
                            ancCustomRefresher();
                       },
                       error: function () {
                           ancCustomRefresher();
                       }
                   });
                }
            });
        }
        else {
            swal({
                title: "Delivery confirmation",
                text: 'The service delivery confirmation has been cancelled',
                type: "warning",
                confirmButtonText: "OK",
                confirmButtonColor: "#009245"
            });
        }
    });
}

function referNotDelivery(id) {
    // DSO confirm that service has not delivered
    swal({
        title: "Delivery Failure",
        text: 'Do you confirm that the current service has not been delivered?',
        type: "info",
        confirmButtonText: "Yes",
        confirmButtonColor: "#009245",
        showCancelButton: true,
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            swal({
                title: "Delivery Failure",
                text: 'The service delivery failure has been submitted',
                type: "success",
                confirmButtonText: "OK",
                confirmButtonColor: "#009245",
            });
            var payload={};
            var mplace = MPLACE;
            var ses =$("#select_balancing_session3").val();
            $.ajax({
                type: "GET",
                url: registeredMarket.url + ACTION_URL + id + "/",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", registeredMarket.token)
                },
                datatype: "json",
                contenttype: 'application/json',
                async: true,
                success: function (data) {
                  // data.statusid={ "id": 12, "status": "not_delivered" };
                   payload['statusid']=getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "not_delivered");
                   payload['actionTypeid']=data.actionTypeid.id;
                   payload['id']=data.id;
                   payload['formid']=data.formid.id;
                   payload['marketActorid']=data.marketActorid.id;
                   payload['marketSessionid']=data.marketSessionid.id;
                   payload['actionEndTime']=data.actionEndTime;
                   payload['actionStartTime']=data.actionStartTime;
                   payload['date']=data.date;
                   payload['deliveryPoint']=data.deliveryPoint;
                   payload['price']=data.price;
                   payload['uom']=data.uom;
                   payload['value']=data.value;
                   payload['ram'] = 0.0;
                   payload['cpu'] = 0.0;
                   payload['disk'] = 0.0;
                   payload['loadid'] = null;
                   $.ajax({
                       type: "PUT",
                       url: registeredMarket.url + MPLACE_URL+mplace+"/marketsessions/"+ses+"/actions/"+data.id+"/edit/",
                       contentType: "application/json",
                       data: JSON.stringify(payload),
                       beforeSend: function (xhr) {
                           xhr.setRequestHeader("Authorization", registeredMarket.token)
                       },
                       success: function () {
                          ancCustomRefresher();
                           },
                       error: function () {
                           ancCustomRefresher();
                       }
                   });
                }
            });
        }
        else {
            swal({
                title: "Delivery Failure",
                text: 'The service delivery failure report has been cancelled',
                type: "warning",
                confirmButtonText: "OK",
                confirmButtonColor: "#009245"
            });
        }
    });
}

function setServiceUnit() {
    if ($("#select-ancillary-service >option:selected").html() == "reactive_power_compensation") {
        $("#uoml").val('KVA');
        $('#uoml option:not(:selected)').attr('disabled', 'disabled').addClass('hidden');
    }
    else {
        $("#uoml").val('KW');
        $('#uoml option:not(:selected)').attr('disabled', 'disabled').addClass('hidden');
    }
}

function ancCustomRefresher(){

    var mplace = MPLACE;
    var ses_id;
    var url;
    var table;
    var non_selectable = [];
    if (is_dso == 1) {
        if ( $('#slct_offers_tab').hasClass('active') ) {
            ses_id = $("#select_balancing_session2").val();
            table = "#data_table_session_closed";
            url = registeredMarket.url + MPLACE_URL + mplace + "/marketsessions/" + ses_id + "/actions/";
            selectable_status = ["valid"];
        } else if ( $('#delivery_cnf_tab').hasClass('active') ) {
            ses_id = $("#select_balancing_session3").val();
            table = "#delivery_table";
            url = registeredMarket.url + MPLACE_URL + mplace + "/marketparticipants/"+username+"/dso/"+is_dso+"/marketsessions/"+ses_id+"/actions/";
            selectable_status = ["selected", "delivered", "not_delivered"];
        }
    } else {
        ses_id = SESSION.id;
        table = "#data_table_session_active";
        url = registeredMarket.url + MPLACE_URL + mplace + "/marketparticipants/"+username+"/dso/"+is_dso+"/marketsessions/"+ses_id+"/actions/";
    }

    console.log("Flexibility service, get actions: ");
    console.log(url);

    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    data = [];
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            success: function (response) {
                if ($.trim(response) != ''){
                    for (var i in response){
                        var act = {
                            id:response[i].id,
                            ancillaryService: service,
                            actionType: response[i].actionTypeid.type,
                            value: parseFloat(response[i].value).toFixed(3),
                            price: parseFloat(response[i].price).toFixed(2),
                            date: moment.utc(response[i].date).format(timeFormatStr),
                            actionStartTime: moment.utc(response[i].actionStartTime).format(timeFormatStr),
                            actionEndTime: moment.utc(response[i].actionEndTime).format(timeFormatStr),
                            deliveryPoint: response[i].deliveryPoint,
                            actionStatus: response[i].statusid.status,
                        }
                        data.push(act);
                        if ((table == "#data_table_session_closed" || table == "#delivery_table") && (selectable_status.includes(act['actionStatus']) == false)) {
                            non_selectable.push(act['id']);
                        }
                    }
                    $(table).bootstrapTable('load', data);
                    if (table == "#data_table_session_closed") {
                        $('#data_table_session_closed').find('input[type="checkbox"]').each(function () {
                            if (non_selectable.includes(parseInt($(this).val())) > 0) {
                                $(this).attr('disabled', 'disabled');
                            }
                        });
                    } else if (table == "#delivery_table") {
                        $("#delivery_table").find('.edit').each(function () {
                            if (non_selectable.includes(parseInt($(this).attr('id'))) > 0) {
                                $(this).attr( "onclick", null );
                                $(this).attr( "href", null );
                                $(this).empty();
                                $(this).text("N/A");
                            }
                        });
                    }
            }else{
                   $(table).bootstrapTable('removeAll');
                }
            }
        });
}


 function getServiceName(form){

            if(form == "reactive_power_compensation"){
                return "Reactive power compensation";
            } else if(form == "congestion_management"){
                return "Congestion management";
            } else if(form == "spinning_reserve" ){
               return "Spinning reserve";
            } else if(form == "scheduling"){
               return "Scheduling";
            } else{
                return form;
            }

}


