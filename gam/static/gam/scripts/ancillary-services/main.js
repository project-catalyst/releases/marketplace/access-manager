$(document).ready(function () {

    var data = [
        {
            state: 0,
            id: 12432,
            date: "2016-04-18",
            actionStartTime: "2016-04-18 09:00",
            actionEndTime: "2016-04-18 16:00",
            value: 12,
            price: 20,
            actionType: "???",
            deliveryPoint: "2016-04-19 08:00",
            actionStatus: "test",
            edit: 12432
        }
    ];

    $("#gam_table").bootstrapTable({ data: data });
    $('.fixed-table-body').css('height', 'auto');

}).on('all.bs.table', "#gam_table", function (name, args) {
    $("td").each(function () {
        $(this).css('vertical-align', 'middle').attr('role', 'navigation');
    });
});