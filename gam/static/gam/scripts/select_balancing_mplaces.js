registeredMarket = getUserRegisteredMarketByForm(user_markets, form);
var MPLACE = getFlexMarket(timeframe);

var SESSION;
var ref_price;
var statusSessions;
var registeredMarket;
var actor_id;

if (is_dso == 1) {
    if ( $('#slct_offers_tab').hasClass('active') ) {
        getSessionsbyStatus('closed','#select_balancing_session2');

    } else if ( $('#delivery_cnf_tab').hasClass('active') ) {
        getSessionsbyStatus('cleared','#select_balancing_session3');
    }
    $("#slct_offers_tab").on("click", function(){
        getSessionsbyStatus('closed','#select_balancing_session2');
    });
    $('#delivery_cnf_tab').on("click", function() {
        getSessionsbyStatus('cleared','#select_balancing_session3');
    });
} else {

    console.log("### Registered Market: ");
    console.log(registeredMarket);

    SESSION = getActiveSessionsofMarket();
    ref_price = getReferencePrice();
    setFlexMarketSessionInfo();

    console.log("Registered market: ");
    console.log(registeredMarket);
}


function getFlexMarket(timeframe) {

    var mplace_flex;
    $.ajax({
        type: "GET",
        url: registeredMarket.url + SELECT_MPLACE+"ancillary-services/"+ timeframe + "/" + username + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
            if (response.length > 1) {
                swal({
                       title: "Error",
                       text: 'Multiple marketplaces received!',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
                return false;
            } else if(response.length === 0) {

                   swal({
                       title: "Error",
                       text: 'You are not a participant in any market of this type! \n You will be redirected to home',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   },
                     function (isConfirm) {
                        if(isConfirm){
                          location.href = "/";
                        }
                     });
            }
            else {
                mplace_flex = response[0];
            }
        },
        error: (function() {alert( "Error in retrieving Flexibility Marketplace" ); })
    });
    return mplace_flex;
}

function getSessionsbyStatus(status, session_btn) {
    $(session_btn).find('option').remove().end();
    $.ajax({
        type: "GET",
        url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/form/" + service + "/" + status + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
            $(session_btn).find('option').remove().end();
            $(session_btn).append($("<option></option>").attr("value", -1).text("-- Select a MarketSession --"));
            if ($.trim(response) != '') {
                for (var i in response) {
                    statusSessions = response;
                    $(session_btn)
                        .append($("<option></option>")
                            .attr("value", response[i].id)
                            .text("Marketsession #" + response[i].id));
                    if (i == 0) {
                        $(session_btn).val(response[i].id);
                        if (status == 'closed') {
                            setClosedMarketSessionInfo();
                        } else if (status == 'cleared') {
                            setClearedMarketSessionInfo();
                        }
                    }
                }
            } else {
                statusSessions = null;
                swal({
                       title: "Info",
                       text: 'No ' + status + ' sessions for this marketplace!',
                       type: "info",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
                $(session_btn)
                    .append($("<option></option>")
                        .attr("value", -2)
                        .text("No " + status + " sessions"));
            }
        }
    });
}

function getActiveSessionsofMarket() {

    console.log("get Active sessions of market: ");
    console.log(registeredMarket);
    console.log(registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/form/" + service + "/active/");
    
    var ses;
    $('#active_sessions_helper_bal').css("display", "none");

    $("#session_helper_start").remove();
    $("#session_helper_end").remove();
    $.ajax({
        type: "GET",
        url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/form/" + service + "/active/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
            if (response.length > 1) {
                    swal({
                       title: "Error",
                       text: 'Multiple market sessions received!',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                    });
                    ses = null;
                    return ses;
            }
            if ($.trim(response) != '') {
                ses = response[0];
            } else {
                ses = null;
                swal({
                       title: "Info",
                       text: 'No active sessions for this marketplace!',
                       type: "info",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
            }
        },
        error: function () {console.log(response);}
    });
    return ses;
}

function getReferencePrice(){
    var rprice;

    if (SESSION != null) {
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/form/" + getSessionFormForm(registeredMarket.url + MPLACE_URL + MPLACE +"/marketsessions/" + SESSION.id + "/", MPLACE, SESSION.id) + "/date/now/referencePrice/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            success: function (response) {
                if ($.trim(response) != '') {
                    rprice = response;
                    $("span#reference_price").text(rprice.price);
                } else {
                    rprice = null;
                }
            },
            error: function () {
                rprice = null;
            }
        });
    }
    return rprice;

}
function setFlexMarketSessionInfo() {

    $("#for_table").css('style', 'none');
    $("#active_sessions_helper_bal").attr('style', '');

    $("#session_helper_start").remove();
    $("#session_helper_end").remove();
    var tmp;
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    var data = [];

    if (SESSION != null) {
        $("#for_table").attr('style', '');
        $("#session_helper_start").remove();
        $("#session_helper_end").remove();
        var div1 = '<div id="session_helper_start">Session is active from ' + moment.utc(SESSION.sessionStartTime).format('YYYY-MM-DD HH:mm ZZ') + ' until <b>' + moment.utc(SESSION.sessionEndTime).format('YYYY-MM-DD HH:mm ZZ') + '</b></div>';
        var div2 = '<div id="session_helper_end">Delivery is active from ' + moment.utc(SESSION.deliveryStartTime).format('YYYY-MM-DD HH:mm ZZ') + ' until ' + moment.utc(SESSION.deliveryEndTime).format('YYYY-MM-DD HH:mm ZZ') + '</div>';
        if (ref_price != null) {

            if(service == "reactive_power_compensation") {
                var div3 = '<div id="rprice_helper">Reference price is ' + parseFloat(ref_price.price).toFixed(2).replace('.', ',') + '/kVA.</div>';
            } else {
                var div3 = '<div id="rprice_helper">Reference price is ' + parseFloat(ref_price.price).toFixed(2).replace('.', ',') + '/kW.</div>';
            }
        } else {
            var div3 = '<div id="rprice_helper">Reference price: Not set</div>';
        }
        $("#date_holder").append(div1);
        $("#date_holder").append(div2);
        $("#date_holder").append(div3);
        $("#data_table_session_active").bootstrapTable();
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + SESSION.id + "/actions/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: true,
            success: function (response) {
                if ($.trim(response) != '') {
                    for (var i in response) {

                        var act = {
                            id: response[i].id,
                            ancillaryService: service,
                            actionType: response[i].actionTypeid.type,
                            value: parseFloat(response[i].value).toFixed(3),
                            price: parseFloat(response[i].price).toFixed(2),
                            date: moment(response[i].date).format(timeFormatStr),
                            actionStartTime: moment(response[i].actionStartTime).format(timeFormatStr),
                            actionEndTime: moment(response[i].actionEndTime).format(timeFormatStr),
                            deliveryPoint: response[i].deliveryPoint,
                            actionStatus: response[i].statusid.status,
                            participant: response[i].marketActorid.id
                        };

                        data.push(act);
                    }
                    $("#data_table_session_active").bootstrapTable('load', data);
                } else {
                    $("#data_table_session_active").bootstrapTable('removeAll');
                }
            }
        });
    } else {
        $("#data_table_session_active").bootstrapTable();
        $("#session_helper_start").remove();
        $("#session_helper_end").remove();
    }

}

function valueFormatter(value, row, index) {
    return parseFloat(value).toFixed(3);
}

function priceFormatter(value, row, index) {
    return parseFloat(value).toFixed(2);
}

function serviceFormatter(value, row, index) {
    if (value == "reactive_power_compensation") {
        return "Reactive power compensation";
    } else if (value == "spinning_reserve") {
        return "Spinning Reserve";
    } else if (value == "congestion_management"){
        return "Congestion management";
    } else if(value == "scheduling"){
          return "Scheduling";
    }
}

function typeFormatter(value, row, index) {
    if (value == "bid") {
        return "<div style=\"color:red;\">Bid</div>";
    } else {
        return "<div style=\"color:green;\">Offer</div>";
    }
}

function statusFormatter(value, row, index) {
    if (value == 'unchecked') {
        return "<span class=\"label label-default\">Unchecked</span>";
    } else if (value == 'valid') {
        return "<span class=\"label label-info\">Valid</span>";
    } else if (value == 'invalid') {
        return "<span class=\"label label-danger\">Invalid</span>";
    } else if (value == 'active') {
        return "<span class=\"label label-info\">Active</span>";
    } else if (value == 'inactive') {
        return "<span class=\"label label-warning\">Inactive</span>";
    } else if (value == 'withdrawn') {
        return "<span class=\"label label-default\">Withdrawn</span>";
    } else if (value == 'accepted') {
        return "<span class=\"label label-success\"></i> Accepted</span>";
    } else if (value == 'partially_accepted') {
        return "<span class=\"label label-warning\">Partially Accepted</span>";
    } else if (value == 'rejected') {
        return "<span class=\"label label-danger\">Rejected</span>";
    } else if (value == 'selected') {
        return "<span class=\"label label-info\">Selected</span>";
    } else if (value == 'delivered') {
        return "<span class=\"label label-primary\">Delivered</span>";
    } else if (value == 'not_delivered') {
        return "<span class=\"label label-warning\">Not delivered</span>";
    } else {
        return "<span class=\"label label-info\">Billed</span>";
    }
}

function manageRulesFormatter(value, row, index) {
    return [
        '<a class="edit" href="javascript:void(0)" title="Edit" onclick="editAction(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>  ',
        '<a class="remove" href="javascript:void(0)" title="Remove" onclick="removeAction(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-trash" style="color:red;"></i>',
        '</a>'
    ].join('');
}

function deliveredFormatter(value, row, index) {
    if (row['actionStatus'] == 'delivered') {
        return '<i class="glyphicon glyphicon-ok-circle text-success"></i>';
    }
    else {
        return [
            '<a class="edit" id=' + row['id'] + ' class="disabled" href="javascript:void(0)" title="Confirm service delivery" onclick="confirmDelivery(' + row['id'] + ');">',
            '<i class="glyphicon glyphicon-shopping-cart"></i>',
            '</a>  ',
        ].join('');
    }
}

function notDeliveredFormatter(value, row, index){
    if (row['actionStatus'] == "not_delivered") {
        return '<i class="glyphicon glyphicon-ok-circle text-danger"></i>';
    }
    else {
        return [
            '<a class="edit" id=' + row['id'] + ' class="disabled" href="javascript:void(0)" title="Report that the service was not delivered" onclick="referNotDelivery(' + row['id'] + ');">',
            '<i class="glyphicon glyphicon-bullhorn"></i>',
            '</a>  ',
        ].join('');
        }
}


function setClosedMarketSessionInfo() {
    $("#closed_sessions_helper").attr('style', '');
    var ses_id = $("#select_balancing_session2").val();
    var tmp;
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    var data = [];
    var non_selectable = [];
    if (statusSessions != null) {

        for (var i in statusSessions) {
            if (ses_id == statusSessions[i].id) {
                tmp = statusSessions[i];
            }
        }
        $("#data_table_session_closed").bootstrapTable();
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + ses_id + "/actions/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            success: function (response) {
                if ($.trim(response) != '') {
                    for (var i in response) {
                        var act = {
                            id: response[i].id,
                            ancillaryService: service,
                            actionType: response[i].actionTypeid.type,
                            value: parseFloat(response[i].value).toFixed(3),
                            price: parseFloat(response[i].price).toFixed(2),
                            date: moment(response[i].date).format(timeFormatStr),
                            actionStartTime: moment(response[i].actionStartTime).format(timeFormatStr),
                            actionEndTime: moment(response[i].actionEndTime).format(timeFormatStr),
                            deliveryPoint: response[i].deliveryPoint,
                            actionStatus: response[i].statusid.status,
                            participant: response[i].marketActorid.id
                        }
                        data.push(act);
                        if (act['actionStatus'] != 'valid') {
                            non_selectable.push(act['id']);
                        }
                    }
                    $("#data_table_session_closed").bootstrapTable('load', data);
                    $('#data_table_session_closed').find('input[type="checkbox"]').each(function () {
                        if (non_selectable.includes(parseInt($(this).val())) > 0) {
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("#data_table_session_closed").bootstrapTable('removeAll');
                }
            }
        });
    } else {
        $("#data_table_session_closed").bootstrapTable();
    }

}



function setClearedMarketSessionInfo() {
    $("#cleared_sessions_helper").attr('style', '');
    var ses_id = $("#select_balancing_session3").val();
    var tmp;
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    var data = [];
    var non_selectable = [];
    var selectable_status = ["selected", "delivered", "not_delivered"];
    if (statusSessions != null) {

        for (var i in statusSessions) {
            if (ses_id == statusSessions[i].id) {
                tmp = statusSessions[i];
            }
        }
        $("#delivery_table").bootstrapTable();
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + tmp.id + "/actions/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: true,
            success: function (response) {
                if ($.trim(response) != '') {
                    for (var i in response) {

                        console.log("Action: " + i);
                        console.log(response[i]);
                        var act = {
                            id: response[i].id,
                            ancillaryService: service,
                            actionType: response[i].actionTypeid.type,
                            value: parseFloat(response[i].value).toFixed(3),
                            price: parseFloat(response[i].price).toFixed(2),
                            date: moment(response[i].date).format(timeFormatStr),
                            actionStartTime: moment(response[i].actionStartTime).format(timeFormatStr),
                            actionEndTime: moment(response[i].actionEndTime).format(timeFormatStr),
                            deliveryPoint: response[i].deliveryPoint,
                            actionStatus: response[i].statusid.status,
                            participant: response[i].marketActorid.id

                        }
                        data.push(act);
                        if (selectable_status.includes(act['actionStatus']) == false) {
                            non_selectable.push(act['id']);
                        }
                    }
                    $("#delivery_table").bootstrapTable('load', data);
                    $("#delivery_table").find('.edit').each(function () {
                            if (non_selectable.includes(parseInt($(this).attr('id'))) > 0) {
                                $(this).attr( "onclick", null );
                                $(this).attr( "href", null );
                                $(this).empty();
                                $(this).text("N/A");
                            }
                        });
                } else {
                    $("#delivery_table").bootstrapTable('removeAll');
                }
            }
        });
    } else {
        $("#delivery_table").bootstrapTable();
    }
}


