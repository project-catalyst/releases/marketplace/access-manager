from datetime import datetime
from urllib.parse import urljoin

import requests, json
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.conf import settings
from traceback import print_exc

# Create your views here.
from app.decorators import login_required_apiview

ANCILLARY_SERVICES = ["congestion_management", "spinning_reserve", "reactive_power_compensation", "scheduling"]


@login_required_apiview
def marketPlaces(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    user_markets = json.loads(request.session.get("user_markets", None))
    market = user_markets[0]

    if request.method == "GET":
        forms_url = market["url"] + "/form/"
        response = requests.get(forms_url,
                                headers={"Content-Type": "application/json", "Authorization": token})
        if response.status_code == 200:
            forms = response.json()
        else:
            forms = []
        print(forms)
        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'forms': forms,
            'title': 'Flexibility',
            'year': datetime.now().year,
            'user_markets':  json.dumps(request.session.get("user_markets", None))

        }
        return render(request, 'gam/marketplaces.html', context)


def flexibilityConfiguration(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    print("flexibility configuration")
    if request.method == "GET":
        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'Flexibility',
            'year': datetime.now().year,
            'user_markets':  json.dumps(request.session.get("user_markets", None))

        }
        return render(request, 'gam/flexibilityConfigureParameters.html', context)

@login_required_apiview
def congestionManagement(request):
    if request.method == "GET":
        return realTime(request, "congestion_management")


@login_required_apiview
def spinningReserve(request):
    if request.method == "GET":
        return realTime(request, "spinning_reserve")


@login_required_apiview
def reactivePowerCompensation(request):
    if request.method == "GET":
        return realTime(request, "reactive_power_compensation")

@login_required_apiview
def scheduling(request):
    if request.method == "GET":
        return realTime(request, "scheduling")


@login_required_apiview
def congestionManagementNoTimeframe(request):
    if request.method == "GET":
        return serviceNoTimeframe(request, "congestion_management", "Congestion management")

@login_required_apiview
def congestionManagementDayAhead(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "congestion_management", "day_ahead")

@login_required_apiview
def congestionManagementIntraDay(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "congestion_management", "intra_day")


@login_required_apiview
def spinningReserveNoTimeframe(request):
    if request.method == "GET":
        return serviceNoTimeframe(request, "spinning_reserve", "Spinning reserve")

@login_required_apiview
def spinningReserveDayAhead(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "spinning_reserve", "day_ahead")

@login_required_apiview
def spinningReserveIntraDay(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "spinning_reserve", "intra_day")


@login_required_apiview
def reactivePowerCompensationNoTimeframe(request):
    if request.method == "GET":
        return serviceNoTimeframe(request, "reactive_power_compensation", "Reactive power compensation")

@login_required_apiview
def reactivePowerCompensationDayAhead(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "reactive_power_compensation", "day_ahead")

@login_required_apiview
def reactivePowerCompensationIntraDay(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "reactive_power_compensation", "intra_day")

@login_required_apiview
def schedulingNoTimeframe(request):
    if request.method == "GET":
        return serviceNoTimeframe(request, "scheduling", "Scheduling")

@login_required_apiview
def schedulingDayAhead(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "scheduling", "day_ahead")

@login_required_apiview
def schedulingIntraDay(request):
    if request.method == "GET":
        return serviceWithTimeframe(request, "scheduling", "intra_day")


def realTime(request, service):
    """ Load Ancillary Services Marketplace"""

    if request.method == "GET":
        try:
            assert isinstance(request, HttpRequest)
            token = request.session["token"]
            context = {
                'service': service,
                'is_dso': request.session['is_dso'],
                'username': request.session["username"],
                'token': token,
                'actor': json.dumps(request.session.get("actor", None)),
                'title': 'Flexibility',
                'user_markets':  json.dumps(request.session.get("user_markets", None)),
                'year': datetime.now().year
            }
            return render(request, 'gam/service.html', context)
        except:
            if settings.DEBUG:
                print_exc()
            return HttpResponse(status=400)


def serviceNoTimeframe(request, service, service_name):
    """ Load page for a specific service, without selecting a timeframe """

    if request.method == "GET":
        try:
            assert isinstance(request, HttpRequest)
            token = request.session["token"]
            context = {
                'service': service,
                'service_name': service_name,
                'is_dso': request.session['is_dso'],
                'username': request.session["username"],
                'token': token,
                'actor': json.dumps(request.session.get("actor", None)),
                'title': 'Flexibility',
                'user_markets':  json.dumps(request.session.get("user_markets", None)),
                'year': datetime.now().year
            }
            return render(request, 'gam/serviceWithoutTimeframe.html', context)
        except:
            if settings.DEBUG:
                print_exc()
            return HttpResponse(status=400)

def serviceWithTimeframe(request, service, timeframe):
    """ Load Ancillary Services Marketplace"""

    if request.method == "GET":
        try:
            assert isinstance(request, HttpRequest)
            token = request.session["token"]
            context = {
                'service': service,
                'timeframe': timeframe,
                'is_dso': request.session['is_dso'],
                'username': request.session["username"],
                'token': token,
                'actor': json.dumps(request.session.get("actor", None)),
                'title': 'Flexibility',
                'user_markets':  json.dumps(request.session.get("user_markets", None)),
                'year': datetime.now().year
            }
            return render(request, 'gam/service.html', context)
        except:
            if settings.DEBUG:
                print_exc()
            return HttpResponse(status=400)


@login_required_apiview
def ancillaryHistory(request):
    if request.method == "GET":
        try:
            assert isinstance(request, HttpRequest)
            token = request.session["token"]
            context = {
                'actor': json.dumps(request.session.get("actor", None)),
                'username': request.session["username"],
                'token': token,
                'title': 'Flexibility - History',
                'user_markets':  json.dumps(request.session.get("user_markets", None)),
                'year': datetime.now().year
            }
            return render(request, 'gam/ancillaryHistory.html', context)
        except:
            if settings.DEBUG:
                from traceback import print_exc
                print_exc()
            return HttpResponse(status=400)


@login_required_apiview
def ancillaryInvoice(request):
    """Displays the GEM invoices"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    context = {
        'username': request.session["username"],
        'actor_id': request.session["actor_id"],
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Flexibility - Invoices',
        'user_markets':  json.dumps(request.session.get("user_markets", None)),
        'form': 'ancillary-services'
    }
    return render(request, 'gam/ancillaryInvoices.html', context)
