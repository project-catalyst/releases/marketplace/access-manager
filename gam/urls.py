from django.conf.urls import url

from gam import views

urlpatterns = [
    url(r'^marketplaces/ancillary/$', views.marketPlaces, name='ancillary_marketplaces'),
    # url(r'^marketplaces/ancillary/real_time/congestion_management/$', views.congestionManagement, name='congestion_management_service'),
    # url(r'^marketplaces/ancillary/real_time/spinning_reserve/$', views.spinningReserve, name='spinning_reserve_service'),
    # url(r'^marketplaces/ancillary/real_time/reactive_power_compensation/$', views.reactivePowerCompensation, name='reactive_power_compensation_service'),
    # url(r'^marketplaces/ancillary/real_time/scheduling/$', views.scheduling, name='scheduling_service'),

    url(r'^marketplaces/ancillary/congestion_management/$', views.congestionManagementNoTimeframe, name='congestion_management_service_no_timeframe'),
    url(r'^marketplaces/ancillary/spinning_reserve/$', views.spinningReserveNoTimeframe, name='spinning_reserve_service_no_timeframe'),
    url(r'^marketplaces/ancillary/reactive_power_compensation/$', views.reactivePowerCompensationNoTimeframe, name='reactive_power_compensation_service_no_timeframe'),
    url(r'^marketplaces/ancillary/scheduling/$', views.schedulingNoTimeframe, name='scheduling_service_no_timeframe'),

    url(r'^marketplaces/ancillary/day_ahead/congestion_management/$', views.congestionManagementDayAhead, name='congestion_management_service_day_ahead'),
    url(r'^marketplaces/ancillary/intra_day/congestion_management/$', views.congestionManagementIntraDay, name='congestion_management_service_intra_day'),
    url(r'^marketplaces/ancillary/day_ahead/spinning_reserve/$', views.spinningReserveDayAhead, name='spinning_reserve_service_day_ahead'),
    url(r'^marketplaces/ancillary/intra_day/spinning_reserve/$', views.spinningReserveIntraDay, name='spinning_reserve_service_intra_day'),
    url(r'^marketplaces/ancillary/day_ahead/reactive_power_compensation/$', views.reactivePowerCompensationDayAhead, name='reactive_power_compensation_service_day_ahead'),
    url(r'^marketplaces/ancillary/intra_day/reactive_power_compensation/$', views.reactivePowerCompensationIntraDay, name='reactive_power_compensation_service_intra_day'),
    url(r'^marketplaces/ancillary/day_ahead/scheduling/$', views.schedulingDayAhead, name='scheduling_service_day_ahead'),
    url(r'^marketplaces/ancillary/intra_day/scheduling/$', views.schedulingIntraDay, name='scheduling_service_intra_day'),

    url(r'^marketplaces/history/ancillary/$', views.ancillaryHistory, name='history_ancillary'),
    url(r'^marketplaces/invoices/ancillary/$', views.ancillaryInvoice, name='invoices_ancillary'),

    url(r'^marketplaces/ancillary/configuration/$', views.flexibilityConfiguration, name='flexibility_configuration'),
]
