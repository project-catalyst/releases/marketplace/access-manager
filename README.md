# CATALYST Access Manager

### Installation

Before building the container, you should tell the Access Manager where the Information Broker is deployed. To do this, edit the ```.env``` file and set the ```INFORMATION_BROKER_URL``` variable, by setting the IP address where the Information listens to (the default port that the Information Broker listens to is 80, so it can be ommitted). If both the Access Manager and the Information Broker are deployed in the same machine, then this IP should be the IP of the machine and definitely not ```localhost```. Then you are ready to build and launch the container.

In short, the following commands should be executed:

```bash
cd access-manager/
vim .env ## To set the Information Broker connection settings
sudo docker-compose -f config/docker-compose.yml up -d --build
```

Now, you should be able to access CATALYST Access Manager, running on port `80`.

### How to install Docker under Ubuntu

To install docker / docker-compose in an ubuntu 16.04 system, run the following as root:

```bash
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
sudo chmod +x /usr/local/bin/docker-compose
```

### Docker-compose Installation

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose