from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


def login_required_view(View):
    """
    Check if the user is connected
    Decorator for class-based views 
    """
    View.dispatch = method_decorator(login_required)(View.dispatch)
    return View


def login_required_apiview(function):
    """ Check if user is logged in"""

    def wrap(request, *args, **kwargs):
        path = request.build_absolute_uri()
        try:
            if request.session["is_authenticated"]:
                return function(request, *args, **kwargs)
            else:
                return redirect_to_login(next=path, login_url=reverse_lazy('login'))
        except Exception as e:
            print(e)
            return redirect_to_login(next=path, login_url=reverse_lazy('login'))
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
