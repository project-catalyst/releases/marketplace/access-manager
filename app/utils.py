﻿from django.conf import settings
from pytz import timezone
from django.core.mail import EmailMultiAlternatives, send_mail
from traceback import print_exc


def setUTCFormatter(date_time):
    format = "%Y-%m-%d %H:%M %z"
    datetime_obj_utc = date_time.replace(tzinfo=timezone('UTC'))
    return datetime_obj_utc.strftime(format)


def sendEmail(receiversList, subject, content, includeHtml):
    try:
        sender = settings.EMAIL_HOST_USER
        format = "text/html"

        # send email with HTML code
        if includeHtml == True:
            msg = EmailMultiAlternatives(subject, "", sender, receiversList)
            msg.attach_alternative(content, format)
            msg.send()
        else:
            send_mail(subject, content, sender, receiversList, fail_silently=False)
    except:
        if settings.DEBUG:
            print_exc()
        pass
