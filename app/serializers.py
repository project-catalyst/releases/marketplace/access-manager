from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_204_NO_CONTENT, \
    HTTP_301_MOVED_PERMANENTLY, HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, \
    HTTP_500_INTERNAL_SERVER_ERROR

from app.models import ActionStatus, ActionType, Form, TimeFrame, MarketServiceType, MarketActorType, MarketActor, \
    Marketplace, SessionStatus, MarketSession, MarketAction, MarketActionCounterOffer, Transaction, Invoice, \
    Marketplace_has_MarketActor, ElectricityPrice, ReferencePrice, Rule, UserActorRel, SG_Consumption, SG_Generation


class ActionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionStatus
        fields = ('id', 'status',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ActionTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionType
        fields = ('id', 'type',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = ('id', 'Form',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class TimeFrameSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeFrame
        fields = ('id', 'type')
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketServiceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketServiceType
        fields = ('id', 'type', 'TimeFrame_id')
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActorTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActorType
        fields = ('id', 'type')
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActor
        fields = ('id', 'companyName', 'email', 'MarketActorType_id', 'phone', 'representativeName', 'VAT')
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

        def update(self, instance, validated_data):
            instance.id = validated_data.get('id', instance.id)
            instance.companyName = validated_data.get('companyName', instance.companyName)
            instance.email = validated_data.get('email', instance.email)
            instance.MarketActorType_id = validated_data.get('MarketActorType_id', instance.MarketActorType_id)
            instance.phone = validated_data.get('phone', instance.phone)
            instance.representativeName = validated_data.get('representativeName', instance.representativeName)
            instance.VAT = validated_data.get('VAT', instance.VAT)
            return instance


class MarketActorSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketActor
        fields = ('id', 'companyName', 'email', 'MarketActorType_id', 'phone', 'representativeName', 'VAT')
        # depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketPlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marketplace
        fields = ('id', 'MarketOperator_id', 'MarketServiceType_id', 'DSO_id',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketPlaceSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Marketplace
        fields = ('id', 'MarketOperator_id', 'MarketServiceType_id', 'DSO_id',)
        # depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


# class MarketplaceSerializer(VersionedSerializers):
#     """
#     Changelog:
#
#     * **v1.0**: `title` is optional
#     * **v2.0**: `title` is required
#     """
#
#     VERSION_MAP = (
#         ('<2.0', MarketPlaceSerializer),
#     )


class SessionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = SessionStatus
        fields = ('id', 'status',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketSession
        fields = ('id', 'sessionStartTime', 'sessionEndTime',
                  'deliveryStartTime', 'deliveryEndTime', 'Marketplace_id', 'SessionStatus_id', 'Form_id',
                  'clearingPrice',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketSessionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketSession
        fields = ('id', 'sessionStartTime', 'sessionEndTime',
                  'deliveryStartTime', 'deliveryEndTime', 'Marketplace_id', 'SessionStatus_id', 'Form_id',
                  'clearingPrice',)
        # depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = MarketAction
        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid', 'vCPU', 'vRAM', 'vDisk',
            'marketActorid', 'formid', 'actionTypeid', 'statusid',)
        # depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketAction
        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid', 'vCPU', 'vRAM', 'vDisk',
            'marketActorid', 'formid', 'actionTypeid', 'statusid',)
        depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

class MarketActionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = MarketAction
        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid', 'vCPU', 'vRAM', 'vDisk',
            'marketActorid', 'formid', 'actionTypeid', 'statusid',)
        #depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionEditSingleSerializer(serializers.Serializer):
    date = serializers.DateTimeField()
    actionStartTime = serializers.DateTimeField()
    actionEndTime = serializers.DateTimeField()
    value = serializers.DecimalField(max_digits=10, decimal_places=3)
    vCPU = serializers.DecimalField(max_digits=10, decimal_places=3)
    vRAM = serializers.DecimalField(max_digits=10, decimal_places=3)
    vDisk = serializers.DecimalField(max_digits=10, decimal_places=3)
    uom = serializers.CharField()
    price = serializers.DecimalField(max_digits=10, decimal_places=3)
    deliveryPoint = serializers.CharField()
    marketSessionid = serializers.IntegerField()
    marketActorid = serializers.IntegerField()
    actionType = serializers.CharField()
    status = serializers.CharField()

    class Meta:
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

class MarketActionCounterOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActionCounterOffer
        fields = ('MarketAction_Bid_id', 'MarketAction_Offer_id', 'exchangedValue',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('id', 'dateTime', 'MarketActionCounterOffer_id',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class TransactionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('id', 'dateTime', 'MarketActionCounterOffer_id',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('id', 'date', 'penalty', 'amount', 'fixedFee', 'Transaction_id',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class UserMarketSerializer(serializers.Serializer):
    type = serializers.CharField(max_length=200)
    form = serializers.CharField(max_length=200)
    url = serializers.CharField(max_length=200)
    id = serializers.IntegerField()
    component = serializers.CharField(max_length=200)
    actorId = serializers.IntegerField(required=False)



class Marketplace_has_MarketActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marketplace_has_MarketActor
        fields = ('id', 'Marketplace_id', 'MarketActor_id',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ElectricityPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElectricityPrice
        fields = ('id', 'price',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ReferencePriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferencePrice
        fields = ('id', 'price', 'validityStartTime', 'validityEndTime', 'Form_id', 'Marketplace_id',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class RuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rule
        fields = ('id', 'title', 'description', 'timestamp', 'value',)


class GeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = None


class UserActorRelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserActorRel
        fields = ('user', 'actor', 'status',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'id', 'first_name', 'last_name', 'email')


class SG_Consumption_serializer(serializers.ModelSerializer):
    Customer_Number = serializers.CharField(max_length=30)
    Customer_Name = serializers.CharField(max_length=64)
    Date = serializers.CharField(max_length=10)
    Measuring_Period_Duration = serializers.CharField(max_length=5)
    Value_Identifier = serializers.CharField(max_length=10)
    Unique_cut_No = serializers.CharField(max_length=5)  # change the . to _
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _
    Device_ID = serializers.CharField(max_length=10)
    Measurements = serializers.ListField(child=serializers.CharField(max_length=16))
    Port_Number = serializers.CharField(max_length=5)
    Unit = serializers.CharField(max_length=10)
    Channel_Name = serializers.CharField(max_length=10)
    Timestamp = serializers.CharField(max_length=8)
    Transformer_Ratio = serializers.CharField(max_length=5)

    class Meta:
        model = SG_Consumption
        fields = (
            'Customer_Number', 'Customer_Name', 'Date', 'Measuring_Period_Duration', 'Value_Identifier',
            'Unique_cut_No',
            'Unique_CHA_No', 'Device_ID', 'Measurements', 'Port_Number', 'Unit', 'Channel_Name', 'Timestamp',
            'Transformer_Ratio')


class SG_ConsumptionChaSerializer(serializers.ModelSerializer):
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _

    class Meta:
        model = SG_Consumption
        fields = ('Unique_CHA_No',)


class SG_Generation_serializer(serializers.ModelSerializer):
    Customer_Number = serializers.CharField(max_length=30)
    Customer_Name = serializers.CharField(max_length=64)
    Date = serializers.CharField(max_length=10)
    Measuring_Period_Duration = serializers.CharField(max_length=5)
    Value_Identifier = serializers.CharField(max_length=10)
    Unique_cut_No = serializers.CharField(max_length=5)  # change the . to _
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _
    Device_ID = serializers.CharField(max_length=10)
    Measurements = serializers.ListField(child=serializers.CharField(max_length=16))
    Port_Number = serializers.CharField(max_length=5)
    Unit = serializers.CharField(max_length=10)
    Channel_Name = serializers.CharField(max_length=10)
    Timestamp = serializers.CharField(max_length=8)
    Transformer_Ratio = serializers.CharField(max_length=5)

    class Meta:
        model = SG_Generation
        fields = (
            'Customer_Number', 'Customer_Name', 'Date', 'Measuring_Period_Duration', 'Value_Identifier',
            'Unique_cut_No',
            'Unique_CHA_No', 'Device_ID', 'Measurements', 'Port_Number', 'Unit', 'Channel_Name', 'Timestamp',
            'Transformer_Ratio')


class SG_GenerationChaSerializer(serializers.ModelSerializer):
    Unique_CHA_No = serializers.CharField(max_length=64)

    class Meta:
        model = SG_Generation
        fields = ('Unique_CHA_No',)
