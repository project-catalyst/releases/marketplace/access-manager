﻿
var actorId;

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            xhr.setRequestHeader("X-CSRFToken", new Cookies().getValue('csrftoken'));
        }
    }
});

$(document).ready(function () {
    actorId = getActorId(registeredMarket.url, USERNAME_TO_ACTOR, registeredMarket.token, username);
    
    getInvoices();
    setInterval(function () {
        getInvoices();
    }, 180000);

}).on('click', ".view-market-action", function () {
    console.log(registeredMarket.token);
    getMarketAction($(this).data('pk'),registeredMarket.token);
});

function setPriceFormatter(value, row, index) {
    return Number(value).toFixed(2).toString().replace('.', ',');
}

function setValueFormatter(value, row, index) {
    return Number(value).toFixed(3).toString().replace('.', ',');
}

function setActionFormatter(value, row, index) {
    return [
        '<a href="#" data-pk="' + value + '" class="view-market-action" style="color:dodgerblue">',
        value,
        '</a>'
    ].join('');

}

function setTypeFormatter(value, row, index) {
    if (value === "bid") {
        return '<span style="color:green;">Bid</span>';
    }
    else {
        return '<span style="color:red;">Offer</span>';
    }
}

function datetimeUTCFormatter(datetime) {
    // momment.js
    return moment(datetime).utc().format("YYYY-MM-DD HH:mm ZZ");
}

function getInvoices() {
    console.log("Get all invoices from: ");
    console.log(registeredMarket.url + INVOICES_URL + actorId+"/"+username+"/");

    $.ajax({
        type: "GET",
        url: registeredMarket.url + INVOICES_URL + actorId+"/"+username+"/",
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        success: function (response) {
            console.log("Invoices good:");
            console.log(response);
            $("#invoices_table").bootstrapTable('destroy').bootstrapTable({data: response.invoices});
        },
        error: function (response) {
            console.log("Invoices error");
            console.log(response);
        }
    });
}


function getMarketAction(pk, token) {

    console.log('Form: ', form);
    console.log("Get market action details from:");
    console.log(registeredMarket.url + ACTION_URL + pk + '/');

    var isFormItLoad = (form === "it_load");
    var isFormThermalEnergy = (form === "thermal_energy");

    $.ajax({
        type: "GET",
        url: registeredMarket.url + ACTION_URL + pk + '/',
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token);
        },
        success: function (action) {
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';
            var price = action.price.replace('.', ',') + ' &#8364;/MWh';
            if (isFormItLoad)
                price = action.price.replace('.', ',') + ' &#8364;';
            if (isFormThermalEnergy)
                price = action.price.replace('.', ',') + ' &#8364;/MWht';
            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Type</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + action.formid.form + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Price</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + price + '</span>',
                '</div>',
                '</div>',
                '<div id="quantity" class="form-group clearfix">',
                '<div  class="' + lclass + '">',
                '<label class="pull-right"><strong>Quantity</strong></label>',
                '</div>',
                '<div  class="' + fclass + '">',
                '<span class="pull-left">' + action.value.replace('.', ',') + ' ' + action.uom + '</span>',
                '</div>',
                '</div>',
                '<div id="cpu" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>CPU</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.cpu + '</span>',
                '</div>',
                '</div>',
                '<div id="ram" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>RAM</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.ram + ' Mb' + '</span>',
                '</div>',
                '</div>',
                '<div id="disk" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Disk</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.disk + ' Gb' + '</span>',
                '</div>',
                '</div>',
                '<div id="deliveryPoint" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery point</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.deliveryPoint + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery start</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryStartTime) + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery end</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryEndTime) + '</span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Action details",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });

            if(isFormItLoad){
                $('#quantity').hide();
                $('#deliveryPoint').hide();
            } else {
                 $('#cpu').hide();
                 $('#ram').hide();
                 $('#disk').hide();
            }
        },
        error: function (response) {
            console.log("Preview invoice error");
        }
    });
}
