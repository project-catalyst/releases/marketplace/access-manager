
// Market variants
let energyFormsWithMarketTabs = new Map();
energyFormsWithMarketTabs.set('electric_energy', energy_market);
energyFormsWithMarketTabs.set('thermal_energy', heat_market);
energyFormsWithMarketTabs.set('it_load', it_load_market);
energyFormsWithMarketTabs.set('ancillary-services', flexibility_market);

// History
// History dropdown menu options
let historyDropDownItems = new Map();
historyDropDownItems.set('electric_energy', electric_energy_history);
historyDropDownItems.set('thermal_energy', thermal_energy_history);
historyDropDownItems.set('it_load', it_load_history);
historyDropDownItems.set('ancillary-services', ancillary_services_history);
historyDropDownItems.set('correlations', correlations_history);

// History left tab options
let historyLeftTabOptions = new Map();
historyLeftTabOptions.set('electric_energy' , electric_energy_history_left_tab);
historyLeftTabOptions.set('thermal_energy' , thermal_energy_history_left_tab);
historyLeftTabOptions.set('it_load' , it_load_history_left_tab);
historyLeftTabOptions.set('ancillary-services' , ancillary_services_history_left_tab);


// Invoices
// Invoices dropdown menu options
let invoicesDropDownItems = new Map();
invoicesDropDownItems.set('electric_energy', electric_energy_invoices);
invoicesDropDownItems.set('thermal_energy', thermal_energy_invoices);
invoicesDropDownItems.set('it_load', it_load_invoices);
invoicesDropDownItems.set('ancillary-services', ancillary_services_invoices);

// Invoices left tab options
let invoicesLeftTabOptions = new Map();
invoicesLeftTabOptions.set('electric_energy', electric_energy_invoices_left_tab);
invoicesLeftTabOptions.set('thermal_energy', thermal_energy_invoices_left_tab);
invoicesLeftTabOptions.set('it_load', it_load_invoices_left_tab);
invoicesLeftTabOptions.set('ancillary-services', ancillary_services_invoices_left_tab);




$(document).ready(function () {

    console.log("User market dynamic page:");
    if(user_markets == null)
     user_markets = []

    // define market types in the inverse order from the one initially chosen, since the prepend function is used
    var possibleEnergyForms = ['ancillary-services', 'it_load', 'thermal_energy' , 'electric_energy'];

    // if user is registered in at least one market variant, add the History & Invoice tab elements
    var navTabList = $("#nav-market-list");
    console.log("inside  gen")
    console.log(user_markets.length)
    if(user_markets.length > 0){
        navTabList.prepend(invoice_tab);
        navTabList.prepend(history_tab);
        if(actor !== "undefined" && actor.marketActorTypeid.type !== "dso")
            navTabList.prepend(correlations_tab);
    }

    var historyDropDown = $("#history-dropdown-menu");
    var historyLeftTab = $("#history-left-tab");


    var invoicesDropDown = $("#invoice-dropdown-menu");
    var invoicesLeftTab = $("#invoices-left-tab");

    var correlationDropDown = $("#correlations-dropdown-menu");

    var forms = [];
    for(var i in user_markets){
        console.log(user_markets[i].form)
        forms.push( user_markets[i].form);
    }

    // for each market variant that the user is registered in, get the form
    // for every form, add the visual elements corresponding to it (Market Tab, Link in History & Invoices dropdowns
    // and left tabs in History & Invoices page)
    for(var i in possibleEnergyForms){

        var form = possibleEnergyForms[i];
        if(forms.indexOf(form) >= 0){
            navTabList.prepend(energyFormsWithMarketTabs.get(form));

            historyDropDown.prepend(historyDropDownItems.get(form));
            historyLeftTab.prepend(historyLeftTabOptions.get(form));

            invoicesDropDown.prepend(invoicesDropDownItems.get(form));
            invoicesLeftTab.prepend(invoicesLeftTabOptions.get(form));
        }
    }
    if(actor != null)
        if(actor.marketActorTypeid.type !== "market operator")
            correlationDropDown.append(correlations_create);

    if(actor != null)
        if(actor.marketActorTypeid.type !== "dso")
            historyDropDown.prepend(correlations_history);
    correlationDropDown.append(correlations_edit);


    var current = location.pathname;
    var first = true;
    $('#nav-market-list>li>a').each(function(){
        var $this = $(this);
        if(current.indexOf($this.attr('href')) !== -1 && first){
            $this.parent('li').addClass('active');
            first = false;
        }
    });

});
