/*
this file structures data for the edit correlations page
this is the main js file for updateCorrelationsDayahead.html and updateCorrelationsIntraday.html
all the server calls are stored in correlation_action_calls.js
 */
var all_actions = [];
var update_front_data = [];
var allCorrelations = [];
var dropdown_constraints = [];
var constraints = [];
var current_selected_contraint = 0;
var succes_div, warning_div;
var succes_div_del, warning_div_del;

/*
document ready function is called when the page is loaded
all actions for a time stamp are stored in variable all_actions. They will be used for the action details modal and for the edit correlation table
all correlations are stored in allCorrelation variable. They are displayed using #cor2 table
 */

var front_data = [];
$(document).ready(function () {
    succes_div = document.getElementById("succesDiv");
    succes_div.style.display = 'none';
    warning_div = document.getElementById("warningDiv");
    warning_div.style.display = 'none';
    succes_div_del = document.getElementById("succesDivDel");
    succes_div_del.style.display = 'none';
    warning_div_del = document.getElementById("warningDivDel");
    warning_div_del.style.display = 'none';
    $("#cor2").bootstrapTable('load', front_data);
});

constructTable();

function constructTable(){
    front_data = [];
    constraints = getAllConstraints(memo_ip, username);
    var allCorrelationsIntact =  getCorrelatedMarketActions(memo_ip, user_markets, MPLACE_URL, username, timeframe, token, SELECT_MPLACE_URL, is_dso);
    allCorrelations = allCorrelationsIntact["correlations"];
    console.log("received correlations")
    console.log(allCorrelations)
    for (var i in allCorrelations) {
            allCorrelations[i].actions_ids = [];
            allCorrelations[i].actions_ids_front = [];
            for (var j in allCorrelations[i]["actions"]) {
                var market_url = "#";
                for(var k in user_markets){
                    if(user_markets[k].id == allCorrelations[i].actions[j].informationBrokerId) {
                        market_url = user_markets[k].url;
                    }
                }
                allCorrelations[i].actions_ids.push(allCorrelations[i].actions[j].actionId);
                allCorrelations[i].actions_ids_front.push({
                    id:allCorrelations[i].actions[j].actionId,
                    url: market_url});
            }

            var description = "";
            for (var constr in constraints){
                if(constraints[constr].id === allCorrelations[i].constraintId) {
                    description = constraints[constr].description;
                }
            }

            var act = {
                  id: allCorrelations[i].correlationId,
                  constraint: description,
                  actions: allCorrelations[i].actions_ids_front
            };
            front_data.push(act);
        }
     dropdown_constraints = document.getElementById("constraint-dropdown");

     for (var i in constraints){
         var option = document.createElement("option");
         option.text = constraints[i].description;
         option.id = constraints[i].id;
         dropdown_constraints.add(option);
     }
     console.log("Data ready for frontend")
     console.log(front_data)
    $("#table_data_helper").attr('style', '');
}

/*
this function display the action details modal for a better user experience
 */
function datetimeUTCFormatter(datetime) {
    return moment(datetime).utc().format("YYYY-MM-DD HH:mm ZZ");
}
function showAction(marketurl, selected_id) {
    action = {};
    if(marketurl == "#")
        console.log("UNKONWN MARKET");
    else
    $.ajax({
        type: "GET",
        url: marketurl + ACTION_URL + selected_id + '/',
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        success: function (action) {
            var isFormItLoad = (action.formid.form === "it_load");
            var isFormThermalEnergy = (action.formid.form === "thermal_energy");
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';
            var price = action.price.replace('.', ',') + ' &#8364;/MWh';
            if (isFormItLoad)
                price = action.price.replace('.', ',') + ' &#8364;';
            if (isFormThermalEnergy)
                price = action.price.replace('.', ',') + ' &#8364;/MWht';
            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Type</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + action.formid.form + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Price</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + price + '</span>',
                '</div>',
                '</div>',
                '<div id="quantity" class="form-group clearfix">',
                '<div  class="' + lclass + '">',
                '<label class="pull-right"><strong>Quantity</strong></label>',
                '</div>',
                '<div  class="' + fclass + '">',
                '<span class="pull-left">' + action.value.replace('.', ',') + ' ' + action.uom + '</span>',
                '</div>',
                '</div>',
                '<div id="cpu" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>CPU</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.cpu + '</span>',
                '</div>',
                '</div>',
                '<div id="ram" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>RAM</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.ram + ' Mb' + '</span>',
                '</div>',
                '</div>',
                '<div id="disk" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Disk</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.disk + ' Gb' + '</span>',
                '</div>',
                '</div>',
                '<div id="deliveryPoint" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery point</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.deliveryPoint + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery start</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryStartTime) + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery end</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryEndTime) + '</span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Action details",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });

            if(isFormItLoad){
                $('#quantity').hide();
                $('#deliveryPoint').hide();
            } else {
                 $('#cpu').hide();
                 $('#ram').hide();
                 $('#disk').hide();
            }
        },
        error: function (response) {
            console.log("Preview invoice error");
        }
    });
}

function updateDeleteRulesFormatter(value, row, index) {
    return [
        '<a class="edit" href="javascript:void(0)" title="Edit" onclick="editRule(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>  ',
        '<a class="remove" href="javascript:void(0)" title="Remove" onclick="removeRule(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-trash" style="color:red;"></i>',
        '</a>',
    ].join('');
}

/*
this function display the edit correlation modal
 */
function editRule(row){
    if (all_actions.length == 0) {
        console.log("waiting for the actions to load")
        all_actions = getAllActions(user_markets, MPLACE_URL, username, timeframe, token, SELECT_MPLACE_URL, is_dso);
    }
    current_selected_contraint = allCorrelations[row - 1].correlationId;
    selected_actions_ids = allCorrelations[row - 1].actions_ids;
    update_front_data = []
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    $('#updateCorrelationModal').modal('show');
      for (var i in all_actions){
        var act = {
          id: all_actions[i].id,
          date: moment.utc(all_actions[i].date).format(timeFormatStr),
          actionStartTime: moment.utc(all_actions[i].actionStartTime).format(timeFormatStr),
          actionEndTime: moment.utc(all_actions[i].actionEndTime).format(timeFormatStr),
          value: parseFloat(all_actions[i].value).toFixed(3),
          deliveryPoint: (all_actions[i].deliveryPoint != " ") ? all_actions[i].deliveryPoint : "-",
          price: parseFloat(all_actions[i].price).toFixed(2),
          cpu: all_actions[i].cpu ? Math.trunc(all_actions[i].cpu) : "-",
          ram: all_actions[i].ram ? Math.trunc(all_actions[i].ram) : "-",
          disk: all_actions[i].disk ? Math.trunc(all_actions[i].disk) : "-",
          actionType: all_actions[i].actionTypeid.type,
          actionStatus: all_actions[i].statusid.id,
          loadid: all_actions[i].loadid,
          participant: all_actions[i].marketActorid.id,
          status : all_actions[i].statusid.status,
          type: all_actions[i].formid.form,
          selected: (selected_actions_ids.includes(all_actions[i].id)) ? (true): (false),
          session_id: all_actions[i].session_id,
          informationBrokerId: all_actions[i].informationBrokerid,
        }
        update_front_data.push(act);
      }

    dropdown_constraints = document.getElementById("constraint-dropdown");
    dropdown_constraints.selectedIndex = row;

    $("#updateCorrelationTable").bootstrapTable('load', update_front_data);
}

function manageRulesFormatter(value, row, index) {
    return ('<input type="checkbox" onclick="selectAction(' + index + ')"' + ((update_front_data[index].selected) ? "checked=True" : "" ) + '/>');
}

function selectAction(index){
    update_front_data[index].selected = !update_front_data[index].selected;
}

/*
this is the method for calling the deleteCorrelation endpoint
the server call is in correlation_action_calls.js file
 */
function removeRule(id){
    succes_div_del.style.display = 'none';
    warning_div_del.style.display = 'none';
    var response = deleteCorrelation(id);
    if (response == 1)
        succes_div_del.style.display = 'block';
    else warning_div_del.style.display = 'block';
    constructTable();
}

function setActionFormatter(value, row, index) {
    ret_str = "";
    len_str = value.length;
    for (var i in value) {
        ret_str += '<a href="#" data-pk="' + value[i].id + '"onclick=showAction("' + value[i].url + '",' +  value[i].id + ')  class="view-market-action" style="color:darkgreen">' + value[i].id + '</a>';
        if (i < len_str - i)
            ret_str += ", ";
    }
    if (ret_str == "")
        console.log("No actions in the actions ids list from the correlation")
    return ret_str;

}

function updateCorrelations(){
    var dropdown =  document.getElementById("constraint-dropdown");
    var selectedConstraint = dropdown[dropdown.selectedIndex].id;
    var send_obj = {};

    send_obj.constraintId = selectedConstraint;
    send_obj.username = username;
    send_obj.correlationId = current_selected_contraint;
    send_obj.timeframe = timeframe;
    send_obj.timestamp = new Date().valueOf();
    send_obj.actions = [];
    update_front_data.forEach(a =>{
        if (a.selected){
            var action = {};
            action.sessionId = a.session_id;
            action.actionId = a.id;
            action.informationBrokerId = a.informationBrokerId;
            send_obj.actions.push(action);
        }
    });
    var response = updateCorrelatedMarketActions(memo_ip, send_obj, token);
    succes_div.style.display = 'none';
    warning_div.style.display = 'none';
     if (response == 1)
        succes_div.style.display = 'block';
    else warning_div.style.display = 'block';
    $('#updateCorrelationModal').modal('hide');
}