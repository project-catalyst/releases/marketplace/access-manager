/*
 * Convert datetime in UTC base
 * 
 * Dependency: moment js
 */

var UtcDateTime = UtcDateTime || (function () {
    var utcFormat = "YYYY-MM-DD HH:mm ZZ";

    return {
        format: function () {
            return utcFormat;
        },
        convert: function (datetime) {
            return moment(datetime).utc().format(utcFormat);
        },
        utcTimestamp: function () {
            return moment().utc().format(utcFormat);
        },
        isoTimestamp: function () {
            // iso 8601
            return UtcDateTime.utcToIso(UtcDateTime.utcTimestamp());
        },
        utcToIso: function (datetime) {
            // input: 2016-04-15 12:00 +0000 
            // output: 2016-04-15T12:00:00+00:00
            var tokens = datetime.split(" ");
            var timezone = tokens[2].substring(0, 3) + ":" + tokens[2].substring(3, 5);
            return  tokens[0] + "T" + tokens[1] + ":00" + timezone;
        }
    };

})();