		$.ajax({
            url : user_markets[0].url + RULES_URL,
            type : 'GET',
            dataType : 'json',
            beforeSend: function (xhr) {

                xhr.setRequestHeader("Authorization", user_markets[0].token)
            },
            success : function(data) {
                for(var key in data){
                    data[key].description = data[key].description.replace(/\\n/g,"\n")
                }
                var table = $('#gam_rules').bootstrapTable({data: data});
            },
            error: function(){
                console.log("error loading rules");
            }
        });