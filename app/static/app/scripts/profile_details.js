function fillProfileMplaces() {
    var data = [];
    var usermplaces;
    $("#mplace_table").bootstrapTable();
    for(var k in user_markets) {
        console.log("user market fill" + user_markets[k].url + MPLACE_URL)
        console.log("username " + username)
        $.ajax({
            type: "GET",
            url: user_markets[k].url + MPLACE_URL,
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", user_markets[k].token)
            },

            success: function (response) {

                var actorId = getActorId(user_markets[k].url, USERNAME_TO_ACTOR, user_markets[k].token, username);

                for (var i in response) {
                    data.push({
                        id: response[i].id,
                        mplace_type: response[i].marketServiceTypeid.type,
                        timeframe: response[i].marketServiceTypeid.timeFrameid.type,
                        token: user_markets[k].token,
                        url: user_markets[k].url,
                        actorId: actorId,
                    });
                }
            },
            error: function(){
                console.log("ERROR in fillProfileMarketPlaces" + user_markets[k])
            }
        });
    }

    $("#mplace_table").bootstrapTable('load', data);

    usermplaces = getUserMplaces();
    console.log("user mplaces");
    console.log(usermplaces);

    for (var i in usermplaces) {
        var marketType = usermplaces[i].marketServiceTypeid.type;
        var timeframe = usermplaces[i].marketServiceTypeid.timeFrameid.type;

        $('td:contains(' + marketType + '):containts(' + timeframe + ')').parent().addClass("selected");
        $('td:contains(' + marketType + '):containts(' + timeframe + ')').siblings('td').children('input').attr('checked', 'true');
    }
}

function getUserMplaces(){
    var mplaces = [];
    for(var i in user_markets) {

        var actorId = getActorId(user_markets[i].url, USERNAME_TO_ACTOR, user_markets[i].token, username);

        $.ajax({
            type: "GET",
            url: user_markets[i] + ACTOR_URL + actorId + "/marketplaces/",
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token)
            },
            success: function (res) {
                mplaces.concat(res);
            }
        });

    }
    return mplaces;
}

function setMarketFormatter(value, row, index) {
    var cancel = [
        '<span '+ 'data-actor-id="' + row["actorId"] + '" data-token="' + row["token"] + '" data-url="' + row["url"] + '">',
        row["id"] ,
        '</span>',
    ].join('');
    return cancel;
}

function selectMplaces(){
    var not_selected = $("tr.selected");
    var mplaces = $("tbody").find("tr").not(".selected");

    var removed_mplaces = [];

    if (not_selected.length == 0) {
        swal({
                   title: "No Marketplace selected",
                   text: 'You have not selected any marketplace to leave',
                   type: "warning",
                   confirmButtonText: "OK",
                   confirmButtonColor: "#009245",
               })
        return;
    }
    $.each(not_selected,function(key,value){
        not_selected_place = $(this).find('td:first').text();
        var elem2 = $(this).find('td:first span')[0]
        url = elem2.getAttribute("data-url");
        market_token = elem2.getAttribute("data-token");
        actorId = elem2.getAttribute("data-actor-id");

        $.ajax({
            type:"DELETE",
            url:url + ASSIGN_ACTORS_URL+actorId+"/marketplace/"+not_selected_place+"/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", market_token)
            },
            datatype:"json",
            contenttype: 'application/json',
            async:false,
            success: function(){
                removed_mplaces.push(not_selected_place);
            }
        });
        swal({
               title: "Marketplace removed",
               text: 'You no longer have access to marketplaces ' + removed_mplaces,
               type: "success",
               confirmButtonText: "OK",
               confirmButtonColor: "#009245",
           });
        $("#mplace_table tbody").empty();
        fillProfileMplaces();
    });
}