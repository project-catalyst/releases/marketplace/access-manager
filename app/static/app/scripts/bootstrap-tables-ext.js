﻿var timeFormatStr = "YYYY-MM-DD HH:mm";

var current = {};
var previous = {};
var selected_status_id = 8;
var ib_sessions_url = "";

function get_sessions(sessions_url) {
	//Get only the active sessions
	ajax_call_wo_data(sessions_url + "/active", "GET", function (all_sessions) { create_sessions_tables(sessions_url, all_sessions); }, function on_error(data) { });
}

function create_sessions_tables(sessions_url, all_sessions) {
	// //all_sessions should contain all active sessions (just one)
	// if (all_sessions.length === 0) {
	// 	//If there is no active session and a previous one existed, then remove the previous one
	// 	if (!jQuery.isEmptyObject(current)) {
	// 		$("#data_table_session_" + current.id).bootstrapTable('destroy');
	// 	}
	// 	//Re initialize the containers
	// 	current = {};
	// 	previous = {};
	// 	$("#active_sessions").empty();
	// 	$("#active_sessions").html("<h3>There is no active session currently.</h3>");
	// 	return;
	// }
	// //Clone the object
	// current = jQuery.extend(true, {}, all_sessions[0]);
	// previous = jQuery.extend(true, {}, current);
	// //Continue with the processing of the session
	// if ($("#data_table_session_" + current.id) && $("#data_table_session_" + current.id).children().length > 0) {
	// 	//If it already exists, then refresh data
	// 	$("#data_table_session_" + current.id).bootstrapTable('refresh', { silent: true });
	// } else {
	// 	//Create the toolbar
	// 	var toolbar = $();
	// 	//Create the bootstrap-table entry
	// 	var table = ' <div id="session_' + current.id + '"><h5>Session <b>' + current.id + '</b> is ' + current.sessionStatusid.status;
	// 	if (current.sessionStatusid.status === "closed") {
	// 	    table += " since " + moment(current.sessionEndTime).format(timeFormatStr) + ".</h5>";
	// 	} else if (current.sessionStatusid.status === "active") {
	// 	    table += " since " + moment(current.sessionStartTime).format(timeFormatStr);
	// 	    table += " until " + moment(current.sessionEndTime).format(timeFormatStr) + ".</h5>";
	// 	    table += "<h5> Delivery since " + moment(current.deliveryStartTime).format(timeFormatStr);
	// 	    table += " until " + moment(current.deliveryEndTime).format(timeFormatStr) + ".</h5>";
	// 	}
	// 	table += '. </div> ';
	// 	table += '<div id="toolbar_' + current.sessionStatusid.status + '_' + current.id + '" class="btn-group"> <button id="newbtn' + current.id + '" + type="button" class="btn btn-default" data-target="#addAction" data-toggle="modal"> <i class="glyphicon glyphicon-plus"></i> </button> <button id="slctbtn_' + current.id + '" type="button" class="btn btn-default" disabled> <i class="glyphicon glyphicon-ok"></i> </button></div>';
	// 	table += '<table id="data_table_session_' + current.id + '" data-toolbar="#toolbar_' + current.sessionStatusid.status + '_' + current.id + '" data-toggle="table" data-url="' + sessions_url + '/' + current.id + '/actions" data-toolbar="#session_' + current.id + '" data-search="true" data-pagination="true" data-page-size="5" data-page-list="[5, 10, 20, 50, 100, 200]" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-sort-name="date" data-sort-order="desc" data-click-to-select="true" data-single-select="true"> <thead> <tr><th data-field="state" data-checkbox="true" data-formatter="stateFormatter"></th> <th data-field="id">ID</th> <th data-field="date" data-formatter="time_formatter" data-sortable="true" >Date</th> <th data-field="actionStartTime" data-formatter="time_formatter" data-sortable="true">Action Start</th> <th data-field="actionEndTime" data-formatter="time_formatter" data-sortable="true">Action End</th> <th data-field="value">Value</th> <th data-field="price" data-sortable="true">Price</th> <th data-field="actionTypeid" data-sortable="true" data-formatter="action_type_formatter" >Type</th> <th data-field="actionStatusid" data-sortable="true" data-formatter="action_status_formatter" >Status</th> </tr> </thead> </table>';
	// 	// Clean up the heading indicating that there are no active sessions
	// 	$("#active_sessions").empty();
	// 	//Update the entry
	// 	$("#active_sessions").html(toolbar);
	// 	$("#active_sessions").append(table);
	// 	$("#data_table_session_" + current.id).bootstrapTable();


		var $sessiontable = $('#data_table_session_' + current.id),
			$selectbtn = $('#slctbtn_' + current.id);

		$sessiontable.on('check.bs.table uncheck.bs.table ' +
		   'check-all.bs.table uncheck-all.bs.table', function () {
			   $selectbtn.prop('disabled', !$sessiontable.bootstrapTable('getSelections').length);
		   });
		$selectbtn.click(function () {
			var sel_id = getIdSelections($sessiontable);
			ajax_call_wo_data(ib_sessions_url + "/" + current.id + "/actions/" + sel_id, "GET", function (_sel_action) {
			    if (_sel_action.actionStatusid.id === 2) {
			        _sel_action.actionStatusid = selected_status_id;
			        update_action_status(_sel_action);
			    } else {
			        alert("You cannot select this action!");
			    }
			}, function on_error(data) { });
			$selectbtn.prop('disabled', true);
		});



		$('#uoml').change(function () {
			$('#uom').val($('#uoml').val());

		});

		$('#typel').change(function () {
			$('#type').val($('#typel').val());

		});
		// $('#AddActionForm').submit(function () {
		// 	var $inputs = $('#AddActionForm input');
		// 	var values = {};
		// 	$inputs.each(function () {
		// 		if (this.id === "type") {
		// 			if ($(this).val() === "offer") {
		// 				values["actionTypeid"] = 2;
		// 			} else if ($(this).val() === "bid") {
		// 				values["actionTypeid"] = 1;
		// 			}
		// 		} else {
		// 			values[this.id] = $(this).val();
		// 		}

		// 		$(this).val('');
		// 		$('#uom').val($('#uoml').val());
		// 		$('#type').val($('#typel').val());
		// 	}
		// 	);

		// 	values['date'] = moment().format();
		// 	values['actionStartTime'] = moment(values['actionStartTime'], 'DD-MM-YYYY HH:mm').format();
		// 	values['actionEndTime'] = moment(values['actionEndTime'], 'DD-MM-YYYY HH:mm').format();
		// 	values['marketSessionid'] = current.id;
		// 	values['marketActorid'] = 2;
		// 	values['formid'] = 1;
		// 	values['actionStatusid'] = 1;
		// 	add_action(values);
		// 	$('#actionStartTime').data("DateTimePicker").minDate(moment());
		// 	$('#actionStartTime').data("DateTimePicker").maxDate(moment().add(32, 'hours'));
		// 	$('#actionEndTime').data("DateTimePicker").minDate(moment().add(1, 'hours'));
		// 	$('#actionEndTime').data("DateTimePicker").maxDate(moment().add(33, 'hours'));
		// });

	}
}

function auto_refresh_sessions(sessions_url) {
	ib_sessions_url = sessions_url;
	get_sessions(sessions_url);
	setInterval(function () { get_sessions(sessions_url); }, 10000 * 1000);
}


function getIdSelections(_table) {
	return $.map(_table.bootstrapTable('getSelections'), function (row) {
		return row.id;
	});
}

function update_action_status(_action) {
	ajax_call_with_data(ib_sessions_url + "/" + current.id + "/actions/" + _action.id, "PUT", function () { get_sessions(ib_sessions_url) }, function on_error(data) { }, _action);
}

function add_action(_action) {
	console.log(_action);
    $('#addAction').modal('hide');
    ajax_call_with_data(ib_sessions_url + "/" + current.id + "/actions/", "POST", function () { setTimeout(function () { get_sessions(ib_sessions_url); }, 3000); }, function on_error(data) { console.log(data); }, _action);
}
