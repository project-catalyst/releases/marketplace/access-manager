/*
this file structures data for the history correlations page
this is the main js file for correlationsHistoryDayahead.html and correlationsHistoryIntraday.html
all the server calls are stored in correlation_action_calls.js
 */
var all_actions = [];
var allCorrelations = [];
var constraints = [];


/*
document ready function is called when the page is loaded
all actions for a time stamp are stored in variable all_actions. They will be used for the action details modal and for the edit correlation table
all correlations are stored in allCorrelation variable. They are displayed using #cor2 table
 */

$(document).ready(function () {
    var front_data = [];

    constraints = getAllConstraints(memo_ip, username);
    allCorrelations = getCorrelatedHistoryMarketActions(memo_ip, username, token, timeframe)["correlations"];
    console.log(allCorrelations);
    for (var i in allCorrelations) {
        allCorrelations[i].actions_ids = [];
        allCorrelations[i].actions_ids_front = [];
        for (var j in allCorrelations[i]["actions"]) {
            var market_url = "";
            for(var k in user_markets){
                if(user_markets[k].id == allCorrelations[i].actions[j].informationBrokerId)
                  market_url = user_markets[k].url;
            }
            allCorrelations[i].actions_ids_front.push("<a onClick='showAction(\"" + market_url+ "\"" + "," + allCorrelations[i].actions[j].actionId +  ")'>" + allCorrelations[i].actions[j].actionId + '</a>');
            allCorrelations[i].actions_ids.push(allCorrelations[i].actions[j].actionId);
        }

        var description = "";
        for (var constr in constraints){
            if(constraints[constr].id === allCorrelations[i].constraintId)
                description = constraints[constr].description;
        }
        var act = {
              id: allCorrelations[i].correlationId,
              constraint: description,
              actions: allCorrelations[i].actions_ids_front,
        };
        front_data.push(act);
    }

    console.log("built front data");
    console.log(front_data)

    $("#cor2").bootstrapTable('load', front_data);
});

function datetimeUTCFormatter(datetime) {
    return moment(datetime).utc().format("YYYY-MM-DD HH:mm ZZ");
}
//this function display the action details modal for a better user experience

function showAction(marketurl, selected_id) {
    console.log(selected_id);

    console.log("Get market action details from:");
    console.log(marketurl + ACTION_URL + selected_id + '/');
    if(marketurl == "")
        console.log("UNKONWN MARKET");
    else
    $.ajax({
        type: "GET",
        url: marketurl + ACTION_URL + selected_id + '/',
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        success: function (action) {
            var isFormItLoad = (action.formid.form === "it_load");
            var isFormThermalEnergy = (action.formid.form === "thermal_energy");
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';
            var price = action.price.replace('.', ',') + ' &#8364;/MWh';
            if (isFormItLoad)
                price = action.price.replace('.', ',') + ' &#8364;';
            if (isFormThermalEnergy)
                price = action.price.replace('.', ',') + ' &#8364;/MWht';
            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Type</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + action.formid.form + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Price</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + price + '</span>',
                '</div>',
                '</div>',
                '<div id="quantity" class="form-group clearfix">',
                '<div  class="' + lclass + '">',
                '<label class="pull-right"><strong>Quantity</strong></label>',
                '</div>',
                '<div  class="' + fclass + '">',
                '<span class="pull-left">' + action.value.replace('.', ',') + ' ' + action.uom + '</span>',
                '</div>',
                '</div>',
                '<div id="cpu" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>CPU</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.cpu + '</span>',
                '</div>',
                '</div>',
                '<div id="ram" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>RAM</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.ram + ' Mb' + '</span>',
                '</div>',
                '</div>',
                '<div id="disk" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Disk</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.disk + ' Gb' + '</span>',
                '</div>',
                '</div>',
                '<div id="deliveryPoint" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery point</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + action.deliveryPoint + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery start</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryStartTime) + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery end</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(action.marketSessionid.deliveryEndTime) + '</span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Action details",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });

            if(isFormItLoad){
                $('#quantity').hide();
                $('#deliveryPoint').hide();
            } else {
                 $('#cpu').hide();
                 $('#ram').hide();
                 $('#disk').hide();
            }
        },
        error: function (response) {
            console.log("Preview invoice error");
        }
    });
}