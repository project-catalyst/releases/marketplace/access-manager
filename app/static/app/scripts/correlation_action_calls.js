/*
this file contain all server calls
in case one endpoint changes only the url and not the payload, this is the only file to be modified
 */
var correlation_endpoint = 'marketplace/all/coupleble/actor/actions/coupled/';

function getMarketID(marketIP, selectMPlaceURL, form, username, granularity, token) {
    var marketid = 0;
    //console.log("getMarketID");
    //console.log(marketIP + selectMPlaceURL + form + "/" + granularity + '/'+ username + "/");
    $.ajax({
        type: "GET",
        url: marketIP + selectMPlaceURL + form + "/" + granularity + '/'+ username + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (response) {
            if (response.length > 1) {
                swal({
                       title: "Error",
                       text: 'Multiple marketplaces received!',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
                return false;

            } else if(response.length === 0){
                 swal({
                       title: "Error",
                       text: 'You are not a participant in any market of this type! \n You will be redirected to home',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   },
                     function (isConfirm) {
                        if(isConfirm){
                          location.href = "/";
                        }
                     });
            }

            else {
                marketid = response[0];
            }
        }
    });

    return marketid;
}

function getSessionsOfMarket(marketIP, mplaceURL, marketId, token ) {
    var session;
    if (marketId !== null) {
        //console.log("getSessionOfMarket");
        //console.log(marketIP + mplaceURL + marketId + "/marketsessions/active/");
        $.ajax({
            type: "GET",
            url: marketIP + mplaceURL + marketId + "/marketsessions/active/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (response) {
                if (response.length > 1) {
                    console.log("Multiple sessions");
                    console.log(response)
                        swal({
                           title: "Error",
                           text: 'Multiple sessions received!##',
                           type: "warning",
                           confirmButtonText: "OK",
                           confirmButtonColor: "#009245",
                       });
                        session = null;
                        return session;
                }
                if ($.trim(response) != '')
                    session = response[0];
                else
                    return null;
            },
            error: function () {console.log("ERROR at getting session of market" + marketIP + mplaceURL + marketId + "/marketsessions/active/");}
        });
    }
    else console.log("marketId is null " + marketIP);
    return session;
}

function getSessionsOfMarketByForm(marketIP, mplaceURL, marketId, form, token) {
    var session;
    if (marketId !== null) {
        $.ajax({
            type: "GET",
            url: marketIP + mplaceURL + marketId + "/marketsessions/form/" + form + "/active/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (response) {
                if (response.length > 1) {
                        swal({
                           title: "Error",
                           text: 'Multiple sessions received!??',
                           type: "warning",
                           confirmButtonText: "OK",
                           confirmButtonColor: "#009245",
                       });
                        session = null;
                         console.log("multiple sessions " + marketIP + mplaceURL + marketId + "/marketsessions/form/" + form + "/active/")
                        return session;
                }


                if ($.trim(response) != '')
                    session = response[0];
                else
                    return null;
            },
            error: function () {console.log("ERROR at get session by form " + marketIP + mplaceURL + marketId + "/marketsessions/form/" + form + "/active/");}
        });
    }
    else console.log("marketId is null for " + marketIP);
    return session;
}



function getMarketActions(marketIP, marketID, marketURL, username, is_dso, session, token) {
     var data = [];
     if (session == null)
         return data;
      $("#table_data_helper").attr('style', '');
     $.ajax({
            type: "GET",
            url: marketIP + marketURL + marketID + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + session.id + "/actions_by_status/valid/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (response) {
                data =
                    response;
            },
         error: function () {
            console.log("error at getting market actions from " + marketIP + marketURL + marketID + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + session.id + "/actions_by_status/valid/")
         }
        });
        return data;
}

function getAllConstraints(memoIP, username){
    var data = []

    $.ajax({
        type: "GET",
        url: memoIP + 'constraints/',
        beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success : function (response) {
            //console.log("Success getAllConstraints");
            //console.log(response);
            data = response;
        },
        error: function () {
            console.log("error getting constraints from " + memoIP + 'constraints/')
        }

    });
    return data;
}

function postCorrelatedMarketActions(memo_ip, data, token) {
    var res = 0;
    //console.log("postCorrelatedMarketActions");
    //console.log(memo_ip + correlation_endpoint);
    //console.log("postCorrelatedMarketActions payload");
    //console.log(data)
    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", token);
                    },
        url: memo_ip + correlation_endpoint,
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(data),
        async: false,
        success : function (response) {
            console.log("success postCorrelatedMarketActions");
            console.log(response)
            data = response;
            res = 1;
        },
        error: function () {
            console.log("error posting correlation " + memo_ip + correlation_endpoint);
        }

    });
    return res;
}

function updateCorrelatedMarketActions(memo_ip, data, token) {
    var res = 0;
    //console.log("updateCorrelatedMarketActions");
    //console.log(memo_ip + correlation_endpoint);
    //console.log("updateCorrelatedMarketActions payload");
    //console.log(data)
    $.ajax({
        type: "PUT",
        beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", token);
                    },
        url: memo_ip + correlation_endpoint,
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(data),
        async: false,
        success : function (response){
            console.log(" success updateCorrelatedMarketActions");
            console.log(response);
            data = response;
            res = 1;
        },
        error: function () {
            console.log("error updating correlation " + memo_ip + correlation_endpoint);
        }
    });
    return res;
}

function getMinMaxActiveSessionInterval(user_markets, MPLACE_URL, username, granularity, token, SELECT_MPLACE_URL, is_dso){
    var res = {}
    res.start = -1;
     res.end = -1;
    user_markets.forEach(user_market => {
         var form = user_market.form;
         if (form == 'electric_energy')
             form = "energy";
         var market_id = getMarketID(user_market.url, SELECT_MPLACE_URL, form, username, granularity, user_market.token);
         if(form == 'ancillary-services'){
            flexibility_forms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling'];
            flexibility_forms.forEach(flexForm => {
                var session = getSessionsOfMarketByForm(user_market.url, MPLACE_URL, market_id, flexForm, user_market.token);
                if (session) {
                 if (res.start == -1) {
                     res.start = session.sessionStartTime;
                     res.end = session.sessionEndTime;
                 }
                 if (res.start > session.sessionStartTime)
                     res.start = session.sessionStartTime;
                 if (res.end < session.sessionEndTime)
                     res.end = session.sessionEndTime;
             }
            });
         }
         else {
             var session = getSessionsOfMarket(user_market.url, MPLACE_URL, market_id, user_market.token);
             if (session) {
                 if (res.start == -1) {
                     res.start = session.sessionStartTime;
                     res.end = session.sessionEndTime;
                 }
                 if (res.start > session.sessionStartTime)
                     res.start = session.sessionStartTime;
                 if (res.end < session.sessionEndTime)
                     res.end = session.sessionEndTime;
             }
         }
     });
    res.start = new Date(res.start).getTime();
    res.end = new Date(res.end).getTime();
    return(res);
}

function getCorrelatedMarketActions(memo_ip, user_markets, MPLACE_URL, username, granularity, token, SELECT_MPLACE_URL, is_dso){
    var data = {};
    var interval = getMinMaxActiveSessionInterval(user_markets, MPLACE_URL, username, granularity, token, SELECT_MPLACE_URL, is_dso);
    if (interval.start < 1 || interval.start >= interval.end)
        console.log("error in intervals, possible that there are no active sessions  " + interval.start + " end " + interval.end);
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", token);
                    },
        url: memo_ip + correlation_endpoint + timeframe + '/'+ interval.start + "/" + interval.end + "/" + username + "/",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success : function (response){
            data = response;
        },
        error: function(){
            console.log("error get all correlation");
        }
    });
    return data;
}

function getCorrelatedHistoryMarketActions(memo_ip, username, token, timeframe){
    var data = {};
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", token);
                    },
        url: memo_ip + correlation_endpoint + timeframe + '/' + username + "/",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success : function (response){
            data = response;
        },
        error: function () {
            console.log("error getting correlation history from " + memo_ip + correlation_endpoint + timeframe + '/' + username + "/");
        }
    });
    return data;
}

function getAllActions(user_markets, MPLACE_URL, username, granularity, token, SELECT_MPLACE_URL, is_dso){
    all_actions = []
    user_markets.forEach(user_market => {
        var form = user_market.form;
        if (form == 'electric_energy')
          form = "energy";
        var market_id = getMarketID(user_market.url, SELECT_MPLACE_URL, form, username, granularity, user_market.token);

        if(form == 'ancillary-services'){
            flex_actions = getFlexibilityActions(user_market, market_id);
            flex_actions.forEach(flexAction => {
                all_actions.push(flexAction);
            });
        }
        else{
            var session = getSessionsOfMarket(user_market.url, MPLACE_URL, market_id, user_market.token);
            if (session !== undefined) {
                var session_actions = getMarketActions(user_market.url, market_id, MPLACE_URL, username, is_dso, session, user_market.token);
                session_actions.forEach(action => {
                    action.selected = false;
                    action.session_id = session.id;
                    action.informationBrokerid = user_market.id;
                    all_actions.push(action);
                });
            }
        }

    });
    if (all_actions.length == 0)
        console.log("NO ACTIVE SESSION HAVE BEEN FOUND. BECAUSE THERE AR NO ACTIVE SESSIONS THE ACTIONS COULDN'T BE LOADED");
    return all_actions;
}

function getFlexibilityActions(user_market, market_id){
    flexibility_actions = [];

    flexibility_forms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling'];

    flexibility_forms.forEach(flexForm => {

        var session = getSessionsOfMarketByForm(user_market.url, MPLACE_URL, market_id, flexForm, user_market.token);
        var session_actions = getMarketActions(user_market.url, market_id, MPLACE_URL, username, is_dso, session, user_market.token);

        session_actions.forEach(action => {
            action.selected = false;
            action.session_id = session.id;
            action.informationBrokerid = user_market.id;
          flexibility_actions.push(action);
        });
    });

    return flexibility_actions;

}

function deleteCorrelation(correlation_id){
    var res = 0;
    data = {};
    data.correlationId = correlation_id;
    //console.log("deleteCorrelation");
    //console.log(memo_ip + correlation_endpoint);
    $.ajax({
        type: "DELETE",
        beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", token);
                    },
        url: memo_ip + correlation_endpoint,
        dataType: "json",
        data: JSON.stringify(data),
        contentType: 'application/json',
        async: false,
        success : function (response){
            data = response;
            res = 1;
        },
        error: function () {
            console.log("error deleting correlation " + memo_ip + correlation_endpoint);
        }
    });
    return res;
}