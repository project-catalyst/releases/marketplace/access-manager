"use strict";

function plotline(index, _chart, _chart_data, _title, _value, _series, _color){ 
		charts[index] = new Highcharts.Chart({
			chart: {
				renderTo : _chart,
				type: 'line'
			},
			title: {
				text: _title,
				style: {
					color: '#2EAADD',
					fontWeight: 'bold'
				}
			},
			rangeSelector:{
				enabled:true
			},
			xAxis: {
				type: 'datetime',
				title: {
					text: 'Time'
				}
			},
			yAxis: {
				title: {
					text: _value
				}
			},
			series: [{
				name: _series,
				data: _chart_data,
				color: _color,
				type : 'spline',
				marker:{enabled:false}
			}],
			credits: {
				enabled: false
			}
		});
}