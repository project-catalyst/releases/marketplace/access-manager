$(document).ready( function(){

}).on('click', '.view-market-session', function(){
   console.log(token);
   displayMarketSessionInfo($(this).data('pk'), token);

});


function displayMarketSessionInfo(pk, token){

    var marketSessionUrl = registeredMarket.url + MARKETSESSION_URL + pk + '/';
    console.log("Get market session with id: " + pk);
    console.log(marketSessionUrl);

    $.ajax({
        type: "GET",
        url: marketSessionUrl,
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token);
        },
        success: function (marketSession) {
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';

            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Session start time</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + datetimeUTCFormatter(marketSession.sessionStartTime) + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Session end time</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(marketSession.sessionEndTime) + '</span>',
                '</div>',
                '</div>',
                '<div id="quantity" class="form-group clearfix">',
                '<div  class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery start time</strong></label>',
                '</div>',
                '<div  class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(marketSession.deliveryStartTime) + '</span>',
                '</div>',
                '</div>',
                '<div id="deliveryPoint" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Delivery end time</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + datetimeUTCFormatter(marketSession.deliveryEndTime) + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong> Marketplace id </strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketSession.marketplaceid.id + ' ( '
                                           + marketSession.marketplaceid.marketServiceTypeid.type + ' '
                                           + marketSession.marketplaceid.marketServiceTypeid.timeFrameid.type  + ' )' +  '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Session status </strong></label>',
                '</div>',
// populate DSO list in new session modal
// $.ajax({
//     type: "GET",
//     url: market.url + DSOLIST_URL,
//     datatype: "json",
//     contenttype: 'application/json',
//     async: true,
//     beforeSend: function (xhr) {
//         console.log("DSO url: ");
//         console.log( market.url + DSOLIST_URL);
//         xhr.setRequestHeader("Authorization", token)
//     },
//     success: function (response) {
//         console.log(response);
//         $.each(response, function (key, value) {
//             $('#dso')
//                 .append($("<option></option>")
//                     .attr("value", value.id)
//                     .text(value.companyName));
//         });
//
//     }
// });

                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketSession.sessionStatusid.status + '</span>',
                '</div>',
                '</div>',
                 '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Form </strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketSession.formid.form + '</span>',
                '</div>',
                '</div>',
                 '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Clearing price</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketSession.clearingPrice + '</span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Market session details",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });

        },
        error: function (response) {
            console.log("Preview market session error");
            console.log(response);
        }
    });
}




