﻿

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", new Cookies().getValue('csrftoken'));
        }
    }
});

function customSessionRefresher(){

    var cntUserMarkets = user_markets.length;
    var sessions = [];
    var currentProcessedRegisteredMarkets = 0;

    // iterate through all the markets the admin has access to
    for(var idx in user_markets) {
        // get all the sessions in the current market
        let registeredMarketUrl = user_markets[idx].url;

       $.ajax({
           url: registeredMarketUrl + ALLSESSION_URL,
           type: 'GET',
           dataType: 'json',
           contentType: 'application/json',
           beforeSend: function (xhr) {
               xhr.setRequestHeader("Authorization", user_markets[idx].token);
           },
           success: function (data) {
               console.log("Url of current actions:" + registeredMarketUrl);



               for (var i in data) {

                   sessions.push({
                       id: data[i].id,
                       sessionStartTime: data[i].sessionStartTime,
                       sessionEndTime: data[i].sessionEndTime,
                       deliveryStartTime: data[i].deliveryStartTime,
                       deliveryEndTime: data[i].deliveryEndTime,
                       marketplace: data[i].marketplaceid.id,
                       status: data[i].sessionStatusid.status,
                       form: data[i].formid.form,
                       registered_market_url: registeredMarketUrl,
                   });

               }
               //console.log(sessions);
               // if all the markets have been queried for their sessions
               // display the data gathered in the sessions table
               currentProcessedRegisteredMarkets++;
               if(currentProcessedRegisteredMarkets === cntUserMarkets) {
                   console.log(sessions);
                   $('#sessions_table').bootstrapTable('load', sessions);
               }
           }
       });
   }

}

function setStatusFormatter(value, row, index) {

    var flag = {
        "unknown": {"type": "pending", "class": "label label-warning"},
        "valid": {"type": "valid", "class": "label label-success"},
        "invalid": {"type": "invalid", "class": "label label-danger"}
    };

    return [
        '<label class="' + flag[row["status"]]["class"] + '">',
        flag[row["status"]]["type"],
        '</label>'
    ].join('');
}

function setActionsFormatter(value, row, index) {
    if (row["type"] === "Market Operator") {
        return "-";
    }

    var validate = [
        '<a href="#" data-username="' + row["username"] + '" data-actor-id="' + row["id"] + '"  data-index="' + index+ '" data-token="'+ row["token"] + '" data-url="'+ row["market_url"] +  '" class="validate-participant" title="Validate participant">',
        '<span class="glyphicon glyphicon-ok text-success"></span>',
        '</a>'
    ].join('');



    var cancel = [
        '<a href="" data-username="' + row["username"] + '" data-actor-id="' + row["id"] + '"  data-index="' + index + '" data-token="'+ row["token"]+ '" data-url="'+ row["market_url"] +  '"class="cancel-participant" title="Cancel participant\'s validation">',
        '<span class="glyphicon glyphicon-remove text-danger "></span>',
        '</a>',
    ].join('');

    if (row["status"] == "unknown") {
        return validate + "  " + cancel;
    }
    else if (row["status"] == "valid") {
        return cancel;
    }
    else if (row["status"] == "invalid") {
        return validate;
    }
}




function marketplaceFormatter(value, row, index) {
    // make each IB ip available to each session
    const registeredMarketUrl = row.registered_market_url;
    return '<a href="#" class="marketplace" style="color:dodgerblue" data-id="' + value + '" data-registered-market-url="' + registeredMarketUrl +'">' + value + '</a>';

}

function utcFormatter(value, row, index){
    return UtcDateTime.convert(value);
}

function formsFormatter(value, row, index) {
    for (var i in forms) {
        if (forms[i]["form"] == value) {
            switch (forms[i]['form']) {
                case "electric_energy":
                case "thermal_energy":
                case "it_load":
                    return '<span class="label label-success">' + forms[i]['form'] + '</span>';
                case "congestion_management":
                case "spinning_reserve":
                case "scheduling"  :
                case "reactive_power_compensation":
                    return '<span class="label label-primary">' + forms[i]['form'] + '</span>';
            }
        }
    }
}

function SessionStatusFormatter(value, row, index) {
    for (var i in sessionStatuses) {
        if (sessionStatuses[i] == value) {
            switch (sessionStatuses[i]) {
                case "inactive":
                    return '<span class="label label-danger">' + sessionStatuses[i] + '</span>';
                case "active":
                    return '<span class="label label-success">' + sessionStatuses[i] + '</span>';
                case "closed":
                    return '<span class="label label-danger">' + sessionStatuses[i] + '</span>';
                case "cleared":
                    return '<span class="label label-default">' + sessionStatuses[i] + '</span>';
                case "completed":
                    return '<span class="label label-info">' + sessionStatuses[i] + '</span>';
                default:
                    return '<span class="label label-info">' + sessionStatuses[i] + '</span>';
            }
        }
    }
}

function newSession() {
    $("#modalAccept").html("Add");
    $("#modalAccept").unbind();
    $("#modalAccept").click(function () {
        submitSession();
    });
    $("#addSession").modal('show');
}

function setSessionChoices(type) {
    var tf = (type.val() == "energy") ? ["intra_day", "day_ahead"] : ["intra_day", "day_ahead"];
    var form = (type.val() == "energy") ? ["electric_energy", "thermal_energy", "it_load" ,] : ["congestion_management", "spinning_reserve", "reactive_power_compensation", "scheduling",];

    $("#form > option").each(function () {
        if (form.indexOf($(this).text()) == -1) {
            $(this).attr('disabled', 'disabled');
            $(this).parent().val('-1');
        }
        else {
            $(this).removeAttr('disabled');
        }
    });
    $("#timeframe > option").each(function () {
        if (tf.indexOf($(this).text()) == -1) {
            $(this).attr('disabled', 'disabled')
            $(this).parent().val('-1');
        }
        else {
            $(this).removeAttr('disabled');
        }
    });


}
function timeframe_mapper(number) {
    if (number == 1) {
        choice = "real_time";
    }
    if (number == 2){
        choice = "intra_day";
    }
    if (number ==3){
        choice = "day_ahead";
    }
    return choice
}



function setMarketplaceChoices() {
    type = $("#form").find("option:selected").text();
    timeframe = $("#timeframe").val();
    dso = $("#dso").val();
    $.ajax({
        type: "GET",
        url: SELECT_MPLACE + type + '/' + timeframe + '/',
        datatype: "json",
        contenttype: 'application/json',
        async: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        success: function (response) {
            console.log(response);
            $('#mplace').find('option').remove().end();
            for (var i in response) {
                $('#mplace')
                    .append($("<option></option>")
                        .attr("value", response[i])
                        .text(response[i]));
            }
        },
        error: function (request, status, errorThrown) {
            console.log('error')
            // There's been an error, do something with it!
            // Only use status and errorThrown.
            // Chances are request will not have anything in it.
        }

    });
}

function submitSession() {

    var valid = {
        a: SessionValidator.type($("#type")),
        b: SessionValidator.timeframe($("#timeframe")),
        d: SessionValidator.form($("#form")),
        e: SessionValidator.datetime($("#sessionStartTime")),
        f: SessionValidator.datetime($("#sessionEndTime")),
        g: SessionValidator.datetime($("#deliveryStartTime")),
        h: SessionValidator.datetime($("#deliveryEndTime"))
    };
    if (!(valid.a && valid.b && valid.d && valid.e && valid.f && valid.g && valid.h)) {
        return;
    }

    var form = $("#form").find("option:selected").text();
    var ancillaryServicesForm = ["congestion_management", "reactive_power_compensation", "spinning_reserve", "scheduling"];
    var marketForm = "";
    // if a ancillary services form is selected, get the "ancillary_services" market
    // the other forms (electric_energy, thermal_energy, it_load) correspond with the marketForm
    if(ancillaryServicesForm.indexOf(form) >= 0){
        marketForm = "ancillary-services";
    } else {
        marketForm = form;
    }

    var registeredMarketOfForm = getUserRegisteredMarketByForm(user_markets, marketForm);
    console.log("Market of form: " + marketForm);
    console.log(registeredMarketOfForm);

    var timeframe = $("#timeframe").val();
    var marketplaceid = getMarketByFormAndTimeframe(registeredMarketOfForm.url, form, timeframe);
    console.log("Post session mplace id: " + marketplaceid);

    var session = {
        "type": $("#type").val(),
        "timeframe": $("#timeframe").val(),
        "formid": getFormByValue(registeredMarketOfForm.url + FORMS, form),
        "marketplaceid": getMarketplace(registeredMarketOfForm.url + MPLACE_URL + marketplaceid + "/"),
        "sessionStatusid": getSessionStatusObject(registeredMarketOfForm.url + SESSION_STATUS_URL, "inactive"),
        "clearingPrice": null,
        "sessionStartTime": UtcDateTime.utcToIso($("#sessionStartTime").val()),
        "sessionEndTime": UtcDateTime.utcToIso($("#sessionEndTime").val()),
        "deliveryStartTime": UtcDateTime.utcToIso($("#deliveryStartTime").val()),
        "deliveryEndTime": UtcDateTime.utcToIso($("#deliveryEndTime").val())
    };
    $('#addSession').modal('hide');

    console.log("Post session URL: ");
    console.log(registeredMarketOfForm.url + MPLACE_URL + marketplaceid + '/marketsessions/');

    console.log("Post session payload: ");
    console.log(session);

    $.ajax({
        url: registeredMarketOfForm.url + MPLACE_URL + marketplaceid + '/marketsessions/',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(session),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarketOfForm.token)
        },
        success: function (data) {
            customSessionRefresher();
            console.log("session ok");
        },
        error: function () {
            customSessionRefresher();
            console.error("session error");
        },
        complete: function () {
            customSessionRefresher();
            resetSessionForm();
        }
    });
    return false;
}

function resetSessionForm() {
    $("#resetSessionForm").click();
}

function marketplaceInfo(markeplaceId, registeredMarketUrl) {
    console.log("GET market details at: " + registeredMarketUrl);
    $.ajax({
        type: "GET",
        url: registeredMarketUrl + MPLACE_URL + markeplaceId+'/',
        contentType: "application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        success: function (data) {
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';

            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Service type</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + data.marketServiceTypeid.type + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Timeframe</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + data.marketServiceTypeid.timeFrameid.type + ' </span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Marketplace information",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });
        },
        error: function () {
            console.error('markeplace info error');
        }
    });
}

$(document).ready(function (){
    customSessionRefresher();
});
