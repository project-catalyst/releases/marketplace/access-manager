﻿var RuleValidator = RuleValidator || (function () {
    return {
        title: function (elem) {
            if (elem.val() === "" || elem.val() === null || elem.val() === undefined) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        description: function (elem) {
            if (elem.val() === "" || elem.val() === null || elem.val() === undefined) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        }
    };
})();

var SessionValidator = SessionValidator || (function () {
    return {
        type: function (elem) {
            if (elem.val() === "" || elem.val() === null || !isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        timeframe: function (elem) {
            if (elem.val() === "" || elem.val() === null || !isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        dso: function (elem) {
            if (elem.val() === "" || elem.val() === null || isNaN(elem.val()) || elem.val() < 1) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        form: function (elem) {
            if (elem.val() === "" || elem.val() === null || isNaN(elem.val()) || elem.val() < 0) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        datetime: function (elem) {
            if (elem.val() === "" || elem.val() === null || elem.val() === undefined) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        clearingPrice: function (elem) {
            if ( (elem.val()).length > 0 && isNaN(elem.val()) ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
    };
})();