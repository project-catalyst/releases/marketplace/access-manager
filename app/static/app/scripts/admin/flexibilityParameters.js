var succes_div, warning_div;
var system_params = [];
var selected;

$(document).ready(function () {
    succes_div = document.getElementById("succesDiv");
    succes_div.style.display = 'none';
    warning_div = document.getElementById("warningDiv");
    warning_div.style.display = 'none';
    constructTable();
});

function constructTable(){
    var front_data = [];

    system_params = getSystemParameters(memo_ip);


     for (var i in system_params){
        var act = {
            id: system_params[i].id,
            name: system_params[i].name,
            value: system_params[i].value,
        };
        front_data.push(act);
      }
  $("#cor2").bootstrapTable("load", front_data);
}

function getSystemParameters(memo_ip){
  var data = [];
  console.log("get system params");
  console.log(memo_ip);
  $.ajax({
    type: "GET",
    url:memo_ip + "systemparameter/",
     beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
     },
    dataType: "json",
    contentType: 'application/json',
    async: false,
    success: function (response) {
        console.log("response for system parameters")
        console.log(response);
        data = response;
    }
  });
  console.log("response for system parameters")
  console.log(data);
  return data;
}

function adminEditRulesFormatter(value, row, index) {
    return [
        '<a class="edit" href="javascript:void(0)" title="Edit" onclick="editRule(' + row['id'] + ',\'' + String(row['value']) + '\',\'' + String(row['name']) + '\');">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>  '].join(' ')
    ;
}

function editRule(id, value,  name){
    selected = {'id': id, 'name':name};
    document.getElementById("new_value").value = value;
    $('#changeSettingsModal').modal('show');
    $('#name').text(name);
    $('#curr_value').text(value);
}

function updateSettings(){
    var value = document.getElementById("new_value").value;
    selected['value'] = value;

    $.ajax({
        type: "PUT",
        url:  memo_ip + "systemparameter/",
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(selected),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        async: false,
        success : function (response){
            succes_div.style.display = 'block';
            warning_div.style.display = 'none';

        },
        error: function (err) {
            succes_div.style.display = 'none';
           warning_div.style.display = 'block';
        }
    });
     $('#changeSettingsModal').modal('hide');
     constructTable();
}