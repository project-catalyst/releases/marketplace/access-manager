
var market = user_markets[0];
console.log("Market for standard information:");
console.log(market);

// populate form list new session modal
$.ajax({
    type: "GET",
    url:  market.url + FORM_URL,
    datatype: "json",
    contenttype: 'application/json',
    async: true,
    beforeSend: function (xhr) {
        console.log("Form url: ");
        console.log( market.url + FORM_URL);
        xhr.setRequestHeader("Authorization", token)
    },
    success: function (response) {
        $.each(response, function (key, value) {
            var tmp ={'id':value.id, 'form':value.form};
            forms.push(tmp);
            $('#form')
                .append($("<option></option>")
                    .attr("value", value.id)
                    .text(value.form));
        });

    }
});


// populate timeframe list in new session modal
$.ajax({
    type: "GET",
    url:  market.url + TIMEFRAME_LIST,
    datatype: "json",
    contenttype: 'application/json',
    async: true,
    beforeSend: function (xhr) {
        console.log("Timeframe url: ");
        console.log(  market.url + TIMEFRAME_LIST);
        xhr.setRequestHeader("Authorization", token)
    },
    success: function (response) {
        $.each(response, function (key, value) {
            $('#timeframe')
                .append($("<option></option>")
                    .attr("value", value.type)
                    .text(value.type));
        });

    }
});

function getMarketByFormAndTimeframe(ip,form, timeframe){

        var marketplace = 0;

        $.ajax({
        type: "GET",
        url: ip + SELECT_MPLACE + form + '/' + timeframe + '/',
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        success: function (response) {
            console.log("Got mplac id: " + response[0]);
           marketplace = response[0]; // market's ip
        },
        error: function (request, status, errorThrown) {

            displayInfo("Error", "No marketplace found for form: " + type + " and timeframe: " + timeframe,
                         "warning", "OK");
        }

    });

        console.log("Returned mplace");
        console.log(marketplace);

       return marketplace;

}

function getAllSessions(registeredMarketUrl){

    var sessions = [];

    $.ajax({
        url : registeredMarketUrl + ALLSESSION_URL,
        type : 'GET',
        datatype : 'json',
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        async: false,
        success : function(data) {

            for (var i in data){
                sessions.push({
                    id: data[i].id,
                    sessionStartTime: data[i].sessionStartTime,
                    sessionEndTime: data[i].sessionEndTime,
                    deliveryStartTime: data[i].deliveryStartTime,
                    deliveryEndTime: data[i].deliveryEndTime,
                    marketplace: data[i].marketplaceid.id,
                    status: data[i].sessionStatusid.status,
                    form: data[i].formid.form,
                });
            }

        }
    });

    return sessions;

}