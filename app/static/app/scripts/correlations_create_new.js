/*
this file contains the main structure of processing the data for creating new correlations
the calls for the server are all stored in correlation_action_calls.js

this is the main js file for correlationsDayahead.html and correlationsIntraday.html
*/

/*
document.ready is called when the page is loaded.

variable all_actions contain all actions that are taken from the backend. getAllActions can be found in correlation_action_calls.js
variable front_act contains the actions from all actions, keeping only the data needed for future processing
variable constraints contain all the constraints taken from the backend. getAllConstraints can be found in correlation_action_calls.js

at the end, the structured data is displayed
#cor2 - table contains front_act
#constraint-dropdown - contains all constraints
*/
var all_actions = [];
var succes_div, warning_div;
$(document).ready(function () {
    succes_div = document.getElementById("succesDiv");
    succes_div.style.display = 'none';
    warning_div = document.getElementById("warningDiv");
    warning_div.style.display = 'none';

    var front_data = [];
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    all_actions = getAllActions(user_markets, MPLACE_URL, username, timeframe, token, SELECT_MPLACE_URL, is_dso);
      for (var i in all_actions){
        var front_act = {
          id: all_actions[i].id,
          date: moment.utc(all_actions[i].date).format(timeFormatStr),
          actionStartTime: moment.utc(all_actions[i].actionStartTime).format(timeFormatStr),
          actionEndTime: moment.utc(all_actions[i].actionEndTime).format(timeFormatStr),
          value: parseFloat(all_actions[i].value).toFixed(3),
          deliveryPoint: (all_actions[i].deliveryPoint != " ") ? all_actions[i].deliveryPoint : "-",
          price: parseFloat(all_actions[i].price).toFixed(2),
          cpu: all_actions[i].cpu ? Math.trunc(all_actions[i].cpu) : "-",
          ram: all_actions[i].ram ? Math.trunc(all_actions[i].ram) : "-",
          disk: all_actions[i].disk ? Math.trunc(all_actions[i].disk) : "-",
          actionType: all_actions[i].actionTypeid.type,
          actionStatus: all_actions[i].statusid.id,
          loadid: all_actions[i].loadid,
          participant: all_actions[i].marketActorid.id,
          status : all_actions[i].statusid.status,
          type: all_actions[i].formid.form,
          selected: all_actions[i].selected,
          session_id: all_actions[i].session_id
        }
        front_data.push(front_act);
      }
      var dropdown_constraints = document.getElementById("constraint-dropdown");
      var constraints = getAllConstraints(memo_ip, username);

      for (var i in constraints){
          var option = document.createElement("option");
          option.text = constraints[i].description;
          option.id = constraints[i].id;
          dropdown_constraints.add(option);
      }
    $("#cor2").bootstrapTable('load', front_data);
});

function manageRulesFormatter(row, value, index) {
    return ('<input type="checkbox" onclick="selectAction(' + index + ')" ' + ((all_actions[index].selected) ? "checked=True" : "") + ' />');
}
function selectAction(index){
    all_actions[index].selected = !all_actions[index].selected;
}

/*
this function is used when pressing "add" button -> adding new correlation
the object to be sent contains : selected constraint, username, timeframe, and selected actions
postCorrelatedActions can be found in correlation_action_calls.js
 */
function addCorrelation(){
    var dropdown =  document.getElementById("constraint-dropdown");
    var selectedConstraint = dropdown[dropdown.selectedIndex].id;
    var send_obj = {};

    send_obj.constraintId = selectedConstraint;
    send_obj.username = username;
    send_obj.timeframe = timeframe;
    send_obj.timestamp = new Date().valueOf();
    send_obj.actions = [];
    all_actions.forEach(a =>{
        if (a.selected){
            var action = {};
            action.sessionId = a.session_id;
            action.actionId = a.id;
            action.informationBrokerId = a.informationBrokerid;
            send_obj.actions.push(action);
            a.selected = false;
        }
    });
    var response = postCorrelatedMarketActions(memo_ip, send_obj, token);
    succes_div.style.display = 'none';
    warning_div.style.display = 'none';
    if (response == 1)
        succes_div.style.display = 'block';
    else warning_div.style.display = 'block';
}
