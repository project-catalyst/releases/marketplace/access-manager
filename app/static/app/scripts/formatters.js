﻿var timeFormatStr = "YYYY-MM-DD HH:mm";

function time_formatter(value) {
	return moment(value).format(timeFormatStr);
}

function action_type_formatter(value) {
    // if (value.type === "offer") {
    //     return "sell";
    // } else if (value.type == "bid") {
    //     return "buy";
    // } else {
    //     return value.type;
    // }
    switch(value.type) {
          case "bid":
              return "<div style=\"color:red;\">Bid</div>";
              break;
          case "offer":
              return "<div style=\"color:green;\">Offer</div>";
              break;
          default:
              return "";
      }
}

function action_status_formatter(value, row, index) {
      switch(value.id) {
          case 1:
              return "<span class=\"label label-default\">Unchecked</span>";
              break;
          case 2:
              return "<span class=\"label label-info\">Valid</span>";
              break;
          case 3:
              return "<span class=\"label label-danger\">Invalid</span>";
              break;
          case 4:
              return "<span class=\"label label-info\">Active</span>";
              break;
          case 5:
              return "<span class=\"label label-warn\">Inactive</span>";
              break;
          case 6:
              return "<span class=\"label label-default\">Withdrawn</span>";
              break;
          case 7:
              return "<span class=\"label label-success\"><i class=\"fa fa-check\"></i> Accepted</span>";
              break;
          case 8:
              return "<span class=\"label label-info\"><i class=\"fa fa-check\"></i>Partially Accepted</span>";
              break;
          case 9:
              return "<span class=\"label label-warn\"><i class=\"fa fa-exclamation-triangle\"></i> Rejected</span>";
              break;
          default:
              return "<span class=\"label label-danger\">Invalid</span>";
      }
}


function stateFormatter(value, row, index) {
    if (row.actionStatusid.id === 8) {
        return {
            disabled: true,
            checked: true
        }
    }
    return value;
}