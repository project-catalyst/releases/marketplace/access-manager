$(document).ready(function () {

}).on('click', ".view-market-actor", function () {
    console.log(token);
    displayMarketActorDetails($(this).data('pk'),token);
});

function displayMarketActorDetails(pk, token) {

    console.log("Retrieveing market actor details from URL: ");
    console.log(registeredMarket.url +MARKET_ACTOR_URL + pk + '/');

    $.ajax({
        type: "GET",
        url: registeredMarket.url +MARKET_ACTOR_URL + pk + '/',
        datatype: "json",
        contenttype: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        success: function (marketActor) {
            var lclass = 'col-sm-5 col-md-5 col-lg-5 col-xs-12';
            var fclass = 'col-sm-7 col-md-7 col-lg-7 col-xs-12';

            console.log("Received market actor: ");
            console.log(marketActor);

            var table = [
                '<div class=row>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Company name</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left" >' + marketActor.companyName + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Email</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.email + '</span>',
                '</div>',
                '</div>',
                '<div id="quantity" class="form-group clearfix">',
                '<div  class="' + lclass + '">',
                '<label class="pull-right"><strong>Market actor type id</strong></label>',
                '</div>',
                '<div  class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.marketActorTypeid + '</span>',
                '</div>',
                '</div>',
                '<div id="deliveryPoint" class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Phone</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.phone + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Representative name</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.representativeName + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>Username</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.username + '</span>',
                '</div>',
                '</div>',
                '<div class="form-group clearfix">',
                '<div class="' + lclass + '">',
                '<label class="pull-right"><strong>VAT</strong></label>',
                '</div>',
                '<div class="' + fclass + '">',
                '<span class="pull-left">' + marketActor.vat + '</span>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');

            swal({
                html: true,
                title: "Market participant details",
                text: table,
                type: "info",
                confirmButtonText: "Close",
                confirmButtonColor: "btn-info"
            });

        },
        error: function (response) {
            console.log("Preview market actor error");
        }
    });
}