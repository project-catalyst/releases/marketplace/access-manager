﻿// Global timers
var usernameTimer = 0, emailTimer = 0, pwdConfirmTimer = 0;

$(document).ready(function () {

    $("#rg_name").keyup(function () {
        UserValidator.validateName($(this), $("#rg_name_info"));
    });

    $("#rg_lastname").keyup(function () {
        UserValidator.validateName($(this), $("#rg_lastname_info"));
    });

    $("#rg_username").keyup(function (event) {
        var element = $(this);
        // clear any interval on key up
        clearInterval(usernameTimer);
        // check the validity of username after user's action
        usernameTimer = setTimeout(
            function () {
                UserValidator.validateUsername(element, $("#rg_username_info"));
            },
            600
        );
    });

    $("#rg_pwd").keyup(function () {
        UserValidator.validatePwd($(this));
    });

    $('#rg_pwd_confirm').keyup(function () {
        var element = $(this);
        // clear any interval on key up
        clearInterval(pwdConfirmTimer);
        // check the validity of username after user's action
        pwdConfirmTimer = setTimeout(
            function () {
                UserValidator.confirmPwd($('#rg_pwd'), element);
            },
            400
        );
    });

    $("#rg_email").keyup(function (event) {
        var element = $(this);

        // clear any interval on key up
        clearInterval(emailTimer);
        // check if user's email exists - unique constraint
        emailTimer = setTimeout(
            function () {
                UserValidator.validateEmail(element, $("#rg_email_info"));
            },
            800
        );
    });

    $("#rg_mobile").keyup(function (event) {
        UserValidator.validateMobile($(this));
    });

    $("#rg_company").keyup(function (event) {
        UserValidator.validateCompanyName($(this));
    });

    $("#rg_actor_type").change(function (event) {
        UserValidator.validateActorType($(this));

        if ($("#rg_actor_type :selected").text() === "Market Participant" || $(this).val() === 2) {
            $("#rg_desired_dso_row").removeClass('hidden');
        }
        else {
            $("#rg_desired_dso_row").addClass('hidden');
            $("#rg_desired_dso").val('-1');
        }
    });

    $("#rg_desired_dso").change(function (event) {
        //UserValidator.validateDesiredDSO($(this));
    });

    $("#rg_vat").keyup(function (event) {
        UserValidator.validateVAT($(this));
    });


    $("#rg_question").keyup(function (event) {
        UserValidator.validateQuestion($(this));
    });


    var container = document.getElementById('market_places_checkbox');
    for(var i in available_markets){
        var market = available_markets[i];
        var checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.id = market.id;
        var market_type;


        if(market.form === "thermal_energy"){
           market_type = "Heat";
        } else if(market.form === "electric_energy"){
            market_type = "Electricity";
        } else if(market.form === "it_load"){
           market_type = "IT Load";
        } else if(market.form === "ancillary-services"){
           market_type = "Flexibility";
        }

        checkbox.name = market.type;
        checkbox.value = market.form;

        var label = document.createElement('label');
        label.htmlFor = market.type;
        label.appendChild(document.createTextNode( market_type));

        var br = document.createElement('br');


        container.appendChild(checkbox);
        container.appendChild(label);
        container.appendChild(br);
    }

});


$("form").submit(function (event) {
    event.preventDefault();
    var state = true;

    // Fields validation
    var nameState       = UserValidator.validateName($("#rg_name"), $("#rg_name_info"));
    var surnameState    = UserValidator.validateName($("#rg_lastname"), $("#rg_lastname_info"));
    var usrState        = UserValidator.validateUsername($("#rg_username"), $("#rg_username_info"));
    var pwdState        = UserValidator.validatePwd($("#rg_pwd"));
    var confirmState    = UserValidator.confirmPwd($("#rg_pwd"), $("#rg_pwd_confirm"));
    var emailState      = UserValidator.validateEmail($("#rg_email"), $("#rg_email_info"));
    var mobileState     = UserValidator.validateMobile($("#rg_mobile"));
    var actorTypeState  = UserValidator.validateActorType($("#rg_actor_type"));
    var vatState        = UserValidator.validateVAT($("#rg_vat"));
    var answer          = UserValidator.validateQuestion($("#rg_question"));
    var companyBrandNameState = UserValidator.validateCompanyName($("#rg_company"));
    var preferedDSO      = UserValidator.validateDesiredDSO($("#rg_desired_dso"));
    var selectedMarkets  = UserValidator.validateSelectedMarkets($('#market_places_checkbox :checkbox:checked'));

    if (!(nameState && surnameState && usrState && pwdState && confirmState && emailState && mobileState && actorTypeState && vatState && companyBrandNameState && answer && selectedMarkets)) {
        return false;
    }
    if (!$("#rg_desired_dso_row").hasClass('hidden')) {
        if (!preferedDSO) {
            return false;
        }
    }

    var check_array = [];
    $('#market_places_checkbox :checkbox:checked').each(function(){
    check_array.push($(this).val());
    });

    console.log("Markets selected by user");
    console.log(check_array);

    var payload = {
        first_name: $("#rg_name").val(),
        last_name: $("#rg_lastname").val(),
        username: $("#rg_username").val(),
        password: $("#rg_pwd").val(),
        email: $("#rg_email").val(),
        phone: $("#rg_mobile").val(),
        market_actor_id: $("#rg_actor_type").val(),
        vat: $("#rg_vat").val(),
        company_name: $("#rg_company").val(),
        dso_id: $("#rg_desired_dso").val(),
        selected_markets: check_array,
    };
    var loading = new AjaxView($("#registrationForm"));
    loading.show();
    $.ajax({
        type: "POST",
        url: "validation/",
        datatype: "json",
        contenttype: 'application/json',
        data: JSON.stringify(payload),
        async: true,
        success: function (response) {
            console.log(response);
            if (response.state === true) {
                swal({
                    html: false,
                    title: "Registration progress",
                    text: 'Your request has submitted. Please check your email account.',
                    type: "info",
                    confirmButtonText: "Back to login page",
                    confirmButtonColor: "btn-info"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location = response.redirect;
                    }
                });
            }
            else {
                swal({
                    html: false,
                    title: "Registration progress",
                    text: 'Please enter a variation of the username you just entered. The specific username has already been given to an other user.',
                    type: "warning",
                    confirmButtonText: "Continue",
                    confirmButtonColor: "btn-danger"
                });
            }
        },
        error: function (response) {
            swal({
                html: false,
                title: "Registration progress",
                text: 'Your request has skipped. An error has occurred.',
                type: "error",
                confirmButtonText: "Try again!",
                confirmButtonColor: "btn-danger"
            });
        },
        complete: function () { loading.hide(); }
    });

    return false;
});

