﻿
var UserValidator = UserValidator || (function () {
    var nameRE  = /^.*[a-zA-Z0-9]{2,50}$/;
    var usrRE   = /^([a-zA-Z0-9_\-]{4,})+$/;
    var pwdRE   = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    var emailRE = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var phoneRE = /^([0-9]{10,15})$/;
    var vatRE   = /^([0-9]{7,45})$/;

    return {
        validateName: function (name, label) {
            if (!nameRE.test(name.val())) {
                name.parent().addClass("has-error");
                label.removeClass("hidden");
                return false;
            }
            name.parent().removeClass("has-error");
            label.addClass("hidden");
            return true;
        },
        validateUsername: function (username, label) {
            label.empty();
            var valid = true;
            var message = "Type at least 4 characters";

            if (!usrRE.test(username.val())) {
                username.parent().addClass("has-error");
                label.append(message);
                label.removeClass("hidden");
                return false;
            }

            username.parent().removeClass("has-error");
            label.addClass("hidden");
            return true;
        },
        validatePwd: function (pass) {
            var state = true;
            if (!pwdRE.test(pass.val())) {
                state = false;
            }

            if (!state) {
                pass.parent().addClass("has-error");
                $("#rg_pwd_info").removeClass("hidden");
                return false;
            }
            pass.parent().removeClass("has-error");
            $("#rg_pwd_info").addClass("hidden");
            return true;
        },
        confirmPwd: function (password, confirm) {
            if (password.val() != confirm.val() || confirm.val().length < 1) {
                confirm.parent().addClass("has-error");
                $("#rg_pwd_confirm_info").removeClass("hidden");
                return false;
            }
            $("#rg_pwd_confirm_info").addClass("hidden");
            confirm.parent().removeClass("has-error");
            return true;
        },
        validateEmail: function (email, label) {
            label.empty();
            var valid = true;
            var message = "Type a valid email";

            if (!emailRE.test(email.val())) {
                email.parent().addClass("has-error");
                label.append(message);
                label.removeClass("hidden");
                return false;
            }
            email.parent().removeClass("has-error");
            label.addClass("hidden");
            return true;
        },
        validateMobile: function (mobile) {
            if (!phoneRE.test(mobile.val())) {
                mobile.parent().addClass('has-error');
                $("#rg_mobile_info").removeClass('hidden');
                return false;
            }
            mobile.parent().removeClass('has-error');
            $("#rg_mobile_info").addClass('hidden');
            return true;
        },
        validateCompanyName: function (name) {
            if (!nameRE.test(name.val())) {
                name.parent().addClass('has-error');
                name.addClass("error");
                $("#rg_company_info").removeClass("hidden");
                return false;
            }
            name.parent().removeClass('has-error');
            name.removeClass("error");
            $("#rg_company_info").addClass("hidden");
            return true;
        },
        validateActorType: function (type) {
            if (type.val() < 0 || type.val() === undefined || type.val() === null) {
                type.parent().addClass('has-error');
                $("#rg_actor_type").addClass("error");
                $("#rg_actor_type_info").removeClass("hidden");
                return false;
            }
            type.parent().removeClass('has-error');
            $("#rg_actor_type").removeClass("error");
            $("#rg_actor_type_info").addClass("hidden");
            return true;
        },
        validateDesiredDSO: function (dso) {
            if (dso.val() < 0 || dso.val() === undefined || dso.val() === null) {
                dso.parent().addClass('has-error');
                $("#rg_desired_dso").addClass("error");
                $("#rg_desired_dso_info").removeClass("hidden");
                return false;
            }
            dso.parent().removeClass('has-error');
            $("#rg_desired_dso").removeClass("error");
            $("#rg_desired_dso_info").addClass("hidden");
            return true;
        },
        validateVAT: function (vat) {
            if (!vatRE.test(vat.val())) {
                vat.parent().addClass('has-error');
                vat.addClass("error");
                $("#rg_vat_info").removeClass("hidden");
                return false;
            }
            vat.parent().removeClass('has-error');
            vat.removeClass("error");
            $("#rg_vat_info").addClass("hidden");
            return true;
        },
        validateQuestion: function (answer) {
            var d = $("span#literal").text().split("+");
            var sum = parseInt(d[0]) + parseInt(d[1]);
            if (answer.val() != sum) {
                answer.parent().addClass("has-error");
                $("#rg_question_info").removeClass("hidden");
                return false;
            }
            answer.parent().removeClass("has-error");
            $("#rg_question_info").addClass("hidden");
            return true;
        },
        validateSelectedMarkets: function (inputs) {
            if (inputs.length == 0) {
                inputs.parent().addClass('has-error');
                $("#rg_marketplaces").addClass("error");
                $("#rg_marketplaces_info").removeClass("hidden");
                return false;
            }
            inputs.parent().removeClass('has-error');
            $("#rg_marketplaces").removeClass("error");
            $("#rg_marketplaces_info").addClass("hidden");
            return true;
        }
    };
})();