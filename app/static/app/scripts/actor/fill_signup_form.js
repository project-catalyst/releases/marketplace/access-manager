
$.ajax({
type: "GET",
url: available_markets[0].url + signup_form_urls,
datatype: "json",
contenttype: 'application/json',
async: true,
success: function (response) {
    market_actors = response['market_actors'];
    dso_list = response['dso_list'];
    $.each(market_actors, function(key, value) {
        // do not allow new accounts to choose msm or aggregator as actor type
        // aggregator is only a 'generic_participant', and msm is used to identify the MSM / MEMPO components
         if(value.type !== "aggregator" && value.type !== "msm" &&
            value.type !== "mcm" && value.type !== "mbm" &&
            value.type !== "memo") {

             $('#rg_actor_type')
                 .append($("<option></option>")
                     .attr("value", value.id)
                     .text(value.type));
         }
    });
    $.each(dso_list, function(key, value) {
     $('#rg_desired_dso')
         .append($("<option></option>")
                    .attr("value",value.id)
                    .text(value.companyName));
    });

}
});