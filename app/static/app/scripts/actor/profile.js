var usernameTimer = 0, emailTimer = 0, pwdConfirmTimer = 0;

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            xhr.setRequestHeader("X-CSRFToken", new Cookies().getValue('csrftoken'));
        }
    }
});

$(document).ready(function () {
    fillProfileMplaces();
    $("#edit-personal-btn").click(function (event) {
        $(this).parent().addClass('hidden');
        $(".preview-personal-info").addClass('hidden');
        $("#personal-toolbar :nth-child(2)").removeClass('hidden');
        $(".editable-personal-info").removeClass('hidden');
    });

    $("#reset-personal-btn").click(function () {
        $("#personal-toolbar :nth-child(1)").removeClass('hidden');
        $(".preview-personal-info").removeClass('hidden');
        $("#personal-toolbar :nth-child(2)").addClass('hidden');
        $(".editable-personal-info").addClass('hidden');
    });

    $("#save-personal-btn").click(function () {
        profileUpdate();
    });

    $("#rg_name").keyup(function () {
        UserValidator.validateName($(this), $("#rg_name_info"));
    });

    $("#rg_lastname").keyup(function () {
        UserValidator.validateName($(this), $("#rg_lastname_info"));
    });

    $("#rg_email").keyup(function (event) {
        var element = $(this);

        // clear any interval on key up
        clearInterval(emailTimer);
        // check if user's email exists - unique constraint
        emailTimer = setTimeout(
            function () {
                UserValidator.validateEmail(element, $("#rg_email_info"));
            },
            800
        );
    });

    $("#rg_mobile").keyup(function (event) {
        UserValidator.validateMobile($(this));
    });

    $("#rg_company").keyup(function (event) {
        UserValidator.validateCompanyName($(this));
    });

    $("#rg_actor_type").change(function (event) {
        UserValidator.validateActorType($(this));
    });

    $("#rg_vat").keyup(function (event) {
        UserValidator.validateVAT($(this));
    });
});


function profileUpdate() {
    var state = true;
    // Fields validation
    var nameState       = UserValidator.validateName($("#rg_name"), $("#rg_name_info"));
    var surnameState    = UserValidator.validateName($("#rg_lastname"), $("#rg_lastname_info"));
    var emailState      = UserValidator.validateEmail($("#rg_email"), $("#rg_email_info"));
    var mobileState     = UserValidator.validateMobile($("#rg_mobile"));
    var actorTypeState  = UserValidator.validateActorType($("#rg_actor_type"));
    var vatState        = UserValidator.validateVAT($("#rg_vat"));
    var companyBrandNameState = UserValidator.validateCompanyName($("#rg_company"));

    if (!(nameState && surnameState && emailState && actorTypeState && vatState && companyBrandNameState & mobileState)) {
        return false;
    }

    // Create a JS object
    var payload = {
        first_name: $("#rg_name").val(),
        last_name: $("#rg_lastname").val(),
        email: $("#rg_email").val(),
        phone: $("#rg_mobile").val(),
        market_actor_id: $("#rg_actor_type").val(),
        vat: $("#rg_vat").val(),
        company_name: $("#rg_company").val()
    };

    // upload data
    $.ajax({
        type: "PUT",
        url: user_markets[0].url + UPDATE_PROFILE_URL + username + '/',
        datatype: "json",
        contenttype: 'application/json',
        data: payload,
        async: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        success: function (response) {
            if (response.state === true) {
                location.reload();
            }
            else {
                swal({
                    html: false,
                    title: "Update profile progress",
                    text: 'An error has detected during your profile modification.',
                    type: "warning",
                    confirmButtonText: "Try again!",
                    confirmButtonColor: "btn-danger"
                });
            }
        },
        error: function (response) {
            swal({
                html: false,
                title: "Registration progress",
                text: 'Your request has skipped. An error has occurred.',
                type: "error",
                confirmButtonText: "Try again!",
                confirmButtonColor: "btn-danger"
            });
        },
        complete: function () { }
    });

    return false;
}
