$(document).ready(function () {
    $.ajax(
        user_markets[0].url + GET_PROFILE_URL + username + '/',
        {
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token)
            },
            complete: function (resp) {
                rules = JSON.parse(resp.responseText)
                console.log(rules);
                market_actors = rules.market_actors;
                if (rules.actor != null) {
                    $('div#hold11').attr('style', '')
                }
                else {
                    $('div#hold10').attr('style', '')
                }
                $('span#preview_id').append(rules.actor.id);
                $('span#preview_name').append(rules.user.first_name);
                $('span#preview_lastname').append(rules.user.last_name);
                if (rules.actor.companyName) {
                    $('div#hold1').attr('style', '');
                    $('span#preview_company').append(rules.actor.companyName);
                }
                else {
                    $('div#hold2').attr('style', '');
                }
                if (rules.actor.email) {
                    $('div#hold3').attr('style', '');
                    $('span#preview_email').append(rules.actor.email);
                }
                else {
                    $('div#hold4').attr('style', '');
                }
                if (rules.actor.phone) {
                    $('div#hold5').attr('style', '');
                    $('span#preview_phone').append(rules.actor.phone);
                }
                else {
                    $('div#hold6').attr('style', '');
                }
                if (rules.actor.vat) {
                    $('div#hold7').attr('style', '');
                    $('span#preview_vat').append(rules.actor.vat);
                }
                else {
                    $('div#hold8').attr('style', '');
                }
                $('span#preview_role').append(rules.actor.marketActorTypeid.type);
                if ((rules.dso != null) && rules.user.username != "admin") {
                    $('div#holder90').attr('style', '');
                    $('span#dso').append(rules.dso.representativeName + " " + "(" + rules.dso.id.toString() + ")");
                }
                $('input#rg_name').val(rules.user.first_name);
                $('input#rg_lastname').val(rules.user.last_name);
                $('input#rg_company').val(rules.actor.companyName);
                $('input#rg_email').val(rules.actor.email);
                $('input#rg_mobile').val(rules.actor.phone);
                $('input#rg_vat').val(rules.actor.vat);

                $.each(market_actors, function (key, value) {
                    if (value.id === rules.actor.marketActorTypeid.id) {
                        $('#rg_actor_type').append($("<option></option>").attr({
                            "value": (value.id).toString(),
                            "selected": true
                        }).text(value.type));
                    } else {
                        $('#rg_actor_type').append($("<option></option>").attr({
                            "value": (value.id).toString(),
                            "disabled": true
                        }).text(value.type));
                    }
                });
                fillProfileMplaces();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                fillProfileMplaces();
                console.log(textStatus);
            }
        }
    );
});