
var registeredMarket;
var actor_id;
var MPLACE;
var sessions;
var ref_price;
var actorId;

$(document).ready(function () {

   //console.log("Market: " + form );
   //console.log("Authenticated: " + is_authenticated);
   registeredMarket = getUserRegisteredMarketByForm(user_markets, form);
   MPLACE = getMarketID(type);
   sessions = getSessionsofMarket();
   ref_price = getReferencePrice(form);
   setIntraMarketSessionInfo();

  //console.log("Registered " + form + " market: ");
  //console.log(registeredMarket);

  actorId = getActorId(registeredMarket.url, USERNAME_TO_ACTOR, registeredMarket.token, username);
  //console.log("Registered actor id: " + actorId);

});

function getMarketID(type) {
    var mplace_intra_day = 0;



    $.ajax({
        type: "GET",
        url: registeredMarket.url + SELECT_MPLACE_URL+ type + "/intra_day/" + username + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
            if (response.length > 1) {
                swal({
                       title: "Error",
                       text: 'Multiple marketplaces received!',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
                return false;

            } else if(response.length === 0){
                 swal({
                       title: "Error",
                       text: 'You are not a participant in any market of this type! \n You will be redirected to home',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   },
                     function (isConfirm) {
                        if(isConfirm){
                          location.href = "/";
                        }
                     });
            }

            else {
                mplace_intra_day = response[0];
            }
        },
        error: function () {
            console.log("error getting market id from " + registeredMarket.url + SELECT_MPLACE_URL+ type + "/intra_day/" + username + "/");
        }
    });

    return mplace_intra_day;
}

function getSessionsofMarket() {
    var ses;
    if (MPLACE != null) {
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/active/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            success: function (response) {
                if (response.length > 1) {
                        swal({
                           title: "Error",
                           text: 'Multiple sessions received!',
                           type: "warning",
                           confirmButtonText: "OK",
                           confirmButtonColor: "#009245",
                       });
                        ses = null;
                        return ses;
                }
                if ($.trim(response) != '') {
                    ses = response[0];
                } else {
                    ses = null;
                    swal({
                       title: "Info",
                       text: 'No active sessions for this marketplace!',
                       type: "info",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   });
                }
            },
            error: function () {console.log(response);}
        });
    }
    return ses;
}

function getReferencePrice(form){
    if (MPLACE != null) {
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/form/" + form + "/date/now/referencePrice/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: false,
            success: function (response) {
                if ($.trim(response) != '') {
                    ref_price = response;
                } else {
                    ref_price = null;
                }
            },
            error: function () {
                ref_price = null;
                console.log("error getting reference price from " + registeredMarket.url + MPLACE_URL + MPLACE + "/form/" + form + "/date/now/referencePrice/")
            }
        });
    }
    return ref_price;
}

function setIntraMarketSessionInfo() {
    var tmp;
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    var data = [];
    if (sessions != null) {
        $("#intra_table_data_helper").attr('style', '');
        $("#intra_session_helper").remove();
        $("#intra_session_helper1").remove();
        $("#intra_refprice_helper").remove();
        var div1 = '<div id="intra_session_helper">Session is active from ' + moment.utc(sessions.sessionStartTime).format('YYYY-MM-DD HH:mm ZZ') + ' until <b>' + moment.utc(sessions.sessionEndTime).format('YYYY-MM-DD HH:mm ZZ') + '</b></div>';
        var div2 = '<div id="intra_session_helper1">Delivery is active from ' + moment.utc(sessions.deliveryStartTime).format('YYYY-MM-DD HH:mm ZZ') + ' until ' + moment.utc(sessions.deliveryEndTime).format('YYYY-MM-DD HH:mm ZZ') + '</div>';
        if (ref_price != null) {
            if( form == "electric_energy") {
                var div3 = '<div id="refprice_helper">Reference price is ' + parseFloat(ref_price.price).toFixed(2).replace('.', ',') + '/MWh.</div>';
            } else if (form == "thermal_energy") {
                var div3 = '<div id="refprice_helper">Reference price is ' + parseFloat(ref_price.price).toFixed(2).replace('.', ',') + '/MWht.</div>';
            } else if (form == "it_load") {
                var div3 = '<div id="refprice_helper">Reference price is ' + parseFloat(ref_price.price).toFixed(2).replace('.', ',') + '/KWh.</div>';
            }
        } else {
            var div3 = '<div id="intra_refprice_helper">Not set</div>';
        }

        $("#intra_session_holder").append(div1);
        $("#intra_session_holder").append(div2);
        $("#intra_session_holder").append(div3);

        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + sessions.id + "/actions/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: true,
            success: function (response) {
                for (var i in response){

                     //console.log("Action: " + i);
                     //console.log(response[i]);

                    var act = {
                        id: response[i].id,
                        date: moment.utc(response[i].date).format(timeFormatStr),
                        actionStartTime: moment.utc(response[i].actionStartTime).format(timeFormatStr),
                        actionEndTime: moment.utc(response[i].actionEndTime).format(timeFormatStr),
                        value: parseFloat(response[i].value).toFixed(3),
                        deliveryPoint: response[i].deliveryPoint,
                        price: parseFloat(response[i].price).toFixed(2),
                        cpu: Math.trunc( response[i].cpu  ),
                        ram: Math.trunc(response[i].ram),
                        disk: Math.trunc(response[i].disk),
                        actionType: response[i].actionTypeid.type,
                        actionStatus: response[i].statusid.id,
                        loadid: response[i].loadid,
                        participant: response[i].marketActorid.id
                    }
                    data.push(act);
                }
                $("#intra_data_table_session").bootstrapTable('load', data);
            }
        });
    } else {
        $("#intra_session_helper").remove();
        $("#intra_session_helper1").remove();
        $("#intra_refprice_helper").remove();
    }

}
function intra_customRefresher(){
    var data = [];
    var timeFormatStr = "YYYY-MM-DD HH:mm ZZ";
    if (sessions != null) {
        $("#intra_table_data_helper").attr('style', '');
        $.ajax({
            type: "GET",
            url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketparticipants/" + username + "/dso/" + is_dso + "/marketsessions/" + sessions.id + "/actions/",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
            datatype: "json",
            contenttype: 'application/json',
            async: true,
            success: function (response) {
                for (var i in response){
                    // console.log("Action: " + i);
                    // console.log(response[i]);

                    var act = {
                        id: response[i].id,
                        date: moment.utc(response[i].date).format(timeFormatStr),
                        actionStartTime: moment.utc(response[i].actionStartTime).format(timeFormatStr),
                        actionEndTime: moment.utc(response[i].actionEndTime).format(timeFormatStr),
                        value: parseFloat(response[i].value).toFixed(3),
                        deliveryPoint: response[i].deliveryPoint,
                        price: parseFloat(response[i].price).toFixed(2),
                        cpu: Math.trunc( response[i].cpu  ),
                        ram: Math.trunc(response[i].ram),
                        disk: Math.trunc(response[i].disk),
                        actionType: response[i].actionTypeid.type,
                        actionStatus: response[i].statusid.id,
                        loadid: response[i].loadid,
                        participant: response[i].marketActorid.id
                    }
                    data.push(act);
                }
                $("#intra_data_table_session").bootstrapTable('load',data);
            }
        });
    }
}

function typeIntraFormatter(value,row, index) {
    if(value =='offer'){
        return "<div style=\"color:red;\">Offer</div>";
    }else{
        return "<div style=\"color:green;\">Bid</div>";
    }
}

function manageRulesIntraFormatter(value, row, index) {
    return [
        '<a class="edit" href="javascript:void(0)" title="Edit" onclick="editAction(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>  ',
        '<a class="remove" href="javascript:void(0)" title="Remove" onclick="removeAction(' + row['id'] + ');">',
        '<i class="glyphicon glyphicon-trash" style="color:red;"></i>',
        '</a>'
    ].join('');
}
function intra_modal_actuate(url, method, id, loadid){

    console.log("#### Intra modal actuate #####");

    var valid = {
        a: ActionValidator.datetime($("#actionStartTime")),
        b: ActionValidator.datetime($("#actionEndTime")),

        e: ActionValidator.price($("#price")),
        e: ActionValidator.price($("#price")),
        f: ActionValidator.deliveryPoint($("#deliveryPoint")),
        g: ActionValidator.type($("#typel"))
    };

    console.log("#### After actuation #####");

    if(form == "it_load"){
        valid['h'] = ActionValidator.cpu($("#cpu"));
        valid['i'] = ActionValidator.ram($("#ram"));
        valid['j'] = ActionValidator.disk($("#disk"));

        valid['c'] = true;
        valid['d'] = true;
    } else {
         valid['c'] =  ActionValidator.value($("#value"));
         valid['d'] = ActionValidator.uom($("#uoml"));
    }

    if (!(valid.a && valid.b && valid.c && valid.d && valid.e && valid.f && valid.g) ) {
        return;
    }

     if(form == "it_load" && !(valid.h && valid.i && valid.j)){
         return;
    }


    var $inputs = $('#AddActionForm input');
    var list = [];
    var values = {};

    values['cpu'] = 0.0;
    values['ram'] = 0.0;
    values['disk'] = 0.0;
    values['value'] = 0.0;
    values['uom'] = 'none';
    values['deliveryPoint'] = ' ';


    $inputs.each(function () {
        if (this.id === "type") {
            values["actionTypeid"] =  getActionType(registeredMarket.url + ACTION_TYPE_URL, $(this).val());
        } else {
            values[this.id] = $(this).val();
        }
        $(this).val('');
        $('#uom').val($('#uoml').val());
        $('#type').val($('#typel').val());
    });


    values['date'] = moment.utc().format();
    values['actionStartTime'] = moment.utc(values['actionStartTime'], 'YYYY-MM-DD HH:mm Z').format();
    values['actionEndTime'] = moment.utc(values['actionEndTime'], 'YYYY-MM-DD HH:mm Z').format();
    values['marketSessionid'] = sessions.id;
    values['marketActorid'] = actorId;
    values['formid'] = getSessionForm(registeredMarket.url + MPLACE_URL+ MPLACE +"/marketsessions/" + sessions.id + "/", MPLACE, sessions.id);
    values['statusid'] = getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "unchecked");

    if(loadid != null && form == "it_load"){
        values['loadid'] = loadid;
    } else {
         values['loadid'] = null;
    }

    if (id != null) {
        values['id'] = id;
    }


    $('#addAction').modal('hide');

    var payload = null;
    if (method === "POST") {
        list.push(values);
        payload = list;
    }
    else {
        payload=values;
    }

    $.ajax({
      url:url,
      type: method,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
      dataType:"json",
      contentType: "application/json; charset=utf-8",
      data:JSON.stringify(payload),
      success: function(data){
          intra_customRefresher();
      },
      error: function(){
        intra_customRefresher();
      }
    });
}


function newAction() {
    if (ref_price !=null){
        $("#reference_price").text(parseFloat(ref_price.price).toFixed(2).replace('.', ','));
    } else {
        $("#reference_price").text("Not set");
    }
    $("#actionStartTime").val('');
    $("#actionEndTime").val('');
    $("#price").val('');
    $("#deliveryPoint").val('');
    $("#value").val('');
    $("#typel").val('').change();
    $("#uoml").val('').change();

    $("#modalAccept").html("Add");
    $("#modalAccept").unbind();



    if(form == "it_load"){

         $("#modalAccept").click(function(){
            var date = moment.utc().format();
            actuateLoad("POST", date);

        });

    } else {

        $("#modalAccept").click(function () {
            var u1 = registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + sessions.id + "/actions/";
            intra_modal_actuate(u1, "POST", null, null);
        });
    }


    $("#addActionLabel").text("Place a new market action");
    $("#addAction").modal('show');
}


function editAction(id){
    var url = registeredMarket.url +ACTION_URL + id + "/";
    if (ref_price !=null){
        $("#reference_price").text(parseFloat(ref_price.price).toFixed(2).replace('.', ','));
    } else { $("#reference_price").text("Not set"); }
    $.ajax({
        url:url,
        type:"GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        success:function( data ) {
            $("#actionStartTime").val(moment.utc(data.actionStartTime).format('YYYY-MM-DD HH:mm ZZ'));
            $("#actionEndTime").val(moment.utc(data.actionEndTime).format('YYYY-MM-DD HH:mm ZZ'));
            $("#price").val(data.price);
            $("#deliveryPoint").val(data.deliveryPoint);
            $("#value").val(data.value);

          if (data.actionTypeid.type == "offer") {
                $("#typel").val('offer').change();
           } else {
                $("#typel").val('bid').change();
           }

          if (data.uom == "KWh") {
              $("#uoml").val('KWh').change();
          } else if(data.uom == "MWht"){
                   $("#uoml").val('MWht').change();
          }
          else {
              $("#uoml").val('MWh').change();
          }

           if(form == 'it_load'){
              $("#cpu").val(data.cpu);
              $("#ram").val(data.ram);
              $("#disk").val(data.disk);
          }

          $("#modalAccept").html("Update");
          $("#modalAccept").unbind();
          $("#modalAccept").click(function(){
              var u1 = registeredMarket.url +  MPLACE_URL + MPLACE + "/marketsessions/" + sessions.id + "/actions/" + data.id + "/edit/";

              var loadid = null;

              if(data.loadid !== null){
                loadid = data.loadid.id;
              }

              intra_modal_actuate(u1, "PUT", data.id, loadid);
          });
          $("#addActionLabel").text("Update market action");
          $("#addAction").modal('show');
        }
    });
}

function removeAction(id) {
    var list_data = {};
    $.ajax({
        url:registeredMarket.url +ACTION_URL + id + "/",
        type:"GET",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", registeredMarket.token)
        },
        success:function( data ) {
            list_data['statusid'] = getActionStatus(registeredMarket.url + ACTION_STATUS_URL, "withdrawn");
            list_data['actionTypeid'] = data['actionTypeid'].id;
            list_data['id'] = data.id;
            list_data['formid'] = data['formid'].id;
            list_data['marketActorid'] = data['marketActorid'].id;
            list_data['marketSessionid'] = data['marketSessionid'].id;
            list_data['actionEndTime'] = data['actionEndTime'];
            list_data['actionStartTime'] = data['actionStartTime'];
            list_data['date'] = data['date'];
            list_data['deliveryPoint'] = data['deliveryPoint'];
            list_data['uom'] = data['uom'];
            list_data['price'] = data['price'];
            list_data['value'] = data['value'];

            list_data['loadid'] = null;
            list_data['cpu'] = 0.0;
            list_data['ram'] = 0.0;
            list_data['disk'] = 0.0;

              if(form == 'it_load'){
                  list_data['loadid'] = data['loadid'].id;
                  list_data['cpu'] = data['cpu'];
                  list_data['ram'] = data['ram'];
                  list_data['disk'] = data['disk'];
              }

              console.log("REMOVE ACTION: Loadid: " + list_data['loadid'] + " CPU: " + list_data['cpu'] + " Ram: " +  list_data['ram'] + "Disk: " + list_data['disk'] );
              console.log("REMOVE ACTION: ");
              console.log(list_data);

            options = {headers: {"Authorization": registeredMarket.token}};
            $.ajax({
                type: "PUT",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", registeredMarket.token)
                },
                url: registeredMarket.url + MPLACE_URL + MPLACE + "/marketsessions/" + sessions.id + "/actions/" + data.id + "/edit/",
                contentType: "application/json",
                data: JSON.stringify(list_data),
                success: function(){
                    intra_customRefresher();
                },
                error: function(){
                    intra_customRefresher();
                }
            });
        }
    });
}


function actuateLoadValue(method, loadid){

    var cpu = {}, ram = {}, disk = {};

    var loadValuesList = [];


        if(method == "POST") {

            cpu['loadid'] = loadid;
            cpu['parameter'] = "CPU";
            cpu['uom'] = "core";
            cpu['value'] = $("#cpu").val();

            ram['loadid'] = loadid;
            ram['parameter'] = "RAM";
            ram['uom'] = "MB";
            ram['value'] = $("#ram").val();

            disk['loadid'] = loadid;
            disk['parameter'] = "Disk";
            disk['uom'] = "GB";
            disk['value'] = $("#disk").val();

           // console.log("CPU Load Value value: " + cpu.value + " core");
           // console.log("RAM Load Value value: " + ram.value + " MB");
           // console.log("Disk Load Value value: " + disk.value + " GB");


            loadValuesList.push(cpu);
            loadValuesList.push(ram);
            loadValuesList.push(disk);
            //LOAD_ID = loadid;

            var postLoadValuesUrl = registeredMarket.url + LOAD_VALUE_URL;
           // console.log("LOAD VALUES POST URL: " + postLoadValuesUrl )

             $.ajax({
                  url:postLoadValuesUrl,
                  type: method,
                    beforeSend: function (xhr) {
                      xhr.setRequestHeader("Authorization", registeredMarket.token)
                    },
                  dataType:"json",
                  contentType: "application/json; charset=utf-8",
                  data:JSON.stringify(loadValuesList),

                  success: function(data){
                       var u1 = registeredMarket.url +MPLACE_URL + MPLACE + "/marketsessions/" + sessions.id +"/actions/";
                       intra_modal_actuate(u1, "POST", null, loadid);
                      console.log("SUCCESFULLY CREATED LOAD VALUES: CPU, RAM, Disk");
                  },
                  error: function(){
                    console.log("ERROR CREATING LOAD VALUES: CPU, RAM, Disk");
                  }
            });

        }




}



function actuateLoad(method, date){
    var load = {};


    var url = registeredMarket.url + LOAD_URL;

    //console.log("Load URL: " + registeredMarket.url + LOAD_URL);


    if(method == "POST") {


        load['date'] = date;

        console.log("Posting new Load at url: " + url + " with payload: " +  load.date);

        $.ajax({
          url:url,
          type: method,
            beforeSend: function (xhr) {
              xhr.setRequestHeader("Authorization", registeredMarket.token)
            },
          dataType:"json",
          contentType: "application/json; charset=utf-8",
          data:JSON.stringify(load),

          success: function(data){
              load['id'] = data.id;
              load['date'] = data.date;
              console.log("Posted Load:" +  load['id'] + "---" + load['date']);
              actuateLoadValue("POST",  data.id);
          },
          error: function(){
             console.log("ERROR posting new load")
            customRefresher();
          }
        });
    }

}

