﻿function ajax_call_wo_data(url, method, callback_success, callback_failure) {
	$.ajax({
		url: url,
		type: method,
		headers: {"accept": "application/json"},
		success: callback_success,
		error: callback_failure
	});
}

function ajax_call_with_data(url, method, callback_success, callback_failure, data) {
	$.ajax({
		url: url,
		type: method,
		contentType: "application/json",
		success: callback_success,
		error: callback_failure,
		data: JSON.stringify(data)
	});
}

function parse_measurements(data) {
	var measurements = [];
	for (var index in data) {
		var datum = data[index];
        var date = moment(datum.Date + "T00:00:00+01:00", "YYYY-MM-DDTHH:mm:ssZ");
		for (var i = 0; i < datum.Measurements.length; i++) {
			measurements.push([date.utc().valueOf(), Number(datum.Measurements[i])]);
            date = date.add(15, 'minutes');
		}
	}
	return measurements;
}


function plotline(index, _chart, _chart_data, _title, _value, _series, _color) {
    charts[index] = new Highcharts.Chart({
        chart: {
            renderTo: _chart,
            type: 'line'
        },
        title: {
            text: _title,
            style: {
                color: '#2EAADD',
                fontWeight: 'bold'
            }
        },
        rangeSelector: {
            enabled: true
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: _value
            }
        },
        series: [{
            name: _series,
            data: _chart_data,
            color: _color,
            type: 'spline',
            marker: { enabled: false }
        }],
        credits: {
            enabled: false
        }
    });
}

 function getSessionForm (url, mid, sid) {
    var sessionForm = 0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function( data ) {
        sessionForm = data.formid.id;
      },
      error: function(){
        sessionForm = 0;
      },
      async: false
    });
    return sessionForm;
 }

 function getSessionFormForm (url, mid, sid) {
    var sessionForm = 0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function( data ) {
        sessionForm = data.formid.form;
      },
      error: function(){
        sessionForm = 0;
      },
      async: false
    });
    return sessionForm;
 }

function getActionStatus (url, status) {
    var status_id = 0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function( data ) {
        $.each(data, function () {
            if (this.status === status) {
                status_id = this.id;
                return false;
            }
        });
      },
      async: false
    });
    return status_id;
 }

 function getActionType (url, type) {
    var action_type = 0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function ( data ) {
      $.each( data, function () {
            if (this.type === type) {
                action_type = this.id;
                return false;
            }
        });
      },
      async: false
    });
    return action_type;
}

function getSessionStatus (url, status) {
    var status_id = 0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function ( data ) {
      $.each( data, function () {
            if (this.status === status) {
                status_id = this.id;
                return false;
            }
        });
      },
      async: false
    });
    return status_id;
}

var ActionStatus = ActionStatus || (function () {
    return {
        formatter: function (id) {
            var status = "";
            $.ajax({
              url:  registeredMarket.url + ACTION_STATUS_URL,
              type:"GET",
              beforeSend: function (xhr) {
                  xhr.setRequestHeader("Authorization", token)
              },
              success:function( data ) {
                $.each(data, function () {
                    if (this.id === id) {
                        status = this.status;
                        return false;
                    }
                });
              },
              async: false
            });
            switch (status) {
                case "unchecked":
                    return "<span class=\"label label-default\">Unchecked</span>";
                    break;
                case "valid":
                    return "<span class=\"label label-info\">Valid</span>";
                    break;
                case "invalid":
                    return "<span class=\"label label-danger\">Invalid</span>";
                    break;
                case "active":
                    return "<span class=\"label label-info\">Active</span>";
                    break;
                case "inactive":
                    return "<span class=\"label label-warning\">Inactive</span>";
                    break;
                case "withdrawn":
                    return "<span class=\"label label-default\">Withdrawn</span>";
                    break;
                case "accepted":
                    return "<span class=\"label label-success\"></i> Accepted</span>";
                    break;
                case "partially_accepted":
                    return "<span class=\"label label-warning\">Partially Accepted</span>";
                    break;
                case "rejected":
                    return "<span class=\"label label-danger\">Rejected</span>";
                    break;
                case "selected":
                    return "<span class=\"label label-info\">Selected</span>";
                    break;
                case "delivered":
                    return "<span class=\"label label-primary\">Delivered</span>";
                    break;
                case "not_delivered":
                    return "<span class=\"label label-warning\">Not delivered</span>";
                    break;
                case "billed":
                    return "<span class=\"label label-info\">Billed</span>";
                    break;
                default:
                    return "<span class=\"label label-danger\">Invalid</span>";
            }
        },
    };

})();


var ActionStatusCheckStatus = ActionStatusCheckStatus || (function () {
    return {
        formatter: function (status) {
            switch (status) {
                case "unchecked":
                    return "<span class=\"label label-default\">Unchecked</span>";
                    break;
                case "valid":
                    return "<span class=\"label label-info\">Valid</span>";
                    break;
                case "invalid":
                    return "<span class=\"label label-danger\">Invalid</span>";
                    break;
                case "active":
                    return "<span class=\"label label-info\">Active</span>";
                    break;
                case "inactive":
                    return "<span class=\"label label-warning\">Inactive</span>";
                    break;
                case "withdrawn":
                    return "<span class=\"label label-default\">Withdrawn</span>";
                    break;
                case "accepted":
                    return "<span class=\"label label-success\"></i> Accepted</span>";
                    break;
                case "partially_accepted":
                    return "<span class=\"label label-warning\">Partially Accepted</span>";
                    break;
                case "rejected":
                    return "<span class=\"label label-danger\">Rejected</span>";
                    break;
                case "selected":
                    return "<span class=\"label label-info\">Selected</span>";
                    break;
                case "delivered":
                    return "<span class=\"label label-primary\">Delivered</span>";
                    break;
                case "not_delivered":
                    return "<span class=\"label label-warning\">Not delivered</span>";
                    break;
                case "billed":
                    return "<span class=\"label label-info\">Billed</span>";
                    break;
                default:
                    return "<span class=\"label label-danger\">Invalid</span>";
            }
        },
    };

})();

var ActionValidator = ActionValidator || (function () {

    return {
        datetime: function (elem) {
            if (elem.val() === "" || elem.val() === null || elem.val() === undefined) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        value: function (elem) {
            if ((elem.val()).length == 0 || elem.val() <0 || isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        price: function (elem) {
            console.log("Number validator: ");
            console.log(elem.val());
            if ( (elem.val()).length == 0 || elem.val() <0 || isNaN(elem.val()) ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        cpu: function (elem) {
            if ( (elem.val()).length == 0 || elem.val() <0 || isNaN(elem.val()) ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        ram: function (elem) {
            if ( (elem.val()).length == 0 || elem.val() <0 || isNaN(elem.val()) ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        disk: function (elem) {
            if ( (elem.val()).length == 0 || elem.val() <0 || isNaN(elem.val()) ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },

        uom: function (elem) {
            if (elem.val() === "" || elem.val() === null || !isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        deliveryPoint: function (elem) {
            if ( elem.val() === "" || elem.val() === null ) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        type: function (elem) {
            if (elem.val() === "" || elem.val() === null || !isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        },
        service: function (elem) {
            if ((elem.val()).length == 0 || elem.val() < 0 || isNaN(elem.val())) {
                elem.parent().addClass("has-error");
                return false;
            }
            elem.parent().removeClass("has-error");
            return true;
        }
    };

})();

function setActionStatusFormatter(value, row, index) {
    return ActionStatus.formatter(value);
}

function setActionStatusSimpleFormatter(value, row, index) {
    return ActionStatusCheckStatus.formatter(value);
}


function setMarketActorFormatter(value, row, index){
    return [
        '<a href="#" data-pk="' + value + '" class="view-market-actor" style="color:dodgerblue">',
        value,
        '</a>'
    ].join('');
}

function typeFormatter(value, row, index) {
    if (value == "bid") {
        return "<div style=\"color:red;\">Bid</div>";
    } else {
        return "<div style=\"color:green;\">Offer</div>";
    }
}

function europeanNumberFormatter(value, row, index){
     return value.replace('.', ',');
}

function convertFloatToEuropeanNotation(number, decimalDigits){
    return number.toFixed(decimalDigits).toString().replace('.', ',');
}

function setMarketSessionFormatter(value, row, index){
    return [
        '<a href="#" data-pk="' + value + '" class="view-market-session" style="color:dodgerblue">',
        value,
        '</a>'
    ].join('');
}

function getMarketplace (url) {
    var mplace;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function ( data ) {
        mplace = data;
      },
      async: false
    });
    return mplace;
}

function getFormByValue (url, form) {
    var formobject=0;
    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function( data ) {
        $.each(data, function () {
            if (this.form === form) {
                formobject = this;
                return false;
            }
        });
      },
      error: function(){
        sessionForm = 0;
      },
      async: false
    });
    return formobject;
}

function getSessionStatusObject (url, status) {
    var status_object = 0;

    $.ajax({
      url: url,
      type:"GET",
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", token)
      },
      success:function ( data ) {
      $.each( data, function () {
            if (this.status === status) {
                status_object = this;
                return false;
            }
        });
      },
      async: false
    });
    return status_object;
}

function convertStringToValidJSONObject(djangoJsonString){

    djangoJsonString = djangoJsonString.replace(/&(l|g|quo)t;/g, function(a,b){
            return {
                l   : '<',
                g   : '>',
                quo : '"'
            }[b];
        });
    djangoJsonString = djangoJsonString.replace(/&#39;/g, '"');

    djangoJsonString = djangoJsonString.replace(/u'/g, '\'');
    djangoJsonString = djangoJsonString.replace(/'/g, '\"');
    djangoJsonString = djangoJsonString.replace(/&#39;/g, '"');

    return JSON.parse(djangoJsonString);


}

function getUserRegisteredMarketByForm(userMarkets, form){

    var userMarketOfForm = null;


    for(var i in userMarkets){

        if(userMarkets[i].form === form){

            userMarketOfForm = userMarkets[i];
        }

    }

    if(userMarketOfForm === null){
        swal({
                       title: "Error",
                       text: 'You are not a participant in any market of this type! \n You will be redirected to home',
                       type: "warning",
                       confirmButtonText: "OK",
                       confirmButtonColor: "#009245",
                   },
                     function (isConfirm) {
                        if(isConfirm){
                          location.href = "/";
                        }
                     });
    }

    return userMarketOfForm;

}

function displayInfo(title, message, type, confirmButtonText, redirectLocation){


       swal({
                       title: title,
                       text: message,
                       type: type,
                       confirmButtonText: confirmButtonText,
                       confirmButtonColor: "#009245",
                   },
                     function (isConfirm) {
                        if(isConfirm && redirectLocation !== null){
                          location.href = redirectLocation;
                        }
                     });

}


function getActorId(registeredMarketUrl, username_to_actor, token, username){
    res = -1;

    console.log("User to actor URL: " + registeredMarketUrl + username_to_actor + username + "/")
     $.ajax({
        type: "GET",
        url: registeredMarketUrl + username_to_actor + username + "/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token)
        },
        datatype: "json",
        contenttype: 'application/json',
        async: false,
        success: function (response) {
                res = response.id;
        },
         error: function () {
            console.log("Couldn't get actor id by username");
         }
    });
    return res;
}