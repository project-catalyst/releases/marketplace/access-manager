$( document ).ready(function() {
        $('#actionStartTime').datetimepicker();
        $('#actionStartTime').data("DateTimePicker").format('DD-MM-YYYY HH:mm');
        //$('#actionStartTime').data("DateTimePicker").minDate(moment().add(1, 'days'));
        $('#actionStartTime').data("DateTimePicker").minDate(moment());
        $('#actionStartTime').data("DateTimePicker").maxDate(moment().add(32, 'hours'));
        $('#actionEndTime').datetimepicker();
        $('#actionEndTime').data("DateTimePicker").format('DD-MM-YYYY HH:mm');
        //  $('#actionEndTime').data("DateTimePicker").minDate(moment().add(25, 'hours'));
        $('#actionEndTime').data("DateTimePicker").minDate(moment().add(1, 'hours'));
        $('#actionEndTime').data("DateTimePicker").maxDate(moment().add(33, 'hours'));
        //$("#actionStartTime").on("dp.change", function (e) {
        //    $('#actionEndTime').data("DateTimePicker").minDate(e.date);
        //});
        $("#actionEndTime").on("dp.show", function (e) {
            $('#actionEndTime').data("DateTimePicker").date(moment($('#actionStartTime').data("DateTimePicker").date()).add(1, 'hours'));
        });
        //$("#actionEndTime").on("dp.change", function (e) {
        //    $('#actionStartTime').data("DateTimePicker").maxDate(e.date);
        //});

        $('#AddActionForm').submit(function () {
            var $inputs = $('#AddActionForm input');
            var values = {};
            $inputs.each(function () {
                if (this.id === "type") {
                    if ($(this).val() === "offer") {
                        values["actionTypeid"] = 2;
                    } else if ($(this).val() === "bid") {
                        values["actionTypeid"] = 1;
                    }
                } else {
                    values[this.id] = $(this).val();
                }

                $(this).val('');
                $('#uom').val($('#uoml').val());
                $('#type').val($('#typel').val());
            }
            );

            values['date'] = moment().format();
            values['actionStartTime'] = moment(values['actionStartTime'], 'DD-MM-YYYY HH:mm').format();
            values['actionEndTime'] = moment(values['actionEndTime'], 'DD-MM-YYYY HH:mm').format();
            values['marketSessionid'] = current.id;
            values['marketActorid'] = 2;
            values['formid'] = 1;
            values['actionStatusid'] = 1;
            alert(values);
            // add_action(values);
            $('#actionStartTime').data("DateTimePicker").minDate(moment());
            $('#actionStartTime').data("DateTimePicker").maxDate(moment().add(32, 'hours'));
            $('#actionEndTime').data("DateTimePicker").minDate(moment().add(1, 'hours'));
            $('#actionEndTime').data("DateTimePicker").maxDate(moment().add(33, 'hours'));
        });
    });