from urllib.parse import urljoin

from django.conf import settings



def ib_endpoints(request):
    return {
        "FORM_DETAILS": "/form/details/",
        "RELATIONS": "/relations/",
        "DSO_LIST_URL": "/dso/",
        "FORMS": "/form/",
        "ALLSESSION": "/allSession/",
        "TIMEFRAME_LIST": "/timeframe/",
        "PENDING_REQUESTS":  "/pending/requests/",
        "RULES_URL": "/rules/",
        "SELECT_MPLACE": "/select/marketplace/",
        "MPLACE_URL": "/marketplace/",
        "MARKETSESSION_URL": "/marketsessions/",
        "MARKETSESSION_BRIEF_URL": "/marketsessions/brief/",
        "LOAD_VALUE_URL": "/loadvalues/",
        "LOAD_URL": "/load/",
        "USER_STATUS": "/marketplaces/admin/participant/status/",
        "ENERGY_HISTORY_URL": "/history/energy-history/",
        "HEAT_HISTORY_URL": "/history/heat-history/",
        "IT_LOAD_HISTORY_URL": "/history/it_load-history/",
        "GET_PROFILE_URL": "/getProfile/",
        "UPDATE_PROFILE_URL": "/update/",
        "CREATE_KEYCLOAK_ACCOUNT": "/create-keycloak-account/",
        "ASSIGN_ACTORS_URL": "/assign/actor/",
        "ACTION_URL": "/action/",
        "MARKET_ACTOR_URL": "/marketactor/",
        "ACTION_STATUS_URL": "/actionstatus/",
        "ACTION_TYPE_URL": "/actiontypes/",
        "SESSION_STATUS_URL": "/sessionstatus/",
        "ACTOR_URL": "/marketactor/",
        "USER_INVOICES": "/invoices/user-invoices/details/",
        "USER_HEAT_INVOICES": "/invoices/heat-invoices/details/",
        "USER_IT_LOAD_INVOICES": "/invoices/it_load-invoices/details/",
        "BALANCING_INVOICES": "/invoices/balancing-invoices/details/",
        "ANCILLARY_HISTORY": "/history/ancillary-history/",
        "ACTOR_BY_USERNAME": "/actor-by-username/",
        "HEAT": "thermal_energy",
        "ENERGY": "energy",
        "IT_LOAD": "it_load",
        "THERMAL": "thermal_energy",
        "ELECTRIC": "electric_energy",
        "IT_LOAD_FORM": "it_load",
        "FLEXIBILITY": "ancillary-services",
        "MEMO": settings.MEMO_URL,
        "USERNAME_TO_ACTOR": "/actor-by-username/",
    }
