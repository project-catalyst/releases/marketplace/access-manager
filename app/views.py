import json
from datetime import datetime
from urllib.parse import urljoin
from django.core.serializers.json import DjangoJSONEncoder

from app.serializers import  UserMarketSerializer

import requests, json
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django.forms import forms
from django.http import HttpRequest, HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.conf import settings

from django.urls import reverse, reverse_lazy
from django.utils.http import is_safe_url
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView

from app.decorators import login_required_apiview
from app.forms import BootstrapAuthenticationForm

import logging

log = logging.getLogger(__name__)


class Struct(dict):
    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        del self[name]


def getDomain(request):
    return request.scheme + "://" + request.META['HTTP_HOST']


def signupForm(request):
    """Load user registration form"""
    # try:
    assert isinstance(request, HttpRequest)
    from random import randint
    question = str(randint(0, 6)) + "+" + str(randint(0, 4))

    url = settings.MEMO_URL + 'marketplaces/IB/'
    try:
        log.info("Getting list of IBs from {}".format(url))
        r = requests.get(url)
        r.raise_for_status()
        available_markets = r.json()
    except Exception as e:
        log.exception(e)
        available_markets = []

    url = settings.MEMO_URL + 'marketplaces/IB/'
    r = requests.get(url)

    log.warning("available markets")
    log.warning(available_markets)
    if r.status_code == 200:
        for market in available_markets:
            if market['type'] == 'ancillary_services' or market['form'] == 'ancillary_services':
                market['type'] = 'ancillary-services'
                market['form'] = 'ancillary-services'
        request.session["available_markets"] = available_markets
        # todo: Replace ancillary_services with ancillary-services here
    context = {
        'title': 'CATALYST Access Manager',
        'year': datetime.now().year,
        'available_markets': json.dumps(request.session.get("available_markets", None)),
        'question': question
    }
    return render(request, 'app/signup.html', context)
    # except:
    #     pass


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    actor = request.session.get("actor", None)
    context = {
        'title': 'CATALYST Access Manager',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
        'actor': json.dumps(request.session.get("actor", None))
    }
    return render(request, 'app/index.html', context)


@login_required_apiview
def marketplaces(request):
    assert isinstance(request, HttpRequest)
    context = {
        'title': 'Energy',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
        'actor': json.dumps(request.session.get("actor", None))
    }

    print(request.session["user_markets"])
    return render(request, 'app/marketplaces.html', context)


@login_required_apiview
def heat_marketplace(request):
    assert isinstance(request, HttpRequest)
    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Heat',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/heat_marketplace.html', context)


@login_required_apiview
def it_load_marketplace(request):
    assert isinstance(request, HttpRequest)
    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'IT Load',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/it_load_marketplace.html', context)


@login_required_apiview
def day_ahead(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Energy Day ahead',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
        'year': datetime.now().year
    }
    return render(request, 'app/dayAhead.html', context)


@login_required_apiview
def heat_day_ahead(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Heat Day ahead',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
        'year': datetime.now().year
    }
    return render(request, 'app/heatDayAhead.html', context)


@login_required_apiview
def it_load_day_ahead(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]
    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'IT Load Day ahead',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
        'year': datetime.now().year
    }
    return render(request, 'app/itLoadDayAhead.html', context)


@login_required_apiview
def intra_day(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {

        'session': [],
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Energy intra day',
        'username': request.session["username"],
        'is_dso': request.session['is_dso'],
        'token': token,
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/intraDay.html', context)


@login_required_apiview
def heat_intra_day(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'session': [],
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Heat Intra Day',
        'username': request.session["username"],
        'is_dso': request.session['is_dso'],
        'token': token,
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/heatIntraDay.html', context)


@login_required_apiview
def it_load_intra_day(request):
    """Presents the available actions"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'IT Load Intra day',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/itLoadIntraDay.html', context)

@login_required_apiview
def correlations(request):
    assert isinstance(request, HttpRequest)

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Correlations',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/correlations.html', context)

@login_required_apiview
def update_correlations(request):
    assert isinstance(request, HttpRequest)

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Correlations',
        'year': datetime.now().year,
        'user_markets':json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/updateCorrelations.html', context)

@login_required_apiview
def correlations_intra_day(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Correlations dayahead',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/correlationsIntraday.html', context)

@login_required_apiview
def correlations_day_ahead(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Correlations dayahead',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/correlationsDayahead.html', context)

@login_required_apiview
def update_correlations_intra_day(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Correlations dayahead',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/updateCorrelationsIntraday.html', context)

@login_required_apiview
def update_correlations_day_ahead(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'Correlations dayahead',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/updateCorrelationsDayahead.html', context)

@login_required_apiview
def history_correlations(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'History Correlations',
        'year': datetime.now().year,
        'user_markets':  json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/correlationsHistory.html', context)

@login_required_apiview
def history_correlations_intra_day(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'History Correlations',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/correlationsHistoryIntraday.html', context)

@login_required_apiview
def history_correlations_day_ahead(request):
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'token': token,
        'actor': json.dumps(request.session.get("actor", None)),
        'is_dso': request.session['is_dso'],
        'username': request.session["username"],
        'title': 'History Correlations dayahead',
        'year': datetime.now().year,
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    print("in view")
    print(context)
    return render(request, 'app/correlationsHistoryDayahead.html', context)

@login_required_apiview
def history(request):
    try:
        assert isinstance(request, HttpRequest)

        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'History',
            'year': datetime.now().year,
            'user_markets': json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/history.html', context)
    except:
        if settings.DEBUG:
            from traceback import print_exc
            print_exc()


@login_required_apiview
def energyHistory(request):
    try:
        assert isinstance(request, HttpRequest)
        token = request.session["token"]

        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'History',
            'username': request.session["username"],
            'token': token,
            'year': datetime.now().year,
            'user_markets': json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/energyHistory.html', context)
    except:
        if settings.DEBUG:
            from traceback import print_exc
            print_exc()
        return HttpResponse(status=400)


@login_required_apiview
def heatHistory(request):
    try:
        assert isinstance(request, HttpRequest)
        token = request.session["token"]

        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'History',
            'username': request.session["username"],
            'token': token,
            'year': datetime.now().year,
            'user_markets': json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/heatHistory.html', context)
    except:
        if settings.DEBUG:
            from traceback import print_exc
            print_exc()
        return HttpResponse(status=400)


@login_required_apiview
def itLoadHistory(request):
    try:
        assert isinstance(request, HttpRequest)
        token = request.session["token"]

        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'ITLoad',
            'username': request.session["username"],
            'token': token,
            'year': datetime.now().year,
            'user_markets': json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/itLoadHistory.html', context)
    except:
        if settings.DEBUG:
            from traceback import print_exc
            print_exc()
        return HttpResponse(status=400)


@login_required_apiview
def invoice_history(request):
    """Displays the invoices of a user"""
    assert isinstance(request, HttpRequest)

    context2 = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Invoices',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }

    log.warning("Invoices context: {}".format(context2))
    return render(request, 'app/invoices.html', context2)


@login_required_apiview
def energyInvoice(request):
    """Displays the GEM invoices"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Energy Invoices',
        'actor_id': request.session['actor_id'],
        'username': request.session["username"],
        'token': token,
        'form': 'electric_energy',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/energyInvoices.html', context)


@login_required_apiview
def heatInvoice(request):
    """Displays the GEM invoices"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Energy Invoices',
        'actor_id': request.session['actor_id'],
        'username': request.session["username"],
        'token': token,
        'form': 'thermal_energy',
        'user_markets': json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/heatInvoices.html', context)


@login_required_apiview
def itLoadInvoice(request):
    """Displays the GEM invoices"""
    assert isinstance(request, HttpRequest)
    token = request.session["token"]

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'IT Load Invoices',
        'actor_id': request.session['actor_id'],
        'username': request.session["username"],
        'token': token,
        'form': "it_load",
        'user_markets': json.dumps(request.session.get("user_markets", None))
    }
    return render(request, 'app/itLoadInvoices.html', context)


@login_required_apiview
def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    token = request.session.get("token", None)

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'About',
        'message': 'About the CATALYST Access Manager',
        'token': token,
        'year': datetime.now().year,
        'user_markets':json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/about.html', context)


def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Contact us',
        'message': 'Please, use the following links to get support and more information',
        'year': datetime.now().year,
        'user_markets':json.dumps(request.session.get("user_markets", None)),
    }
    return render(request, 'app/contact.html', context)


@login_required_apiview
def smart_grid(request):
    if request.method == "GET":
        # try:
        assert isinstance(request, HttpRequest)

        # actor = None
        # rel = UserActorRel.objects.filter(user__username=request.user.username, status=1)
        # if len(rel) == 1:
        #     actor = MarketActor.objects.get(id=rel[0].actor.id)
        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'url_consumption': reverse('sgi_consumption'),
            'url_generation': reverse('sgi_generation'),
            'title': 'Smart grid',
            'year': datetime.now().year,
            'user_markets':json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/smart_grid.html', context)
        # except:
        #     if settings.DEBUG:
        #         from traceback import print_exc
        #         print_exc()


@login_required_apiview
def admin(request):
    """Displays the user <-> market actor relationships"""
    # users_actors = UserActorRel.objects.all()
    token = request.session["token"]

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'username': request.session["username"],
        'title': 'Admin',
        'year': datetime.now().year,
        'token': token,
        'memo': settings.MEMO_URL,
        'user_markets': json.dumps(request.session.get("user_markets", None))
    }
    return render(request, 'app/admin.html', context)


@csrf_exempt
def signup(request):
    """ Register new user """

    try:
        assert isinstance(request, HttpRequest)
        payload = json.loads(request.body)

        url = settings.MEMO_URL + 'marketplaces/IB/'
        try:
            log.info("Getting list of IBs from {}".format(url))
            r = requests.get(url)
            r.raise_for_status()
            available_markets = r.json()
        except Exception as e:
            log.exception(e)
            available_markets = []

        for market in available_markets:
            if market['type'] == 'ancillary_services' or market['form'] == 'ancillary_services':
                market['type'] = 'ancillary-services'
                market['form'] = 'ancillary-services'

        selected_markets = payload["selected_markets"]
        log.warning("Creating new account...")
        log.warning("Markets selected by user: {}".format(selected_markets))
        call_markets_ids = []

        if len(available_markets) == 0:
            log.warning("MEMO did not provide any available ib deployments to perform the signup")

        information_broker = available_markets[0]

        del payload['selected_markets']
        log.warning('Creating keycloak user at url: {}'.format(information_broker['url'] + '/create-keycloak-user/'))
        log.warning('Selected markets {}'.format(selected_markets))
        log.warning("Payload: {}".format(payload))
        create_keycloak_user_request = requests.post(information_broker['url'] + '/create-keycloak-user/', data=payload)

        log.warning('Status of keycloak user creation request: {}'.format(create_keycloak_user_request.status_code))

        if create_keycloak_user_request.status_code == 200:
            called_markets = []
            success_called_markets = True
            for market in selected_markets:
                for available in available_markets:
                    if market == available["form"]:
                        response_actor = requests.post(available['url'] + '/create-actor-assign-client-roles/', data=payload)
                        log.warning('Status of ' + available["type"] +' actor creation request: {}'.format(response_actor.status_code))
                        called_markets.append(available)
                        call_markets_ids.append(available["id"])
                        if response_actor.status_code != 200:
                            success_called_markets = False
                        break
                if not success_called_markets:
                    break
            if not success_called_markets:
                log.warning('There was an error creating the user. Begin cleaning the db')
                request.delete(information_broker['url'] + '/delete-keycloak-user/' + payload['username'] + "/")
                for market in called_markets:
                    log.warning('Clean data for {}', market["type"])
                    request.delete(market['url'] + '/delete-actor-and-user/' + payload['username'] + "/")


            payload["marketRegistered"] = call_markets_ids
            payload["status"] = "valid"
            log.warning("Payload: {}".format(payload))

            memo_user_registration_notification = {}
            memo_user_registration_notification["firstName"] = payload["first_name"]
            memo_user_registration_notification["lastName"] = payload["last_name"]
            memo_user_registration_notification["username"] = payload["username"]
            memo_user_registration_notification["email"] = payload["email"]
            memo_user_registration_notification["phone"] = payload["phone"]
            memo_user_registration_notification["vat"] = payload["vat"]
            memo_user_registration_notification["companyName"] = payload["company_name"]
            memo_user_registration_notification["actorTypeId"] = int(payload["market_actor_id"])
            memo_user_registration_notification["marketRegistered"] = payload["marketRegistered"]
            memo_user_registration_notification["status"] = payload["status"]


            log.warning("MEMO notification for new user registration {}".format(memo_user_registration_notification))
            notify_memo_request = requests.post(settings.MEMO_URL + 'marketparticipant/request/', json=memo_user_registration_notification)
            log.warning('Status of memo notification request: {}'.format(notify_memo_request.status_code))

        else:
            log.warning("Error creating new account, a user already exists with that username")
            return JsonResponse({"state": False, "redirect": reverse('signup')})

        domain = getDomain(request) + settings.RELATIVE_URL

        # Market user notification
        subject = "[CATALYST] Marketplace registration"
        body = "<div>Dear " + str(payload['first_name']) + " " + str(payload['last_name']) + ",<br><br>"
        body += "Your registration was successfully completed in the CATALYST Marketplace.<br>"
        body += "Your username is: <strong>" + payload['username'] + "</strong>.<br><br>"
        body += "Visit us on the <a href='" + str(domain) + "'>CATALYST Marketplace</a>.<br><br>"
        body += "Sincerely,<br>The administrator team<br><br>"
        body += "---<br>"
        body += "We don't check this mailbox, so please don't reply to this message.<br>"
        body += "<a href='" + str(domain) + "'>CATALYST Marketplace</a></div>"
        # sendEmail(payload['email'], subject, body, True)

        # Market Operator notification
        # operatorType = MarketActorType.objects.get(pk=3)
        # operator = MarketActor.objects.filter(MarketActorType_id=operatorType.id).first()

        # todo: take the operator from the market in which the user has registered
        # todo: and notify him with an email


        subject = "[CATALYST] A Marketplace registration request is pending"
        # body = "<div>Dear " + str(operator.representativeName) + ",<br><br>"
        body += "A new user called " + str(payload['first_name']) + " " + str(payload['last_name']) + "(" + str(
            payload['username']) + ") has been registered in the CATALYST Marketplace.<br>"
        body += "Please grant him (her) with permission to access it.<br><br>"
        body += "Sincerely,<br>The administrator team</div>"
        # sendEmail([operator.email], subject, body, True)

        # server response in success
        # return JsonResponse({"state": True, "redirect": domain+"login/"})
        return JsonResponse({"state": True, "redirect": reverse('login')})

    except:
        if settings.DEBUG:
            from traceback import print_exc
            print_exc()
        return JsonResponse({"state": False})


@login_required_apiview
def profile(request):
    """Load user profile"""
    try:
        if hasattr(request, 'session'):
            assert isinstance(request, HttpRequest)
        token = request.session["token"]

        context = {
            'actor': json.dumps(request.session.get("actor", None)),
            'title': 'My profile',
            'year': datetime.now().year,
            'username': request.session["username"],
            'token': token,
            'user_markets':json.dumps(request.session.get("user_markets", None)),
        }
        return render(request, 'app/profile.html', context)
    except:
        from traceback import print_exc
        print_exc()


@login_required_apiview
def today_markets(request):
    """Presents the available marketplaces"""
    assert isinstance(request, HttpRequest)

    context = {
        'actor': json.dumps(request.session.get("actor", None)),
        'title': 'Welcome',
        'message': 'Please, select one of the following marketplace types to continue.',
        'year': datetime.now().year,
        'user_markets':json.dumps(request.session.get("user_markets", None)),
    }

    return render(request, 'app/today.html', context)


def login2(request):
    """Presents the available marketplaces"""
    if request.method == "POST":
        form = BootstrapAuthenticationForm(request.POST)
        username = dict(request._post)['username'][0]
        pwd = dict(request._post)['password'][0]
        data = {"username": username, "password": pwd}

        user_markets_url = settings.MEMO_URL + 'userMarkets/IB/' + username + '/'
        available_markets_url = settings.MEMO_URL + 'marketplaces/IB/'

        log.warning("Retrieving user markets for username: {} at URL: {}".format(username, user_markets_url))
        um_request = requests.get(user_markets_url)
        am_request = requests.get(available_markets_url)

        log.warning("Status code for retrieving user markets for username: {} at URL: {} is {}".format(username, user_markets_url, um_request.status_code))

        # Treat cases in which operator is created before MEMO, and MEMO does not know of the markets
        # in which the operator is registered. Or, cases in which MEMO is unable to provide user markets
        if um_request.status_code == 200:
            try:
                user_markets = um_request.json()
                if len(user_markets) == 0:
                    log.warning("No user markets objects returned for user: {}. Using available markets instead".format(username))
                    user_markets = am_request.json()
            except Exception as e:
                log.warning("Unable to parse JSON for user markets. Using available markets instead")
                user_markets = am_request.json()
                log.warning(e)
        else:
            log.warning("User markets status code: {}. Using available markets instead".format(um_request.status_code))
            user_markets = am_request.json()

        for market in user_markets:
            if market['type'] == 'ancillary_services' or market['form'] == 'ancillary_services':
                market['type'] = 'ancillary-services'
                market['form'] = 'ancillary-services'

        information_broker = user_markets[0]

        log.warning("Authenticating with username: {}, at URL: {}".format(username, information_broker['url'] + '/tokens/'))
        r = requests.post(information_broker['url'] + '/tokens/', json=data)
        log.warning("Status code of authentication request for username : {} at URL: {} is {}".format(username, information_broker['url'] + '/tokens/', r.status_code))

        if form.is_valid() and r.status_code == 200:
            request.session["username"] = data["username"]
            request.session["is_authenticated"] = 1
            request.session["token"] = "Token " + r.json()["token"]
            request.session["actor_id"] = r.json()["actor"]["id"]
            request.session["actor"] = r.json()["actor"]


            print(r.json()["actor"])

            if r.json()["actor"]["marketActorTypeid"]["type"] == "dso":
                request.session["is_dso"] = 1
            else:
                request.session["is_dso"] = 0


            if (um_request.status_code == 200) or (user_markets is not None):

                user_market_serializer = UserMarketSerializer(data=user_markets, many=True)
                log.warning("Received json for user markets of {} : {}".format(username, user_markets))
                if user_market_serializer.is_valid():
                    user_markets_with_token = user_market_serializer.data
                    # TODO: replace ancillary_services with ancillary-services here
                    for market in user_markets_with_token:
                        r = requests.post(market['url'] + '/tokens/', json=data)
                        log.warning("Retrieve token from market {} status {}".format(market['type'], r.status_code))
                        if r.status_code == 200:
                            market['token'] = "Token " + r.json()["token"]
                    log.warning("User markets with token {}".format(user_markets_with_token))
                    user_market_objects = json.dumps(list(user_markets_with_token), cls=DjangoJSONEncoder)
                else:
                    user_market_objects = None
                    log.warning("Serializer errors: {}".format(user_market_serializer.errors))

                request.session["user_markets"] = user_market_objects
                log.warning("User markets retrieved for username: {} are {}".format(username, user_market_objects))

            return HttpResponseRedirect(reverse_lazy('home'))
        else:
            form = BootstrapAuthenticationForm
            errors = "invalid user"
            return render(request, 'app/login.html', {'form': form, 'errors': errors})
    else:
        form = BootstrapAuthenticationForm
        return render(request, 'app/login.html', {'form': form})


class LoginView(TemplateView, FormView):
    """
    Provides the ability to login as a user with a username and password
    oauth2 functionality after login (Oauth2Manager)
    """
    form_class = BootstrapAuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'app/login.html'
    success_url = reverse_lazy('marketplaces')

    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """Login user and save token to session"""
        # auth_login(self.request, form.get_user())
        username = dict(self.request._post)['username'][0]
        pwd = dict(self.request._post)['password'][0]
        data = {"username": username, "password": pwd}
        r = requests.post(settings.IB_URL + '/tokens/', data=data)
        print(r.json())
        if r.status_code == 200:
            self.request.session["token"] = "Token " + r.json()["token"]
            self.request.session["actor_id"] = r.json()["actor"]["id"]
            self.request.session["actor"] = r.json()["actor"]
            if r.json()["actor"]["marketActorTypeid"]["type"] == "dso":
                self.request.session["is_dso"] = 1
            else:
                self.request.session["is_dso"] = 0
        else:
            form["errors"] = "invalid user"
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to
