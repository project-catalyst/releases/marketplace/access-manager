from django.conf.urls import url

from app import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^marketplaces/energy/$', views.marketplaces, name='marketplaces'),
    url(r'^marketplaces/energy/day_ahead/$', views.day_ahead, name='day_ahead'),
    url(r'^marketplaces/energy/intra_day/$', views.intra_day, name='intra_day'),
    url(r'^marketplaces/heat/$', views.heat_marketplace, name='heat_marketplace'),
    url(r'^marketplaces/heat/day_ahead/$', views.heat_day_ahead, name='heat_day_ahead'),
    url(r'^marketplaces/heat/intra_day/$', views.heat_intra_day, name='heat_intra_day'),
    url(r'^marketplaces/it_load/$', views.it_load_marketplace, name='it_load_marketplace'),
    url(r'^marketplaces/it_load/day_ahead/$', views.it_load_day_ahead, name='it_load_day_ahead'),
    url(r'^marketplaces/it_load/intra_day/$', views.it_load_intra_day, name='it_load_intra_day'),
    url(r'^correlations$', views.correlations, name='correlations'),
    url(r'^correlations/update_correlations$', views.update_correlations, name='update_correlations'),
    url(r'^correlations/intra_day$', views.correlations_intra_day, name='correlations_intra_day'),
    url(r'^correlations/day_ahead$', views.correlations_day_ahead, name='correlations_day_ahead'),
    url(r'^correlations/update_correlations/intra_day$', views.update_correlations_intra_day, name='update_correlations_intra_day'),
    url(r'^correlations/update_correlations/day_ahead$', views.update_correlations_day_ahead, name='update_correlations_day_ahead'),
    url(r'^marketplaces/history/correlation_history$', views.history_correlations, name='history_correlations'),
    url(r'^marketplaces/history/correlation_history/intra_day/$', views.history_correlations_intra_day, name='history_correlations_intra_day'),
    url(r'^marketplaces/history/correlation_history/day_ahead/$', views.history_correlations_day_ahead, name='history_correlations_day_ahead'),
    url(r'^marketplaces/history/$', views.history, name='history'),
    url(r'^marketplaces/history/energy/$', views.energyHistory, name='history_energy'),
    url(r'^marketplaces/history/heat/$', views.heatHistory, name='history_heat'),
    url(r'^marketplaces/history/ITload/$', views.itLoadHistory, name='history_it_load'),
    url(r'^marketplaces/invoices/$', views.invoice_history, name='invoices'),
    url(r'^marketplaces/invoices/energy/$', views.energyInvoice, name='invoices_energy'),
    url(r'^marketplaces/invoices/heat/$', views.heatInvoice, name='invoices_heat'),
    url(r'^marketplaces/invoices/itLoad/$', views.itLoadInvoice, name='invoices_it_load'),
    url(r'^about$', views.about, name='about'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^sgi/$', views.smart_grid, name='smart_grid'),
    url(r'^marketplaces/admin/$', views.admin, name='madmin'),

]
