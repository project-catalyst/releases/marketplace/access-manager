"""AccessManager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import *
from datetime import datetime
from app import views
from app.forms import BootstrapAuthenticationForm

admin.autodiscover()

urlpatterns = [
    url(r'^login/$', views.login2, name='login'),
    url(r'^logout$',
        logout,
        {
            'next_page': '/',
        },
        name='logout'),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('app.urls')),
    url(r'^', include('gam.urls')),
    url(r'^signup/$', views.signupForm, name='user_signup'),
    url(r'^signup/validation/$', views.signup, name='validate_user_signup'),
    url(r'^profile/', views.profile, name='user_profile'),
]

if settings.RELATIVE_URL and len(settings.RELATIVE_URL) > 0:
    urlpatterns = [url(r'^{}/am/'.format(settings.RELATIVE_URL), include(urlpatterns)), ]

